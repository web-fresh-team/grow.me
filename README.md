# Yii2 Boilerplate
Yet another [Yii2](http://www.yiiframework.com/) application skeleton =)

## DIRECTORY STRUCTURE

```
src/ 				source code
vendors				3rd-party packages
runtime/            contains files generated during runtime
tests/              contains various tests for the basic application
web/				the entry script and Web resources
```


## SCSS - site

```
cd web/css/scss
scss --watch main.scss:../main.css
```

## SCSS - admin

```
cd web/css/admin/scss
scss --watch ../main.scss:../main.css
```

## Fixtures

Fixuteres located at:
```
tests/unit/fixtures - fixture files
tests/unit/templates/fixtures - template files
```

Generate data with next command: 
```
php yii fixture/generate <name>   (name - of template)
```

Load generated data to table with next command: 
```
php yii fixture/load <Name>  (name of fixure file)
```
