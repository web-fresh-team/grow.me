<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

$config = [
    'id' => 'app',
    'name' => 'Grow Me',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => ['log', 'core', 'app\components\Events'],
    'language' => 'ru-RU',
    'aliases' => [
        '@uploads' => dirname(dirname(__DIR__)) . '/uploads',
    ],
    'controllerMap' => [
        'elfinder-backend' => [
            'class' => 'mihaildev\elfinder\Controller',
            'user'=>'admin',
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'path' => 'uploads',
                    'name' => 'Uploads'
                ],
            ],
        ],
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'user' => 'user',
            'access' => ['@'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'class' => 'mihaildev\elfinder\UserPath',
                    'path'  => 'uploads/user_{id}',
                    'name'  => 'My Documents',
                    'access' => ['read' => '*', 'write' => '*']
                ],
            ],
        ],
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'depends' => [
                        'yii\web\JqueryAsset',
                    ],
                ],
            ],
        ],
        'twitter'=>[
            'class'=>'yii\authclient\clients\Twitter',
            'accessToken' => [
                'class'=>'yii\authclient\OAuthToken',
                'token' => $params['twitterAccessToken'],
                'tokenSecret' => $params['twitterAccessTokenSecret'],
            ],
            'consumerKey' => $params['twitterApiKey'],
            'consumerSecret' => $params['twitterApiSecret']
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '5099246',
                    'clientSecret' => 'flcfgG5AjaMdKN3Cb2Mw',
                    'scope' => 'email',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '1648394312044399',
                    'clientSecret' => '9781f46dbe36a603e2f573a03005b1e6',
                ]
            ]
        ],
        'formatter' => [
            'class' => 'app\components\Formatter',
        ],
        'request' => [
            'cookieValidationKey' => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'i18n' => [
            'translations' => [
                '*' => ['class' => 'yii\i18n\PhpMessageSource'],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'registration' => 'user/default/register',
                'login' => 'user/default/login',
                'logout' => 'user/default/logout',
                'search' => 'site/search',
                'advertisement' => 'site/advertisement',
                'blog' => 'site/blog',
                'contact-us' => 'site/contact-us',
                'request-password-reset' => 'user/default/request-password-reset',
                'reset-password' => 'user/default/reset-password',
                'idea/<slug>' => 'site/idea',
                'idea-create' => 'site/idea-create',
                'idea-edit/<slug>' => 'site/idea-edit',
                'personal-area/<username>' => 'site/personal-area',
                'risks' => 'site/risks',
                'terms' => 'site/terms'
            ]
        ],
        'db' => require(__DIR__ . '/db.php'),
        'thumbler'=> [
            'class' => 'alexBond\thumbler\Thumbler',
            'sourcePath' => '@webroot',
            'thumbsPath' => '@webroot/thumbs',
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],
    ],
    'modules' => $modules,
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    // $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
