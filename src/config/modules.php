<?php

return [
    'core' => ['class' => 'nullref\core\Module'],
    'admin' => ['class' => 'nullref\admin\Module'],
    'category' => ['class' => 'app\modules\category\Module'],
    'project' => ['class' => 'app\modules\project\Module'],
    'user' => ['class' => 'app\modules\user\Module'],
    'comment' => ['class' => 'app\modules\comment\Module'],
    'content' => ['class' => 'app\modules\content\Module'],
    'settings' => ['class' => 'pheme\settings\Module'],
    'feedback' => ['class' => 'app\modules\feedback\Module'],
    'blog' => ['class' => 'app\modules\blog\Module']
];