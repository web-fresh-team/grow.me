<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\console\Application;
use yii\console\Controller;

class ConsoleAdapter extends Component
{
    /**
     * Create and return an application instance
     * if the console app doesn't exist we create a new instance
     * otherwise, it returns the existing instance
     *
     * @return null|\yii\base\Module|Application
     */
    public static function switchToConsoleApp()
    {
        $config = self::getConsoleConfig();
        if (!$consoleApp = Yii::$app->getModule($config['id']))
            $consoleApp = new Application($config);

      //  define('STDOUT', fopen('php://output', 'w'));

        return $consoleApp;
    }

    public static function runConsoleActionFromWebApp($action, $params = [])
    {
        $webApp = Yii::$app;
        $consoleApp = self::switchToConsoleApp();
        $result = ($consoleApp->runAction($action, $params) == Controller::EXIT_CODE_NORMAL);
        Yii::$app = $webApp;
        return $result;
    }


    public static function getConsoleConfig()
    {
        return require(Yii::getAlias('@app/config/console.php'));
    }
}