<?php

namespace app\components;


use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class JsonBehavior extends  Behavior
{
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'dataToJson',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'dataToJson',
            ActiveRecord::EVENT_AFTER_FIND => 'jsonToData',
            ActiveRecord::EVENT_AFTER_UPDATE => 'jsonToData',
            ActiveRecord::EVENT_AFTER_INSERT => 'jsonToData',
        ];
    }

    public function dataToJson()
    {
        $model = $this->owner;
        foreach ($this->attributes as $attribute) {
            if (isset($model->$attribute) && !is_scalar($model->$attribute)) {
                $model->$attribute = Json::encode($model->$attribute);
            }
        }
    }

    public function jsonToData()
    {
        $model = $this->owner;
        foreach ($this->attributes as $attribute) {
            $model->$attribute = isset($model->$attribute) ? Json::decode($model->$attribute) : [];
        }
    }
}