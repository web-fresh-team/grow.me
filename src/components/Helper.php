<?php

namespace app\components;

use alexBond\thumbler\Thumbler;
use Yii;

/**
 * @author    Dmytro Karpovych
 * @copyright 2015 NRE
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Helper
{
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public static function getStatuses()
    {
        return [
            self::STATUS_DISABLE => Yii::t('catalog', 'Disable'),
            self::STATUS_ENABLE => Yii::t('catalog', 'Enable'),
        ];
    }

    public static function getInputFileOptions($options = [])
    {
        $defaults = [

            'buttonName' => Yii::t('app', 'Browse'),
            'language' => 'ru',
            'controller' => 'elfinder',
            'filter' => 'image',
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false
        ];

        return array_merge($options, $defaults);
    }


    public static function getThumb($source_image, $width, $height, $defaultImg = null, $method = Thumbler::METHOD_BOXED, $color = 'FFFFFF', $exept = false)
    {
        if (empty($source_image) || !(file_exists(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $source_image))) {
            $source_image = '/img/custom/no-photo.png';

            if ($defaultImg == 'avatar') {
                $method = Thumbler::METHOD_BOXED;
                $source_image = '/img/custom/no-avatar.png';
            } else if($defaultImg == 'friend-avatar') {
                $method = Thumbler::METHOD_BOXED;
                $source_image = '/img/custom/no-avatar.png';
            } else if($defaultImg == 'personal-area-project') {
                $method = Thumbler::METHOD_CROP_CENTER;

            } else if($defaultImg == 'project-card') {
                $method = Thumbler::METHOD_BOXED;
            }
            if( !(file_exists(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $source_image))) {
                $source_image = '/img/custom/no-photo.png';
            }
        }
        return str_replace(DIRECTORY_SEPARATOR, '/', '/thumbs/' . \Yii::$app->get('thumbler')->resize(urldecode($source_image), $width, $height, $method, $color, $exept));
    }

}