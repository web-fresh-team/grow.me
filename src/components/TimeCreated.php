<?php

namespace app\components;

use Yii;
/**
 * @author  Yaroslav Velychko <lijkbezorger@gmail.com>
 */

trait TimeCreated
{
    public function countTime($createdAt) {
        $nowTime =  new \DateTime();
        $createdTime = new \DateTime();
        $createdTime->setTimestamp($createdAt);

        $interval = $createdTime->diff($nowTime);
        $res = '';
        if($interval->y > 0) {
            $res = Yii::t('time','{Year, plural, one{# year} other{# years}} ago', ['Year' => $interval->y]);
        } elseif ($interval->m > 0) {
            $res = Yii::t('time','{Day, plural, one{# day} other{# days}} ago', ['Day' => $interval->days]);
        } elseif ($interval->m == 0 && $interval->d >0) {
            $res = Yii::t('time','{Day, plural, one{# day} other{# days}} ago', ['Day' => $interval->d]);
        } elseif ($interval->d == 0 && $interval->h > 0) {
            $res = Yii::t('time','{Hour, plural, one{# hour} other{# hours}} ago', ['Hour' => $interval->h]);
        } elseif ($interval->h == 0 && $interval->i > 0) {
            $res = Yii::t('time','{Minute, plural, one{# minute} other{# minutes}} ago', ['Minute' => $interval->i]);
        } else {
            $res = Yii::t('time', '{Second, plural, one{# second} other{# seconds}} ago', ['Second' => $interval->s]);
        }
        return $res;
    }
}

