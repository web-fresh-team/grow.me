<?php
namespace app\components;


use yii\authclient\clients\Twitter;
use yii\base\BootstrapInterface;
use app\modules\blog\models\BlogPost;
use yii\base\Event;
use Yii;
use yii\base\Exception;
use yii\helpers\StringHelper;

class Events implements BootstrapInterface
{
    public function bootstrap($app)
    {
        Event::on(BlogPost::className(), BlogPost::EVENT_AFTER_INSERT, function (Event $event) {

            /** @var BlogPost $sender */
            $sender = $event->sender;
            $publish = ($sender->status == $sender::STATUS_APPROVED);

            if ($publish) {
                try {
                    $this->makeTweet($sender);
                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                }
            }
        });
    }

    protected function makeTweet($model)
    {

        /** @var Twitter $twitter */
        $twitter = Yii::$app->get('twitter');

        $linkToPost = Yii::$app->urlManager->createAbsoluteUrl(['blog', '#' => 'post'.$model->id]);
        $media_id = null;
        if (!empty($model->photo)) {
            $filepath = str_replace('/', DIRECTORY_SEPARATOR, Yii::getAlias('@webroot').$model->photo);
            if ((filesize($filepath) < 1024*1024*5)&&($image = file_get_contents($filepath))) {
                $image_data = base64_encode($image);
                $params = [
                    'media_data' => $image_data,
                ];
                $media_id = $twitter->api('https://upload.twitter.com/1.1/media/upload.json', 'POST', $params)['media_id_string'];
            }
        }

        $maxlen = 140 - (strlen($linkToPost) + 5);
        $tweet = $model->text;
        $tweet = strip_tags($tweet);
        $tweet = preg_replace('/\s+/', ' ', $tweet);
        $suffix = (strlen($tweet) > $maxlen)?'...':'';
        $tweet = StringHelper::truncate($tweet, $maxlen, $suffix, null, true);
        $tweet .= "\n" . $linkToPost;
        $params = ['status' => $tweet];
        if (!empty($media_id)) {
            $params['media_ids'] = $media_id;
        }
        $twitter->api('statuses/update.json', 'POST', $params);
    }

}