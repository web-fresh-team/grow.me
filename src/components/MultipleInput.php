<?php
/**
 * @author    Serhiy Vinichuk <serhiyvinichuk@gmail.com>
 * @copyright 2015 AtNiwe
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


namespace app\components;

use unclead\widgets\MultipleInput as BaseInput;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

class MultipleInput extends BaseInput
{
    /**
     * @var string Class to use for columns
     */
    public $columnClass = 'unclead\widgets\MultipleInputColumn';

    /**
     * Initialization.
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (empty($this->columnClass)) {
            throw new InvalidConfigException(get_called_class() . '::$columnClass must be set');
        }
    }

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if (empty($this->columns)) {
            $this->guessColumns();
        }
        foreach ($this->columns as $i => $column) {
            $column = Yii::createObject(array_merge([
                'class' => $this->columnClass,
                'widget' => $this,
            ], $column));
            $this->columns[$i] = $column;
        }
    }

    /**
     * Returns element's name.
     *
     * @param string $name
     * @param string $index
     * @return string
     */
    public function getElementName($name, $index = null)
    {
        if ($index === null) {
            $index = '{index}';
        }
        return $this->getInputNamePrefix($name) . '[' . $index . '][' . $name . ']';
    }

    /**
     * Return prefix for name of input.
     *
     * @param string $name input name
     * @return string
     */
    private function getInputNamePrefix($name)
    {
        if ($this->hasModel()) {
            if (empty($this->columns) || (count($this->columns) == 1 && $this->model->hasProperty($name))) {
                return $this->model->formName();
            }
            return Html::getInputName($this->model, $this->attribute);
        }
        return $this->name;
    }
} 