<?php

namespace app\modules\comment;

use nullref\core\interfaces\IAdminModule;

class Module extends \yii\base\Module implements IAdminModule
{
    public $controllerNamespace = 'app\modules\comment\controllers';

    public static function getAdminMenu()
    {
        return [
            'label' => \Yii::t('admin', 'Comments'),
            'url' => ['/comment/admin'],
            'icon' => 'comment',
        ];
    }
}
