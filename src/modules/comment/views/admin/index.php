<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\comment\models\Comment;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php

    Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'text:ntext',
            [
                'attribute' => 'project_id',
                'value' => function(Comment $model) {
                    return $model->project->title;
                }
            ],
            [
                'class' => '\pheme\grid\ToggleColumn',
                'attribute' => 'status',
            ],
            //'created_at',
            //'updated_at',
            // 'parent_id',
            // 'user_id',
            // 'project_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

    Pjax::end();

    ?>

</div>
