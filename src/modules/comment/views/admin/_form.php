<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\User;
use app\modules\project\models\Project;

/* @var $this yii\web\View */
/* @var $model app\modules\comment\models\Comment */
/* @var $form yii\widgets\ActiveForm */

$users = User::getDropDownArray('id', 'username');
$projects = Project::getDropDownArray('id', 'title');
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->
        dropDownList($model->getStatusesMap(), ['prompt' => (Yii::t('app', 'Choose status'))]); ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->dropDownList($users) ?>

    <?= $form->field($model, 'project_id')->dropDownList($projects) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
