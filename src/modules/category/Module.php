<?php

namespace app\modules\category;

use nullref\core\interfaces\IAdminModule;

class Module extends \yii\base\Module implements IAdminModule
{
    public $controllerNamespace = 'app\modules\category\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => \Yii::t('admin', 'Categories'),
            'url' => ['/category/admin'],
            'icon' => 'tags',
        ];
    }
}
