<?php

use app\modules\category\models\Category;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\category\models\Category */
/* @var $form yii\widgets\ActiveForm */

$categories = array_merge([0 => ''], Category::getDropDownArray('id', 'title', ['NOT', ['id' => $model->id]]));

?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?php if (!$model->isNewRecord): ?>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?php endif ?>

    <?= $form->field($model, 'parent_id')->dropDownList($categories) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
