<?php

namespace app\modules\feedback;

use nullref\core\interfaces\IAdminModule;

class Module extends \yii\base\Module implements IAdminModule
{
    public $controllerNamespace = 'app\modules\feedback\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => \Yii::t('app', 'Feedbacks'),
            'url' => ['/feedback/admin'],
            'icon' => 'comment-o',
        ];
    }
}
