<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\feedback\models\Feedback;

/* @var $this yii\web\View */
/* @var $model app\modules\feedback\models\Feedback */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$status = Feedback::getStatusesMap()[$model->status];
?>
<div class="feedback-view">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'value' => $status
            ],
            'text:ntext',
            'title',
            'photo',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
