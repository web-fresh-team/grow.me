<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\project\models\Project;

/* @var $this yii\web\View */
/* @var $model \app\modules\project\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title',
            [
                'attribute' => 'short_description',
                'format' => 'html',
            ],
            [
                'attribute' => 'description',
                'format' => 'html',
            ],
            'bank',
            [
                'attribute' => 'status',
                'value' => $model->getStatusesMap()[$model->status],
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'slug',
            [
                'attribute' => 'user_id',
                'value' => $model->user->username,
            ],
            [
                'attribute' => 'likes_amount',
                'value' => $model->countLikes(),
            ],

        ],
    ]) ?>

</div>
