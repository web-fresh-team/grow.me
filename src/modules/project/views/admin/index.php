<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\project\models\Project;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            'short_description:ntext',
            'bank',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function (Project $model) {
                    return $model->getStatusesMap()[$model->status];
                }
            ],
            'slug',
            // 'created_at',
            // 'updated_at',
            // 'user_id',
            [
                'attribute' => 'user_id',
                'value' => 'user.username',
            ],
            [
                'attribute' => 'categories',
                'value' => function($model){
                    $list = [];

                    foreach ($model->categories as $cat) {
                        $list[]=$cat->title;
                    }

                    return implode(', ', $list);
                },
                'label' => Yii::t('app', 'Categories'),

            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
