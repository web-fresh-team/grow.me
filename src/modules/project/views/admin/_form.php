<?php

use app\components\MultipleInput;
use app\components\Helper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\User;
use mihaildev\ckeditor\CKEditor;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \app\modules\project\models\Project */
/* @var $form yii\widgets\ActiveForm */
/* @var $commentsDP yii\data\ActiveDataProvider */


$user = User::getDropDownArray('id', 'username', []);
$language = Yii::$app->language;

$this->registerJs(<<<JS
var counter = 1;
jQuery('#photos-input').on('init addNewRow',function() {
counter++;
    var row = jQuery(this).find('.input-group').last(),
    input = row.find('input'),
    id = 'new_w' + counter,
    btn = row.find('button');


    counter++;
    input.attr('id', id);
    btn.attr('id', id + '_button');
    mihaildev.elFinder.register(id, function (file, id) {
        $('#' + id).val(file.url).trigger('change');
        return true;
    });
    btn.off().on('click', function () {
        mihaildev.elFinder.openManager({
            "url": "/elfinder-backend/manager?filter=image&callback=" + id + "&lang=en",
            "width": "auto",
            "height": "auto",
            "id": "id"
        });
    });
});
JS
);

?>

<div class="project-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'editorOptions' => [
            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
            'height' => 400,
        ],
    ]) ?>

    <?= $form->field($model, 'short_description')->textarea(['maxlength' => true, 'rows' => 5]) ?>

    <div class="row">

        <div class="col-lg-6">

            <?= $form->field($model, 'bank')->textInput() ?>

            <?= $form->field($model, 'status')->
            dropDownList($model->getStatusesMap(), ['prompt' => (Yii::t('app', 'Choose status'))]); ?>

            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'user_id')->dropDownList($user) ?>

            <?= $form->field($model, 'categories_list')->checkboxList(\app\modules\category\models\Category::getDropDownArray('id', 'title')) ?>

        </div>

        <div class="col-lg-6">

            <?= $form->field($model, 'photos')->widget(MultipleInput::className(), [
                'id' => 'photos-input',
                'columnClass' => 'app\components\MultipleInputColumn',
                'columns' => [
                    [
                        'name' => 'url',
                        'type' => 'widget',
                        'widgetConfig' => \yii\helpers\ArrayHelper::merge(Helper::getInputFileOptions(), [
                            'class' => \mihaildev\elfinder\InputFile::className(),
                            'controller' => 'elfinder-backend'
                        ]),
                    ]
                ],
            ]) ?>

        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Yii::t('app', 'Comments') ?></h1>
        </div>
    </div>

    <div class="row">

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $commentsDP,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'text:ntext',
                [
                    'class' => '\pheme\grid\ToggleColumn',
                    'attribute' => 'status',
                ],
                //'created_at',
                //'updated_at',
                // 'parent_id',
                // 'user_id',
                // 'project_id',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'View'),
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                                'data-method' => 'post',
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = '/comment/admin/view?id=' . $model->id;
                            return $url;
                        } elseif ($action === 'update') {
                            $url = '/comment/admin/update?id=' . $model->id;
                            return $url;
                        } elseif ($action === 'delete') {
                            $url = '/comment/admin/delete?id=' . $model->id;
                            return $url;
                        }
                    }
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>
