<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \app\modules\project\models\Project */

/* @var $commentsDP \yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Create Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'commentsDP' => $commentsDP,
    ]) ?>

</div>
