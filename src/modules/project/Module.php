<?php

namespace app\modules\project;

use nullref\core\interfaces\IAdminModule;

class Module extends \yii\base\Module implements IAdminModule
{
    public $controllerNamespace = 'app\modules\project\controllers';


    public static function getAdminMenu()
    {
        return [
        'label' => \Yii::t('admin', 'Projects'),
        'url' => ['/project/admin'],
        'icon' => 'cube',
    ];
    }
}
