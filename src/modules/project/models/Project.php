<?php

namespace app\modules\project\models;

use app\components\JsonBehavior;
use app\models\Investment;
use app\models\Likes;
use app\models\ProjectHasCategory;
use app\modules\blog\models\BlogPost;
use app\modules\category\models\Category;
use app\modules\comment\models\Comment;
use app\modules\user\models\User;
use nullref\useful\DropDownTrait;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $short_description
 * @property string | array $photos
 * @property double $bank
 * @property integer $status
 * @property integer $likes_amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $slug
 * @property integer $user_id
 *
 * @property Comment[] $comments
 * @property Investment[] $investments
 *
 * @property $user User
 *
 * @property $categories[]  Category
 *
 */
class Project extends ActiveRecord
{
    use DropDownTrait;

    const STATUS_CREATED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_EMBODIED = 3;
    const STATUS_FOR_DELETING = 4;

    const SCENARIO_CREATE_FROM_CLIENT = 'client_create';

    public function init()
    {
        parent::init();
        if (Yii::$app->controller->action->uniqueId == 'site/idea-create') {
            self::setScenario(self::SCENARIO_CREATE_FROM_CLIENT);
        }
    }

    static public function getStatusesMap()
    {
        return [
            self::STATUS_CREATED => Yii::t('app', 'Created'),
            self::STATUS_IN_PROGRESS => Yii::t('app', 'In progress'),
            self::STATUS_EMBODIED => Yii::t('app', 'Embodied'),
            self::STATUS_FOR_DELETING => Yii::t('app', 'For deleting')
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => 1],
            [['likes_amount'], 'default', 'value' => 0],
            [['title', 'description', 'short_description', 'photos', 'bank'], 'required'],
            [['description'], 'string'],
            [['short_description'], 'string', 'max' => 150],
            [['bank'], 'number'],
            [['bank'], 'compare', 'compareValue' => 0, 'operator' => '>', 'message' => Yii::t('app', '{attribute} should be biggest than 0')],
            [['status', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['photos', 'categories_list'], 'safe']
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE_FROM_CLIENT => ['title','status','description','short_description','photos','bank']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'short_description' => Yii::t('app','Short description'),
            'photos' => Yii::t('app', 'Photos'),
            'bank' => Yii::t('app', 'Budget'),
            'status' => Yii::t('app', 'Status'),
            'likes_amount' => Yii::t('app', 'Likes Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'slug' => Yii::t('app', 'Slug'),
            'user_id' => Yii::t('app', 'User'),
            'categories_list' => Yii::t('app', 'Categories'),
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'datetime' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            'json' => [
                'class' => JsonBehavior::className(),
                'attributes' => ['photos'],
            ],
            'categoriesMtM' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'categories_list' => 'categories',
                ],
            ]
        ]);
    }

    public function createBlogPost($status)
    {
        $blogPost = new BlogPost();
        $blogPost->status = $status;
        $blogPost->title = $this->title;
        $blogPost->text = $this->short_description;
        $blogPost->photo = isset($this->photos[0]['url'])?$this->photos[0]['url']:null;
        return ($blogPost->validate() && $blogPost->save());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['project_id' => 'id'])->orderBy(['created_at' => SORT_DESC]);
    }

    public function getApprovedComments()
    {
        return $this->getComments()->where(['status' => Comment::STATUS_APPROVED]);
    }

    public function getLastComments($amount = 0)
    {
        return $this->getApprovedComments()->offset($amount)->limit(3);
    }

    public function getCommentsAmount()
    {
        return $this->getApprovedComments()->count('*');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestments()
    {
        return $this->hasMany(Investment::className(), ['project_id' => 'id'])->orderBy(['created_at' => SORT_DESC]);
    }

    public function getSentInvestments()
    {
        return  $this->getInvestments()->where(['status' => Investment::STATUS_SENT]);
    }

    public function getAcceptedInvestments()
    {
        return  $this->getInvestments()->where(['status' => Investment::STATUS_ACCEPTED]);
    }

    public function getAmount()
    {
        $amount = $this->getAcceptedInvestments()->sum('amount');
        return ($amount != null ) ? $amount : 0;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Likes::className(), ['project_id' => 'id']);
    }

    public function getLikesAmount()
    {
        return Likes::find()->where(['project_id' => $this->id])->count();
    }

    public function countLikes()
    {
        $likesM = new Likes();
        $likes = count($likesM->find()->where(['project_id' => $this->id])->asArray()->all());
        $this->likes_amount = $likes;
        return $likes;
    }


    /**
     * @param User $user
     * @return bool
     */
    public function isLikedByUser(User $user)
    {
        $like = Likes::findOne([
            'user_id' => $user->id,
            'project_id' => $this->id,
        ]);

        if($like) {
            return true;
        } else {
            return false;
        }
    }

    public function isLikedByCurrentUser()
    {
        /* @var $user User */
        $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
        return $this->isLikedByUser($user);
    }

    public function likeByUser(User $user)
    {
        $like = new Likes();
        $like->user_id = $user->id;
        $like->project_id = $this->id;
        $like->save();
    }

    public function dislikeByUser(User $user)
    {
        /* @var $like Likes */
        $like = Likes::findOne(['user_id' => $user->id]);
        $like->delete();
    }

    public function likeByCurrentUser()
    {
        /* @var $user User */
        $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
        $this->likeByUser($user);
    }

    public function dislikeByCurrentUser()
    {
        /* @var $user User */
        $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
        $this->dislikeByUser($user);
    }

    public function  getCategories(){
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('project_has_category', ['project_id' => 'id']);
    }

    public function getCategoryArray()
    {
        $categoryM = new ProjectHasCategory();
        $categoryArray = $categoryM->find()->where(['project_id' => $this->id])->asArray()->all();
        return implode(', ',$categoryArray);
    }

}
