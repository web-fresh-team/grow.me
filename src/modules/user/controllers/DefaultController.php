<?php

namespace app\modules\user\controllers;

use app\modules\user\models\LoginForm;
use app\modules\user\models\RegistrationForm;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use app\modules\user\models\User;
use app\modules\user\models\RequestPasswordResetForm;
use app\modules\user\models\PasswordResetForm;
use yii\web\Response;
use Yii;

class DefaultController extends Controller
{
    public $defaultAction = 'login';


    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'authSuccess']
            ]
        ];
    }

    /**
     * This function will be triggered when user is successfuly authenticated using some oAuth client.
     *
     * @param ClientInterface $client
     * @return boolean|Response
     */
    public function authSuccess($client)
    {
        $userAttributes = $client->getUserAttributes();
        $role = Yii::$app->request->get('role');

        /** @var User $user */
        $user = User::findOne([
            $client->getId() . '_id' => $userAttributes['id'],
        ]);

        if (Yii::$app->user->isGuest) {
            if ($user) { // login
                Yii::$app->user->login($user);
                return $this->redirect(Url::to(['/personal-area/'.$user->username]));

            } else { // signup
                if (isset($userAttributes['email']) && User::find()->where(['email' => $userAttributes['email']])->exists()) {
                    $user = User::findOne(['email' => $userAttributes['email']]);
                    if ($user->{$client->getId() . '_id'} != $userAttributes['id']) {
                        $user->{$client->getId() . '_id'} = $userAttributes['id'];
                    }
                    $user->role = $role;
                    $user->save();
                    Yii::$app->user->login($user);
                    return $this->redirect(Url::to(['/personal-area/'.$user->username]));
                } else {
                    if (empty($role)) {
                        return false;
                    }
                    $form = new RegistrationForm(['scenario' => RegistrationForm::SCENARIO_AUTHCLIENT]);
                    $form->fillFromAuthClient($client, $role);
                    $user = $form->register();
                    if ($user) {
                        Yii::$app->user->login($user);
                        return $this->redirect(Url::to(['/personal-area/'.$user->username]));

                    } else {
                        print_r($form->getErrors());
                        return false;
                    }
                }
            }
        } else { // user already logged in
            // add auth provider
            /** @var User $user */
            $user = Yii::$app->user->getIdentity();
            $user->{$client->getId().'_id'} = $userAttributes['id'];
            $user->save();
            return $this->render('/personal-area', [
                'username' =>$user->username,
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            $username = User::findOne(['id' => \Yii::$app->user->identity->getId()])->username;
            return $this->redirect(Url::to(['/personal-area/' . $username]));
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Register user
     *
     * @param $role
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionRegister($role)
    {
        $role = intval($role);
        if (($role > 2) || ($role < 1)) {
            throw new BadRequestHttpException();
        }

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegistrationForm(['role' => $role, 'scenario' => RegistrationForm::SCENARIO_FORM]);

        if ($model->load(\Yii::$app->request->post())) {
            $user = $model->register();

            if ($user) {
                if ($user->sendConfirmationViaMail()) {
                    \Yii::$app->session->setFlash('email-sent',
                        \Yii::t('app', 'Email with confirm link was sent to ' . $user->email));
                }
            }
        }

        return $this->render('register', [
            'model' => $model
        ]);
    }

    /**
     * Activates user account
     *
     * @param $token
     * @return \yii\web\Response
     */
    public function actionConfirmEmail($token)
    {
        $user = User::findUserByConfirmToken($token);
        if ($user) {
            $user->confirm();
            $user->save();
            \Yii::$app->getSession()->setFlash('user-successful-registration');
            \Yii::$app->user->login($user);
            return $this->redirect(Url::to(['/personal-area/' . $user->username]));
        } else {
            return $this->goHome();
        }
    }

    public function actionRequestPasswordReset()
    {
        $model = new RequestPasswordResetForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmailWithResetPasswordToken()) {
                \Yii::$app->getSession()->setFlash('request-success', \Yii::t('app', 'Check your email for further instructions.'));

            } else {
                \Yii::$app->getSession()->setFlash('request-error',
                    \Yii::t('app', 'Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('request-password-reset', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new PasswordResetForm($token);
        } catch (Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (!$model->isPasswordResetTokenValid()) {
            \Yii::$app->getSession()->setFlash('reset-token-error', \Yii::t('app', 'Invalid password reset token or token time has expired.'));
        } else if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->passwordReset()) {
            \Yii::$app->getSession()->setFlash('user-successful-password-reset');
            \Yii::$app->user->login($model->getUser());
            return $this->redirect('/personal-area/' . $model->getUser()->username);
        }

        return $this->render('password-reset', [
            'model' => $model,
        ]);
    }
}
