<?php

namespace app\modules\user;

use nullref\core\interfaces\IAdminModule;

class Module extends \yii\base\Module  implements IAdminModule
{
    public $controllerNamespace = 'app\modules\user\controllers';


    public static function getAdminMenu()
    {
        return [
            'label' => \Yii::t('app', 'Users'),
            'url' => ['/user/admin'],
            'icon' => 'user',
        ];
    }
}
