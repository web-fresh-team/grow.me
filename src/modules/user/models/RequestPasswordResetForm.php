<?php

namespace app\modules\user\models;

use yii\base\Model;
use app\modules\user\models\User;

class RequestPasswordResetForm extends Model {

    public $email;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => \Yii::t('app', 'There is no user with email'),
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmailWithResetPasswordToken()
    {
        /* @var $user User */
        $user = User::
            findOne([
            'email' => $this->email,
            'status' => User::STATUS_ACTIVE
        ]);

        if ($user) {
            if (!$user->isPasswordResetTokenValid()) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return \Yii::$app->mailer->compose([
                    'html' => 'passwordResetToken-html',
                    'text' => 'passwordResetToken-text'
                ], ['user' => $user])
                    ->setFrom(\Yii::$app->params['supportEmail'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
} 