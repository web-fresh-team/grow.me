<?php

namespace app\modules\user\models;

use app\models\Friend;
use app\models\Investment;
use app\models\Message;
use app\modules\comment\models\Comment;
use app\modules\project\models\Project;
use nullref\useful\DropDownTrait;
use nullref\useful\PasswordTrait;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $role
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $confirm_token
 * @property string $status
 * @property string $photo
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    use PasswordTrait;

    use DropDownTrait;

    const STATUS_NEW = 1;
    const STATUS_ACTIVE = 2;

    const ROLE_OWNER = 1;
    const ROLE_INVESTOR = 2;

    const SCENARIO_ADMIN_CREATE = 'admin create';
    const SCENARIO_ADMIN_UPDATE = 'admin update';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'first_name', 'last_name', 'email'], 'required'],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('app', "Username {value} is already taken.")],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('app', "Email {value} is already taken.")],
            [['role', 'status'], 'integer'],
            ['password', 'safe', 'on' => self::SCENARIO_ADMIN_UPDATE],
            ['password', 'required', 'on' => self::SCENARIO_ADMIN_CREATE],
            ['password', 'string', 'min' => 6],
            [['username', 'first_name', 'last_name', 'email', 'phone', 'address'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            ['email', 'email'],
            ['photo', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'role' => Yii::t('app', 'Role'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'photo' => Yii::t('app', 'Photo'),
            'status' => Yii::t('app', 'Status'),
            'confirm_token' => Yii::t('app', 'Confirm Token'),
            'password' => Yii::t('app', 'Password')
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'futureFriends' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'futureFriend_list' => 'futureFriends',
                ],
            ],
            'activeFriends' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'activeFriend_list' => 'activeFriends',
                ],
            ],
        ]);
    }

    static public function getRolesMap()
    {
        return [
            User::ROLE_OWNER => Yii::t('app', 'Project owner'),
            User::ROLE_INVESTOR => Yii::t('app', 'Investor')
        ];
    }

    static public function getStatusesMap()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_NEW => Yii::t('app', 'New'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestments()
    {
        return $this->hasMany(Investment::className(), ['user_id' => 'id'])
            ->joinWith('project','`investment`.`project_id` = `project`.`id`')
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['to_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages0()
    {
        return $this->hasMany(Message::className(), ['from_id' => 'id']);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function setPassword($value)
    {
        if (empty($value)) return;
        $this->password_hash = Yii::$app->security->generatePasswordHash($value);
        $this->_password = $value;
    }

    public function isPasswordCorrect($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    static public function findActiveUserByEmail($email)
    {
        return User::findOne(['email' => $email, 'status' => User::STATUS_ACTIVE]);
    }

    /**
     * @return bool
     */
    public function sendConfirmationViaMail()
    {
         return Yii::$app->mailer->compose(
             ['html' => 'confirmRegistration-html', 'text' => 'confirmRegistration-text'],
             ['user' => $this])
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($this->email)
            ->setSubject(Yii::t('app',"Account activation"))
            ->send();
    }

    public function generateConfirmRegistrationToken()
    {
        $this->confirm_token = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . "_" . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function isPasswordResetTokenValid()
    {
        if (empty($this->password_reset_token)) {
            return false;
        }

        $expire = Yii::$app->params['user.resetPasswordTokenExpire'];
        $parts = explode('_', $this->password_reset_token);
        $timestamp = end($parts);
        return time() <= $timestamp + $expire;

    }

    /**
     * @param $token
     * @return null|User
     */
    static public function findUserByConfirmToken($token)
    {
        return self::findOne(['confirm_token' => $token]);
    }

    /**
     * @param $token
     * @return null|User
     */
    static public function findByPasswordResetToken($token)
    {
        return self::findOne(['password_reset_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    public function getConfirmToken()
    {
        if ($this->confirm_token === null) {
            $this->generateConfirmRegistrationToken();
        }
        return $this->confirm_token;
    }

    public function confirm()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['user_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function  getFutureFriends()
    {
        return $this->hasMany(User::className(), ['id' => 'from_id'])
            ->viaTable('friend', ['to_id' => 'id'], function($query){
                $query->andWhere(['status' => Friend::STATUS_SENT]);
            });
    }

    public function getActiveFriends()
    {
        return $this->hasMany(User::className(), ['id' => 'to_id'])
            ->viaTable('friend', ['from_id' => 'id'], function($query){
                $query->andWhere(['status' => Friend::STATUS_ACCEPTED]);
            });
    }

    public function getRole()
    {
        return $this->role;
    }

}
