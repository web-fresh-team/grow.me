<?php

namespace app\modules\user\models;

use yii\base\Model;
use Yii;


class LoginForm extends Model {

    public $username;
    public $email;
    public $password;
    public $rememberMe = true;

    /**
     * @var User
     */
    public $_user = null;

    public function rules()
    {
        return
        [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password')
        ];
    }

    public function validatePassword($attribute, $params)
    {
        $user = $this->getUser();
        if(($user === null) || (!$user->isPasswordCorrect($this->password))) {
            $this->addError($attribute, \Yii::t('app', "Incorrect email or password"));
        }
    }

    public function login()
    {
        if($this->validate()) {
            return \Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    public function getUser()
    {
        if($this->_user === null) {
            $this->_user = User::findActiveUserByEmail($this->email);
        }

        return $this->_user;
    }
}