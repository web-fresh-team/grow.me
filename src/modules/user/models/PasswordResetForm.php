<?php

namespace app\modules\user\models;

use app\modules\user\models\User;
use yii\base\Model;
use Yii;

class PasswordResetForm extends Model {
    /**
     * @var string
     */
    public $password;

    public $password_again;

    /**
     * @var User
     */
    private $_user;

    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            \Yii::$app->session->setFlash('reset-token-error', \Yii::t('app', 'Password reset token cannot be blank.'));
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            \Yii::$app->session->setFlash('reset-token-error', \Yii::t('app', 'Wrong password reset token.'));
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['password', 'password_again'], 'required'],
            [['password', 'password_again'], 'string', 'min' => 6],
            ['password_again', 'compare',
                'compareAttribute' => 'password',
                'operator' => '==',
                'message' => \Yii::t('app', 'Passwords are not equal.')
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'Password'),
            'password_again' => Yii::t('app', 'Password Again')
        ];
    }

    public function isPasswordResetTokenValid()
    {
        if(!empty($this->_user)) {
            return $this->_user->isPasswordResetTokenValid();
        }
        return false;
    }

    public function passwordReset()
    {
        $user = $this->_user;
        if ($user->isPasswordResetTokenValid()) {
            $user->setPassword($this->password);
            $user->removePasswordResetToken();
            return $user->save(false);
        }

        return false;
    }

    public function getUser()
    {
        return $this->_user;
    }

} 