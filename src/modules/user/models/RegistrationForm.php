<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;
use yii\authclient\ClientInterface;

class RegistrationForm extends Model {

    public $username;
    public $email;
    public $password;
    public $password_again;
    public $role;
    public $first_name;
    public $last_name;
    public $address;
    public $phone;

    /** @var  ClientInterface $client */
    public $client;

    const SCENARIO_FORM = 'register via form';
    const SCENARIO_AUTHCLIENT = 'register via authclient';

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'password_again' => Yii::t('app', 'Password Again'),
            'role' => Yii::t('app', 'Role'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_FORM => ['username', 'email', 'password', 'password_again', 'first_name', 'last_name', 'role'],
            self::SCENARIO_AUTHCLIENT => ['username', 'email', 'first_name', 'last_name', 'role'],
        ]);
    }

    public function rules()
    {
        return [
            [['username', 'email', 'password', 'first_name', 'last_name'], 'required',
                'on' => self::SCENARIO_FORM],
            [['username', 'email', 'first_name', 'last_name'], 'required',
                'on' => self::SCENARIO_AUTHCLIENT],
            [['role'], 'integer'],
            ['status', 'default', 'value' => User::STATUS_NEW, 'on' => self::SCENARIO_FORM],
            ['status', 'default', 'value' => User::STATUS_ACTIVE, 'on' => self::SCENARIO_AUTHCLIENT],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => User::className(),
                'message' => Yii::t('app', 'This username already taken.')],
            [['username', 'email'], 'trim'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::className(),
                'message' => Yii::t('app','This email already taken.')],
            ['email', 'email'],
            [['username', 'first_name', 'last_name', 'email', 'phone', 'address'],
                'string', 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 32],
            ['password_again', 'required', 'message' => Yii::t('app', 'Enter password again')],
            ['password_again', 'compare',
                'compareAttribute' => 'password',
                'operator' => '==',
                'message' => Yii::t('app','Passwords are not equal.')],
        ];
    }

    /**
     * @return \app\modules\user\models\User|null
     */
    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->role = $this->role;
            $user->address = $this->address;
            $user->phone = $this->phone;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateConfirmRegistrationToken();
            if ($this->client !== null) {
                $user->{$this->client->getId().'_id'} = $this->client->getUserAttributes()['id'];
            }
            if ($user->save()) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param ClientInterface $client
     * @param int $role
     */
    public function fillFromAuthclient($client, $role)
    {
        $userAttributes = $client->getUserAttributes();

        if ($client->getId() == "facebook") {
            $name = $userAttributes['name'];
            $firstname = explode(' ', $name)[0];
            $lastname = explode(' ', $name)[1];
        } else { //vkontakte
            $firstname = $userAttributes['first_name'];
            $lastname = $userAttributes['last_name'];
        }
        $email = $userAttributes['email'];
        $username = $userAttributes['email'];

        $this->client = $client;
        $this->username = $username;
        $this->email = $email;
        $this->first_name = $firstname;
        $this->last_name = $lastname;
        $this->role = $role;
    }

} 