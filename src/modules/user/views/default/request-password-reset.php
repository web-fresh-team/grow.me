<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\RequestPasswordResetForm */


$this->title = 'Reset password';

?>
<div class="request-password-reset">
    <div class="wrapper-footer">
        <div class="container">

            <?php if (Yii::$app->session->hasFlash('request-success')): ?>
                <div class="alert alert-success">
                    <?= Yii::$app->session->getFlash('request-success') ?>
                </div>
            <?php endif ?>

            <?php $form = ActiveForm::begin([
                'class' => 'yii\widgets\ActiveForm',
                'fieldConfig' => [
                    'class' => 'yii\widgets\ActiveField',
                    'template' => "{input}\n{hint}\n{error}",
                    'inputOptions' => [
                        'class' => 'registration-input',
                    ],
                    'errorOptions' => [
                        'class' => 'for-error',
                    ],
                    'options' => [
                        'class' => 'input-wrapper'
                    ]
                ],
                'errorCssClass' => 'error',
            ]); ?>
            <div class="registration-form">

                <?=
                $form->field($model, 'email', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('email')
                    ]
                ])->textInput(['maxlength' => true])?>

                <div class="button">
                    <?= Html::submitButton(Yii::t('app', 'Reset Password'), ['class' => 'button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>

</div>