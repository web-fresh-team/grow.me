<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\LoginForm */
/* @var $form ActiveForm */

$this->title = Yii::t('app', "Log in");
?>
<div class="site-login">
    <div class="wrapper-footer">
        <div class="container">
            <?php $form = ActiveForm::begin([
                'class' => 'yii\widgets\ActiveForm',
                'fieldConfig' => [
                    'class' => 'yii\widgets\ActiveField',
                    'template' => "{input}\n{hint}\n{error}",
                    'inputOptions' => [
                        'class' => 'registration-input',
                    ],
                    'errorOptions' => [
                        'class' => 'for-error',
                    ],
                    'options' => [
                        'class' => 'input-wrapper'
                    ]
                ],
                'errorCssClass' => 'error',

            ]); ?>
            <div class="registration-form">

                <?=
                $form->field($model, 'email', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('email')
                    ]
                ]) ?>
                <?=
                $form->field($model, 'password', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('password')
                    ]
                ])->passwordInput() ?>

                <div class="button">
                    <?= Html::submitButton(Yii::t('app', 'Sign In'), ['class' => 'button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="link-container">
                    <?php if(Yii::$app->request->isPost && (!$model->validate())): ?>
                        <?= Html::a('Forgot password?', ['/user/default/request-password-reset'], ['class' => 'forgot-pass-link']) ?>
                    <?php endif; ?>
                </div>
                <?= yii\authclient\widgets\AuthChoice::widget([
                    'baseAuthUrl' => ['default/auth'],
                    'popupMode' => false,
                ]); ?>
            </div>


        </div>
    </div>


</div><!-- site-login -->