<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\user\models\User;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\RegistrationForm */
/* @var $form ActiveForm */

$this->title = $model->role == User::ROLE_INVESTOR ? Yii::t('app', 'I want to invest') : Yii::t('app', 'I have an idea');
$userRole = Yii::$app->request->get('role');
?>

<div class="modules-user-default-register">
    <div class="wrapper-footer">
        <div class="container">
            <?php if (Yii::$app->session->hasFlash('email-sent')): ?>
                <div class="alert alert-success">
                    <?= Yii::$app->session->getFlash('email-sent') ?>
                </div>
            <?php endif ?>
            <?php $form = ActiveForm::begin([
                'class' => 'yii\widgets\ActiveForm',
                'fieldConfig' => [
                    'class' => 'yii\widgets\ActiveField',
                    'template' => "{input}\n{hint}\n{error}",
                    'inputOptions' => [
                        'class' => 'registration-input',
                    ],
                    'errorOptions' => [
                        'class' => 'for-error',
                    ],
                    'options' => [
                        'class' => 'input-wrapper'
                    ]
                ],
                'errorCssClass' => 'error',
            ]); ?>
            <div class="registration-form">
                <?= $form->field($model, 'username', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('username')
                    ]
                ]) ?>
                <?= $form->field($model, 'email', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('email')
                    ]
                ]) ?>
                <?= $form->field($model, 'password', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('password')
                    ]
                ])->passwordInput() ?>
                <?= $form->field($model, 'password_again', [
                    'inputOptions' => [
                    'placeholder' => $model->getAttributeLabel('password_again')
                ]
                ])->passwordInput() ?>
                <?= $form->field($model, 'first_name', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('first_name')
                    ]
                ]) ?>
                <?= $form->field($model, 'last_name', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('last_name')
                    ]
                ]) ?>           

                <?= $form->field($model, 'phone', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('phone')
                    ]
                ]) ?>

                <?= $form->field($model, 'address', [
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel('address')
                    ]
                ]) ?>

                <div class="button">
                    <?= Html::submitButton(Yii::t('app', 'Sign Up'), ['class' => 'button', 'style' => 'font-size: 18px']) ?>
                </div>

                <?= yii\authclient\widgets\AuthChoice::widget([
                    'baseAuthUrl' => ['default/auth', 'role' => $userRole],
                    'popupMode' => false,
                ]); ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div><!-- modules-user-default-register -->
