<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\PasswordResetForm */

?>

<div class="password-reset">
    <div class="wrapper-footer">
        <div class="container">
            <?php if (!Yii::$app->session->hasFlash('reset-token-error')): ?>

                <?php $form = ActiveForm::begin([
                    'class' => 'yii\widgets\ActiveForm',
                    'fieldConfig' => [
                        'class' => 'yii\widgets\ActiveField',
                        'template' => "{input}\n{hint}\n{error}",
                        'inputOptions' => [
                            'class' => 'registration-input',
                        ],
                        'errorOptions' => [
                            'class' => 'for-error',
                        ],
                        'options' => [
                            'class' => 'input-wrapper'
                        ]
                    ],
                    'errorCssClass' => 'error',

                ]); ?>
                <div class="registration-form">

                    <?=
                    $form->field($model, 'password', [
                        'inputOptions' => [
                            'placeholder' => $model->getAttributeLabel('password')
                        ]
                    ])->passwordInput() ?>
                    <?=
                    $form->field($model, 'password_again', [
                        'inputOptions' => [
                            'placeholder' => $model->getAttributeLabel('password_again')
                        ]
                    ])->passwordInput() ?>

                    <div class="button">
                        <?= Html::submitButton(Yii::t('app', 'Save Password'), ['class' => 'button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            <?php else: ?>
                <div class="alert alert-danger">
                    <?= Yii::$app->session->getFlash('reset-token-error') ?>
                </div>
            <?php endif ?>

        </div>
    </div>

</div>