<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
$roles = User::getRolesMap();
?>
<div class="user-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            [
                'attribute' => 'role',
                'content' => function(User $model){
                    return User::getRolesMap()[$model->role];
                }
            ],
            'first_name',
            'last_name',
            'email:email',
            // 'phone',
            // 'address',
            // 'confirm_token',
            [
                'attribute' => 'status',
                'content' => function(User $model){
                    return User::getStatusesMap()[$model->status];
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
