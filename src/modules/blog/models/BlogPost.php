<?php

namespace app\modules\blog\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "blog_post".
 *
 * @property integer $id
 * @property integer $status
 * @property string $text
 * @property string $title
 * @property string $photo
 * @property integer $created_at
 * @property integer $updated_at
 * @property array $data
 */
class BlogPost extends \yii\db\ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_APPROVED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'text', 'title'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            ['photo', 'safe']
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'datetime' => [
                'class' => TimestampBehavior::className(),
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'text' => Yii::t('app', 'Text'),
            'title' => Yii::t('app', 'Title'),
            'photo' => Yii::t('app', 'Photo'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    static public function getStatusesMap()
    {
        return [
            self::STATUS_CREATED => Yii::t('app', 'Created'),
            self::STATUS_APPROVED => Yii::t('app', 'Approved'),
        ];
    }
}
