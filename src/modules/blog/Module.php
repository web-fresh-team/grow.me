<?php

namespace app\modules\blog;

use nullref\core\interfaces\IAdminModule;

class Module extends \yii\base\Module implements IAdminModule
{
    public $controllerNamespace = 'app\modules\blog\controllers';

    static public function getAdminMenu()
    {
        return [
            'label' => \Yii::t('app', 'Blog'),
            'url' => ['/blog/admin'],
            'icon' => 'newspaper-o',
        ];
    }
}
