<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\BlogPost */
/* @var $form yii\widgets\ActiveForm */

$statuses = \app\modules\feedback\models\Feedback::getStatusesMap();
?>

<div class="blog-post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'editorOptions' => [
            'preset' => 'basic',
            'height' => 400,
            'fillEmptyBlocks' => false,
            'tabSpaces' => 0,
        ]
    ]) ?>

    <?= $form->field($model, 'photo')->widget(\mihaildev\elfinder\InputFile::className(), [
        'language'      => 'ru',
        'controller'    => 'elfinder-backend', // ��������� �������� �����������, �� ��������� ����� elfinder
        'filter'        => 'image',    // ������ ������, ����� ������ ������ �������� https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false,       // ����������� ������ ���������� ������

    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($statuses) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
