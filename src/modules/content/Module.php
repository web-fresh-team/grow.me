<?php

namespace app\modules\content;

use nullref\core\interfaces\IAdminModule;


class Module extends \yii\base\Module implements IAdminModule
{
    public $controllerNamespace = 'app\modules\content\controllers';

    public static function getAdminMenu()
    {
        return [
            'label' => \Yii::t('admin', 'Content'),
            'url' => ['/content/admin'],
            'icon' => 'pencil-square-o',
        ];
    }
}