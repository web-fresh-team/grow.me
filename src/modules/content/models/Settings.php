<?php

namespace app\modules\content\models;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $type
 * @property string $section
 * @property string $key
 * @property Text $value
 * @property integer $active
 * @property \DateTime $created
 * @property \DateTime $modified
 */
class Settings extends \yii\db\ActiveRecord
{

}