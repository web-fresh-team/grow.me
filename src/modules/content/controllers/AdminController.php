<?php

namespace app\modules\content\controllers;

use nullref\admin\components\AdminController as Controller;
use Yii;

class AdminController extends Controller
{
    public function actionIndex()
    {
        return $this->render('view');
    }

    public function actionUpdate($key)
    {
        $settings = Yii::$app->settings;
        if(Yii::$app->request->isPost) {
            $value = Yii::$app->request->post()['value'];
            $settings->set($key, $value);

            return $this->render('view');
        }

        return $this->render('update', [
            'key' => $key,
        ]);
    }

    public function actionGraph($title,$slicesRows)
    {
        $settings = Yii::$app->settings;

        $array = [5];

        if(Yii::$app->request->isPost) {
            $GraphTitle  = Yii::$app->request->post()['title'];

            $array = [
                'slice1' => [
                    'name' => Yii::$app->request->post()['name-1'],
                    'numb' => Yii::$app->request->post()['numb-1'],
                    'info' => Yii::$app->request->post()['info-1'],
                    'color' => Yii::$app->request->post()['color-1'],
                ],
                'slice2' => [
                    'name' => Yii::$app->request->post()['name-2'],
                    'numb' => Yii::$app->request->post()['numb-2'],
                    'info' => Yii::$app->request->post()['info-2'],
                    'color' => Yii::$app->request->post()['color-2'],
                ],
                'slice3' => [
                    'name' => Yii::$app->request->post()['name-3'],
                    'numb' => Yii::$app->request->post()['numb-3'],
                    'info' => Yii::$app->request->post()['info-3'],
                    'color' => Yii::$app->request->post()['color-3'],
                ],
                'slice4' => [
                    'name' => Yii::$app->request->post()['name-4'],
                    'numb' => Yii::$app->request->post()['numb-4'],
                    'info' => Yii::$app->request->post()['info-4'],
                    'color' => Yii::$app->request->post()['color-4'],
                ],
                'slice5' => [
                    'name' => Yii::$app->request->post()['name-5'],
                    'numb' => Yii::$app->request->post()['numb-5'],
                    'info' => Yii::$app->request->post()['info-5'],
                    'color' => Yii::$app->request->post()['color-5'],
                ],

            ];
            $array = json_encode($array);

            //SET SETTINGS OMG
            $settings->set($title, $GraphTitle);
            $settings->set($slicesRows, $array);
            return $this->render('view');
        } else {

        }
        return $this->render('graph', [
            'title' => $title,
            'slicesRows' => $slicesRows,
        ]);
    }
}
