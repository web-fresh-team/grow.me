<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $key string */

$settings = Yii::$app->settings;
?>

<div class="content-update">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= $key ?></h1>
        </div>
    </div>
    <?= Html::beginForm(['update', 'key' => $key], 'post'); ?>
    <?= Html::textarea('value', $settings->get($key), ['class' => 'form-control', 'rows' => 8]); ?>
    <br/>
    <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary']) ?>
    <?= Html::endForm(); ?>


</div>