<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $slicesRows string */

$settings = Yii::$app->settings;
?>
<div class="graph">
    <?= Html::beginForm([
        'graph',
        'title' => 'index.graph-title',
        'slicesRows' => 'index.graph-rows',
    ],'post'); ?>
    <h3><?= Yii::t('app', 'Title graph') ?></h3>
    <?= Html::input('text', 'title', $settings->get($title), ['class' => 'form-control'])?>
    <table>
        <?php $i = 1;
        $array = $settings->get($slicesRows);
        $array = json_decode($array);

        if(!$array) {
            $array = [
                'slice1' => [
                    'name' => '',
                    'numb' => '',
                    'info' => '',
                    'color' => '',
                ],
                'slice2' => [
                    'name' => '',
                    'numb' => '',
                    'info' => '',
                    'color' => '',
                ],
                'slice3' => [
                    'name' => '',
                    'numb' => '',
                    'info' => '',
                    'color' => '',
                ],
                'slice4' => [
                    'name' => '',
                    'numb' => '',
                    'info' => '',
                    'color' => '',
                ],
                'slice5' => [
                    'name' => '',
                    'numb' => '',
                    'info' => '',
                    'color' => '',
                ],
            ];
        }
//        foreach ($array as $row ):?>
            <tr>
                <?php
                $i = 1;
                foreach ($array as $row ):?>
                    <th>
                        <h2><?= Yii::t('app', 'Slice') ?> <?= $i ?></h2>
                    </th>
                <?php
                    $i++;
                endforeach ?>
            </tr>
            <tr>
                <td>
                    <h3><?= Yii::t('app', 'Name') ?></h3>
                </td>
            </tr>
            <tr>
                <?php
                $i = 1;
                foreach ($array as $row ):?>
                    <td>
                        <?= Html::input('text', 'name-'.$i.'', $row->name, ['class' => 'form-control']) ?>
                    </td>
                <?php
                    $i++;
                endforeach ?>
            </tr>
            <tr>
                <td>
                    <h3><?= Yii::t('app', 'Info') ?></h3>
                </td>
            </tr>
        <tr>
            <?php
            $i = 1;
            foreach ($array as $row ):?>
                <td>
                    <?= Html::input('text', 'info-'.$i.'', $row->info, ['class' => 'form-control']) ?>
                </td>
            <?php
                $i++;
            endforeach ?>
        </tr>
            <tr>
                <td>
                    <h3><?= Yii::t('app', 'Color') ?></h3>
                </td>
            </tr>
        <tr>
            <?php
            $i = 1;
            foreach ($array as $row ):?>
                <td>
                    <?= Html::input('text', 'color-'.$i.'', $row->color, ['class' => 'form-control']) ?>
                </td>
            <?php
                $i++;
            endforeach ?>
        </tr>
            <tr>
                <td>
                    <h3><?= Yii::t('app', 'Number') ?></h3>
                </td>
            </tr>
        <tr>
            <?php
            $i = 1;
            foreach ($array as $row ):?>
                <td>
                    <?= Html::input('text', 'numb-'.$i.'', $row->numb, ['class' => 'form-control']) ?>
                </td>
            <?php
                $i++;
            endforeach ?>
        </tr>
    </table>
    <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary']) ?>
    <?= Html::endForm(); ?>
</div>
