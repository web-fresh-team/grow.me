<?php
use yii\helpers\Html;

$settings = Yii::$app->settings;

?>
<div class="content-view">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Yii::t('app', 'Content') ?></h1>
        </div>
    </div>
    <table class="content-table table table-striped table-bordered">
        <tr>
            <th><?= Yii::t('app', 'Key') ?></th>
            <th><?= Yii::t('app', 'Text') ?></th>
            <th><?= Yii::t('app', 'Action') ?></th>
        </tr>
        <tr>
            <td><h3>sub-slogan</h3></td>
            <td><?= $settings->get('index.slogan') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.slogan'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>main-slogan</h3></td>
            <td><?= $settings->get('index.main-slogan') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.main-slogan'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>top-info-text</h3></td>
            <td><?= $settings->get('index.top-info-text') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.top-info-text'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>info-text</h3></td>
            <td><?= $settings->get('index.info-text') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.info-text'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>bottom-title</h3></td>
            <td><?= $settings->get('index.bottom-title') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.bottom-title'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>bottom-text</h3></td>
            <td><?= $settings->get('index.bottom-text') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.bottom-text'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>link-to-graph</h3></td>
            <td><?= $settings->get('index.link-to-graph') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.link-to-graph'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td><h3>graph-title</h3></td>
            <td><?= $settings->get('index.graph-title') ?></td>
            <td><?= Html::a(Yii::t('app', 'Change text'), ['admin/update', 'key' =>'index.graph-title'], ['class' => 'btn btn-primary'])?></td>
        </tr>
        <tr>
            <td>
                <h3>graph-rows</h3>
            </td>
            <td>
                
            </td>
            <td>
                <?= Html::a(Yii::t('app', 'Change text'), [
                    'admin/graph',
                    'title' => 'index.graph-title',
                    'slicesRows' => 'index.graph-rows',
                ], ['class' => 'btn btn-primary']) ?>
            </td>
        </tr>
    </table>
</div>
