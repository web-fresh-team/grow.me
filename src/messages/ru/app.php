<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Risks' => 'Риски',
    'Terms' => 'Условия',
    'Additionally' => 'Дополнительно',
    'Categories List' => 'Список категорий',
    'Change donut-graph at index page' => 'Изменить диаграмму-бублик на главной',
    'Change graph' => 'Изменить диаграмму',
    'Crowdfunding' => 'Кроудфаундинг',
    'Dashboard' => 'Панель управления',
    'Exit' => 'Выход',
    'Investment' => 'Инвестиция',
    'Jobs' => 'Работа',
    'Kiev, Ukraine' => 'Киев, Украина',
    'Legal Stuff' => 'Юридический Материал',
    'New password was saved.' => 'Новый пароль сохранен',
    'Press' => 'Нажмите',
    'Raise Funding' => 'Финансирование',
    'Search' => 'Поиск',
    'Sent invest' => 'Отправить инвестицию',
    'Update {modelClass}: ' => 'Обновление {modelClass}',
    'Videos' => 'Видео',
    '${amount} invested' => '${amount} инвестировано',
    'About' => 'Про',
    'About Me' => 'Про меня',
    'About us' => 'О нас',
    'Accept' => 'Принять',
    'Accepted' => 'Потвержденно',
    'Account activation' => 'Активация аккаунта',
    'Action' => 'Действие',
    'Active' => 'Активный',
    'Add friend' => 'Добавить',
    'Add photo' => 'Добавить фото',
    'Address' => 'Адрес',
    'Advertisement' => 'Реклама',
    'All categories' => 'Все категории',
    'All issues related to the deployment of advertising on the site and community meetings to direct {email}' => 'Все вопросы, связанные с размещением рекламы на сайте и встречах сообщества направляйте на {email}',
    'Amount' => 'Сумма',
    'Amount: ' => 'Сумма: ',
    'Approved' => 'Потвержденно',
    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить этот элемент?',
    'Auth Key' => 'Код аутентификации',
    'Bank' => 'Банк',
    'Blog' => 'Блог',
    'Blog Posts' => 'Статьи',
    'Body' => 'Введите сюда ваш текст',
    'Browse' => 'Просмотр',
    'Budget' => 'Бюджет',
    'By bank' => 'За банком',
    'By bank ascendind' => 'За банком по росту',
    'By bank descendind' => 'За банком по убыванию',
    'By likes ascendind' => 'За популярностью по росту',
    'By likes descendind' => 'За популярностью по убыванию',
    'Cancel' => 'Отмена',
    'Categories' => 'Категории',
    'Category' => 'Категория',
    'Category ID' => 'Категория',
    'Change' => 'Изменить',
    'Change text' => 'Изменить текст',
    'Check your email for further instructions.' => 'Проверьте ваш email для дальнейших инструкций',
    'Choose status' => 'Выберите статус',
    'Color' => 'Цвет',
    'Comments' => 'Комментарии',
    'Comments list' => 'Список комментариев',
    'Confirm Token' => 'Токен подтверждения',
    'Contact' => 'Контакты',
    'Contact us' => 'Обратная связь',
    'Contacts' => 'Контакты',
    'Content' => 'Контент',
    'Create' => 'Создать',
    'Create Blog Post' => 'Создать статью',
    'Create Category' => 'Создать категорию',
    'Create Comment' => 'Создать комменарий',
    'Create Feedback' => 'Создать отзыв',
    'Create Project' => 'Создать проект',
    'Create User' => 'Создать пользователя',
    'Created' => 'Создан',
    'Created At' => 'Создан',
    'Delete' => 'Удалить',
    'Delete friend' => 'Удалить друга',
    'Delete project' => 'Удалить проект',
    'Denied' => 'Отменен',
    'Description' => 'Описание',
    'Description and characteristics' => 'Описание',
    'Dialog with ' => 'Диалог с ',
    'Do you really want to delete friend?' => 'Вы точно хотите удалить друга?',
    'Do you really want to delete this project?' => 'Вы точно хотите удалить этот проект?',
    'Edit' => 'Изменить',
    'Email' => 'Email',
    'Email with confirm link was sent to ' => 'Email с подтверждающей ссылкой был отправлен на ',
    'Email {value} is already taken.' => 'Email {value} уже успользуется',
    'Embodied' => 'Внедрен',
    'Enter password again' => 'Введите пароль снова',
    'Except bank' => 'Без банка',
    'Except likes' => 'Без популярности',
    'FAQ' => 'FAQ',
    'Feedbacks' => 'Отзывы',
    'First Name' => 'Имя',
    'First name' => 'Имя',
    'Follow the link below to confirm your email address and activate account:' => 'Следуйте ссылкой ниже для подтвержления вашей электронной почты и активациии аккаунта',
    'Follow the link below to reset your password:' => 'Следуйте ссылкой ниже для сброса вашего пароля',
    'For deleting' => 'К удалению',
    'From ID' => 'От',
    'Glossary' => 'Глоссарий',
    'Grow Me' => 'Grow Me',
    'Guide' => 'Гайд',
    'Hello, {username}' => 'Привет, {username}',
    'Home' => 'Домой',
    'I have an idea' => 'У меня есть идея',
    'I have an idea for a company' => 'У меня есть идея для компании',
    'I want to invest' => 'Я инвестор',
    'ID' => 'ID',
    'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.' => 'Если у вас есть деловое предложение или другие вопросы, пожалуйста, заполните следующую форму, чтобы связаться с нами. Спасибо.',
    'Images' => 'Картинки',
    'In progress' => 'В процессе',
    'Incorrect email or password' => 'Неправильный email или пароль',
    'Info' => 'Информация',
    'Input your text' => 'Введите ваш текст',
    'Invalid password reset token or token time has expired.' => 'Неправильный токен Сброса пароля или истекший',
    'Invest' => 'Инвестировать',
    'Invest requests' => 'Входящие инвестиции',
    'Investing' => 'Инвестирование',
    'Investment was accepted' => 'Инвестиция была принята',
    'Investments' => 'Инвестиции',
    'Investor' => 'Инвеститор',
    'Key' => 'Ключ',
    'Last Name' => 'Фамилия',
    'Last name' => 'Фамилия',
    'Likes Amount' => 'Колличество лайков',
    'Log in' => 'Войти',
    'Login' => 'Войти',
    'Logout' => 'Выйти',
    'My projects count' => 'Количество моих проектов',
    'Name' => 'Имя',
    'Need more comments' => 'Нужно больше комментариев',
    'New' => 'Новый',
    'New Friends' => 'Новые друзья',
    'No accepted invest' => 'Нет подтвержденных инвестиций',
    'No comments yet' => 'Пока нет комментариев',
    'No posts here yet' => 'Здесь пока нет статей',
    'Nobody sent invest' => 'Никто не присылал инвестиций',
    'Not read' => 'Не читать',
    'Number' => 'Число',
    'OK' => 'OK',
    'Ok' => 'Ок',
    'Our Profile' => 'Наш профиль',
    'Parent ID' => 'Родительский',
    'Password' => 'Пароль',
    'Password Again' => 'Пароль снова',
    'Password Hash' => 'Хеш пароля',
    'Password Reset Token' => 'Токен Сброса пароля',
    'Password reset token cannot be blank.' => 'Токен восстановления пароля не может быть пустым',
    'Passwords are not equal.' => 'Пароли не совпадают',
    'Personal area' => 'Личный кабинет',
    'Phone' => 'Телефон',
    'Photo' => 'Фото',
    'Photos' => 'Фотографии',
    'Popular' => 'Пополярный',
    'Popular on Grow.Me' => 'Популярное на Grow.Me',
    'Project ID' => 'Проект',
    'Project owner' => 'Владелец проекта',
    'Projects' => 'Проекты',
    'Read' => 'Читать',
    'Read more' => 'Больше',
    'Reset Password' => 'Сброс пароля',
    'Role' => 'Роль',
    'Save Password' => 'Сохранить пароль',
    'Send' => 'Отправить',
    'Send invest' => 'Отправить инвестицию',
    'Sent' => 'Отправлено',
    'Short description' => 'Короткое описание',
    'Sign In' => 'Войти',
    'Sign Up' => 'Зарегистрироваться',
    'Slice' => 'Разрез',
    'Slug' => 'Slug',
    'Something went wrong' => 'Что-то пошло не так',
    'Sorry, we are unable to reset password for email provided.' => 'Простите, мы не можем сбросить пароль за предоставленным email`ом',
    'Start' => 'Начать',
    'Startup catalog' => 'Каталог стартапов',
    'Status' => 'Статус',
    'Subject' => 'Тема',
    'Text' => 'Текст',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Благодарим Вас за обращение к нам. Мы ответим вам как можно скорее.',
    'The collected amount' => 'Собранная сумма',
    'There is no user with email' => 'Нет пользователя с email`ом',
    'This email already taken.' => 'Этот email уже занят',
    'This user does not have investments yet' => 'У этого юзера еще нету инвестиций в проекты',
    'This user does not have projects yet' => 'У этого юзера еще нету проектов',
    'This username already taken.' => 'Имя пользователя уже занято',
    'Title' => 'Заголовок',
    'Title graph' => 'Заголовок диаграммы',
    'To ID' => 'Кому',
    'Topic' => 'Тема',
    'Update' => 'Обновить',
    'Update Blog Post: ' => 'Обновить статью: ',
    'Update Category: ' => 'Обновить категорию: ',
    'Update Comment: ' => 'Обновить комментарий: ',
    'Update Feedback: ' => 'Обновить отзыв: ',
    'Update Project: ' => 'Обновить проект: ',
    'Update User: ' => 'Обновить пользователя: ',
    'Updated At' => 'Обновлен',
    'User' => 'Пользователь',
    'User ID' => 'Пользователь',
    'User: ' => 'Пользователь: ',
    'Username' => 'Имя пользователя',
    'Username {value} is already taken.' => 'Имя пользователя {value} уже занято',
    'Users' => 'Пользователи',
    'Verification Code' => 'Код верификации',
    'View' => 'Представление',
    'Why Grow.me?' => 'Почему Grow.me?',
    'Write to friend' => 'Написать другу',
    'Wrong password reset token.' => 'Неправильный токен зброса пароля',
    'You already send request for this user' => 'Вы уже отправили запрос этому пользователю',
    'You are already friend with this user' => 'Вы уже друг с этим пользователем',
    'You don`t have friend yet' => 'У Вас еще нету друзей',
    'You don`t have investments yet' => 'У Вас еще нету инвестиций',
    'You don`t have projects yet' => 'У Вас еще нету проектов',
    'You have successfully registered.' => 'Вы были успешно зарегистрированны',
    'You have successfully reset your password.' => 'Вы успешно сбросили пароль',
    'You sent friend request to {futureFriend}' => 'Вы отправили запрос на дружбу {futureFriend}',
    'You will be redirected to user page.' => 'Вы будете перенаправлены на страницу пользователя',
    'Your comment successfully sent!' => 'Ваш комментарий успешно отправлен!',
    'Your sum' => 'Ваша сумма',
    'list of Friends' => 'список друзей',
    'list of ideas' => 'Список идей',
    'list of invest' => 'Список инвестиций',
    '{attribute} should be biggest than 0' => '{attribute} должен быть больше 0',
    '{username}\'s page ' => 'Страница пользователя {username}',
    '© 2010-{year} All Rights Reserved' => '© 2010-{year} Все права защищены',
];
