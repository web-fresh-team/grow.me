<?php

namespace app\controllers;

use alexBond\thumbler\Thumbler;
use app\components\ConsoleAdapter;
use app\models\ContactForm;
use app\models\SortProject;
use app\modules\blog\models\BlogPost;
use app\modules\category\models\Category;
use app\modules\comment\models\Comment;
use app\modules\project\models\Project;
use app\modules\feedback\models\Feedback;
use app\modules\user\models\User;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\Friend;
use app\components\Helper;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $projects = Project::find()
            ->orderBy(['likes_amount' => SORT_DESC])
            ->limit(3)
            ->all();
        $newProjects = Project::find()
            ->where(['status' => 2])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(3)
            ->all();

        $feedBacks = Feedback::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(18)
            ->all();


        return $this->render('index', [
            'projects' => $projects,
            'newProjects' => $newProjects,
            'feedBacks' => $feedBacks,
        ]);
    }

    public function actionCategory()
    {
        return $this->render('category');
    }

    public function actionIdeaCreate()
    {
        $model = new Project();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->identity->getId();
            if ($model->validate() && $model->save()) {
                $model->createBlogPost(BlogPost::STATUS_APPROVED);
                return $this->redirect(['idea', 'slug' => $model->slug]);
            } else {
                return $this->render('idea-edit', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('idea-edit', [
                'model' => $model,
            ]);
        }
    }

    public function actionIdeaEdit($slug)
    {
        $model = Project::find()->where(['slug' => $slug])->one();
        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            if (!isset($post['Project']['photos'])) {
                $post['Project'] = array_merge($post['Project'],['photos' => []]);
            }
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(['idea', 'slug' => $model->slug]);
        } else {
            return $this->render('idea-edit', [
                'model' => $model,
            ]);
        }

    }

    public function actionIdea($slug, $counter = 0)
    {
        $currentProject = Project::find()->where(['slug' => $slug])->one();
        if (!is_null($currentProject)) {
            if (Yii::$app->request->isAjax) {
                $counter = intval($counter) + 3;
                $comments = $currentProject->getLastComments($counter)->all();
                $result = [];
                $disable = '';

                foreach ($comments as $comment) {
                    $result[] = [
                        'username' => $comment->user->username,
                        'text' => $comment->text,
                        'added' => $comment->getTimeCreated(),
                    ];
                }

                if ($counter + 3 < intval($currentProject->getCommentsAmount())) {
                    $disable = false;
                } else {
                    $disable = true;
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'counter' => $counter,
                    'comments' => $result,
                    'disable' => $disable,
                ];

            }
            if ($counter == 0) {
                return $this->render('idea', [
                    'project' => $currentProject,
                ]);
            }
        } else {
            return $this->redirect('/site/index');
        }
    }

    public function actionAddComment($projectId, $text)
    {
        $model = new Comment();
        if (Yii::$app->request->isAjax) {
            if (!Yii::$app->user->isGuest) {
                $model->text = $text;
                $model->user_id = Yii::$app->user->identity->getId();
                $model->project_id = $projectId;
                $model->parent_id = '';
                if ($model->validate() && $model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $message = Yii::t('app', 'Your comment successfully sent!');
                    return [
                        'message' => $message,
                    ];
                }
            } else {
                return $this->redirect('/login');
            }

        }
    }

    public function actionBlog()
    {
        $posts = BlogPost::findAll(['status' => BlogPost::STATUS_APPROVED]);

        return $this->render('blog', [
            'posts' => $posts
        ]);
    }

    public function actionInvest()
    {
        return $this->render('invest');
    }

    public function actionInvestor()
    {
        return $this->render('investor');
    }

    public function actionOtherPage()
    {
        return $this->render('other-page');
    }

    public function actionChangePhoto($path)
    {
        if (Yii::$app->request->isAjax) {
            $username = Yii::$app->user->identity->username;
            /* @var $model User */
            $model = User::findOne(['username' => $username]);

            $model->photo = $path;

            if ($model->validate() && $model->save()) {
                return $this->redirect('/personal-area/' . $model->username, [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionPersonalArea($username)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/login');
        }

        /* @var $model User */
        $model = User::findOne(['username' => $username]);

        if ($model === null) {

            return $this->redirect('/error');
        }
        $friendship = new Friend();
        $friendship->from_id = Yii::$app->user->identity->getId();
        $friendship->to_id = intval($model->getId());
        $existFriendship = Friend::findOne([
            'from_id' => $friendship->from_id,
            'to_id'   => $friendship->to_id,
        ]);
        if($existFriendship)
            $existFriendship = 1;
        else
            $existFriendship = 0;

        $projects = [];
        $countProject = 0;
        $amount = 0;
        if ($model->role == User::ROLE_OWNER) {
            $projects = $model->projects;
            foreach($projects as $project) {
                /** @var $project Project */
                if($project->status != Project::STATUS_FOR_DELETING) {
                    $countProject++;
                    $amount += $project->getAmount();
                }
            }
        } elseif ($model->role == User::ROLE_INVESTOR) {
            foreach ($model->investments as $investment) {
                array_push($projects, $investment->project);
            }
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $model[$post['name']] = $post['value'];
            if ($model->validate() && $model->save()) {
                if ($post['name'] == 'username') {
                    return $this->redirect('/personal-area/'. $model->username, 200);
                } else {
                    return [
                        'model' => $model
                    ];
                }
            } else {
                Yii::$app->response->setStatusCode(422);
                return $model->getFirstError($post['name']);
            }
        }

        $page = (Yii::$app->user->identity->getId() == $model->id) ? 'personal-area' : 'user';

        return $this->render($page, [
            'model' => $model,
            'projects' => $projects,
            'countProjects' => $countProject,
            'amount' => $amount,
            'existFriendship' => $existFriendship,
        ]);
    }

    public function actionDeleteProject($project_id)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {
                /* @var $project Project */
                $project = Project::findOne(['id' => $project_id]);
                $owner = Yii::$app->user->identity->getId();
                if($project->user_id == $owner) {
                    $project->status = Project::STATUS_FOR_DELETING;
                    $project->save();
                }
            }
        } else {
            return $this->redirect('/login');
        }
    }

    public function actionLikeProject($project_id)
    {
        if (Yii::$app->request->isAjax) {
            /* @var $project Project */
            $project = Project::findOne(['id' => $project_id]);
            /* @var $user User */
            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
            if ($project->isLikedByCurrentUser()) {
                $project->dislikeByCurrentUser();
            } else {
                $project->likeByCurrentUser();
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'amount' => $project->likesAmount,
            ];
        }

    }

    public function actionContactUs()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact-us', [
                'model' => $model,
            ]);
        }
    }

    public function actionAdvertisement()
    {
        return $this->render('advertisement');
    }

    public function actionStart()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        /* @var $user User */
        $user = Yii::$app->user->identity;
        if ($user->role == User::ROLE_OWNER) {
            return $this->redirect('/idea-create');
        } elseif ($user->role == User::ROLE_INVESTOR) { //
            return $this->redirect('/search');
        } else {
            return $this->goHome();
        }

    }

    public function actionRegistration()
    {
        return $this->render('registration');
    }

    public function actionRegistrationOfInvest()
    {
        return $this->render('registration-of-invest');
    }

    public function actionStartup()
    {
        return $this->render('startup');
    }

    public function actionRisks()
    {
        return $this->render('risks');
    }

    public function actionTerms()
    {
        return $this->render('terms');
    }

    public function actionThumb($url, $width, $height)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        return Helper::getThumb($url, $width, $height);
    }

    public function actionSearch()
    {

        $model = new SortProject();

        $model->load(Yii::$app->request->get());

        $projects = $model->getProjects();

        return $this->render('search-page', [
            'model' => $model,
            'projects' => $projects
        ]);
    }

    /**
     * So danger
     */
    public function actionMigrate() {
        ConsoleAdapter::runConsoleActionFromWebApp('migrate', ['migrationPath' => '@app/migrations/', 'interactive' => false]);
    }


}
