<?php

namespace app\controllers;

use app\models\Investment;
use Yii;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Controller;


class InvestmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCreateInvest($projectId, $amount)
    {
        if (Yii::$app->request->isAjax) {
            $investment  = new Investment();

            if($amount < 0) {
                $done = false;
                return [
                    'done' => $done,
                ];
            }

            $investment->amount = $amount;
            $investment->project_id = $projectId;
            $investment->user_id = Yii::$app->user->identity->getId();
            $investment->status = Investment::STATUS_SENT;

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($investment->validate() && $investment->save()) {
                $message = Yii::t('app', 'Investment was accepted');
                $done = true;
            } else {
                $message = Yii::t('app', 'Something went wrong');
                $done = false;
            }


            return [
                'done' => $done,
                'message' => $message,
            ];
        }
    }

    public function actionAcceptInvest($investId)
    {
        if (Yii::$app->request->isAjax) {
            $investment  = Investment::findOne(['id' => $investId]);
            $investment->status = Investment::STATUS_ACCEPTED;

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($investment->validate() && $investment->save()) {
                $message = Yii::t('app', 'Investment was accepted');
                $done = true;
            } else {
                $message = Yii::t('app', 'Something went wrong');
                $done = false;
            }

            return [
                'done' => $done,
                'message' => $message,
            ];
        }
    }


}
