<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\user\models\User;
use app\models\Friend;
use yii\web\Response;
use yii\filters\VerbFilter;
use Yii;

class FriendController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionDeleteFriend($toId)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {
                $fromId = Yii::$app->user->identity->getId();
                /* @var $friendship Friend */
                /* @var $friendship2 Friend */
                $friendship = Friend::findOne([
                    'from_id' => $fromId,
                    'to_id' => $toId,
                ]);

                $friendship2 = Friend::findOne([
                    'from_id' => $toId,
                    'to_id' => $fromId,
                ]);
                $friendship->delete();
                $friendship2->delete();
            }
        } else {
            return $this->redirect('/login');
        }
    }

    public function actionAcceptFriend($fromId)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {
                $toId = Yii::$app->user->identity->getId();
            /* @var $friendship Friend */
            $friendship = Friend::findOne([
                'from_id' => $fromId,
                'to_id' => $toId,
            ]);
            if ($friendship->status == 0) {
                $friendship->status = 1;
                $friendship->save();

                $newFriendship = new Friend();
                $newFriendship->status = 1;
                $newFriendship->from_id = $toId;
                $newFriendship->to_id = $fromId;
                $newFriendship->save();

                return 1;
            }
            }
        } else {
            return $this->redirect('/login');
        }
    }

    public function actionDeniedFriend($fromId)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {
                $toId = Yii::$app->user->identity->getId();

                /* @var $friendship Friend */
                $friendship = Friend::findOne([
                    'from_id' => $fromId,
                    'to_id' => $toId,
                ]);
                if ($friendship->status == 0) {
                    $friendship->delete();
                }
            }
        }
    }

    public function actionAddToFriend($futureFriend)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {

                $model = new Friend();
                $model->from_id = Yii::$app->user->identity->getId();
                $model->to_id = intval($futureFriend);
                $model->status = Friend::STATUS_SENT;

                /* @var $friendship Friend */
                $friendship = Friend::findOne([
                    'from_id' => $model->from_id,
                    'to_id'   => $model->to_id,
                ]);
                Yii::$app->response->format = Response::FORMAT_JSON;

                if($friendship) {
                    if($friendship->status == 0) {
                        $message = Yii::t('app', 'You already send request for this user');

                        return [
                            'message' => $message,
                        ];
                    }
                    else {
                        $message = Yii::t('app', 'You are already friend with this user');

                        return [
                            'message' => $message,
                        ];
                    }
                }
                elseif ($model->validate() && $model->save()) {
                    $message = Yii::t('app', 'You sent friend request to {futureFriend}', ['futureFriend' => User::findOne(['id' => $futureFriend])->username]);

                    return [
                        'message' => $message,
                    ];
                }
            }
        } else {
            return $this->redirect('/login');
        }
    }

    public function actionWriteMessage()
    {

    }
}