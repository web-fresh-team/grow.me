<?php

namespace app\controllers;

use app\models\Message;
use Yii;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\modules\user\models\User;


class MessageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLoadMessages($friendId) {
        if (Yii::$app->request->isAjax) {
            $currentUser = Yii::$app->user->identity->getId();
            $myMsg = Message::find()->andWhere(['from_id'=>$currentUser, 'to_id'=>$friendId])->all();
            $hisMsg = Message::find()->andWhere(['from_id'=>$friendId, 'to_id'=>$currentUser])->all();
            $messages = array_merge($myMsg, $hisMsg);

            usort($messages, function ($a, $b){
                $a = $a->created_at;
                $b = $b->created_at;

                if ( $a == $b) {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            });

            $msgs = [];
            foreach ($messages as $message) {
                $isMy = false;
                if ($message->from_id == Yii::$app->user->identity->getId()){
                    $isMy = true;
                }

                $msgs[] = [
                    'isMy' => $isMy,
                    'text' => $message->text,
                    'sent' => $message->timeCreated,
                ];
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'messages' => $msgs,
            ];
        }
    }

    public function actionSendMessage($friendId,$text) {
        if (Yii::$app->request->isAjax) {
            $model = new Message();

            $model->from_id = Yii::$app->user->identity->getId();
            $model->to_id = $friendId;
            $model->text = $text;
            $model->status = Message::STATUS_NOTREAD;

            if ($model->validate() && $model->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'text' => $model->text,
                    'sent' => $model->timeCreated,
                ];
            }
        }
    }

    public function actionChat($id=0)
    {
        if (!Yii::$app->user->isGuest) {

            $user = User::findOne(['id' => Yii::$app->user->identity->getId()]);

            return $this->render('chat', [
                'user' => $user,
                'friendId' => $id,
            ]);
        } else {
            return $this->redirect('/login');
        }

    }

}
