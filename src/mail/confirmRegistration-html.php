<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/confirm-email', 'token' => $user->confirm_token]);
?>
<div class="password-reset">
    <p><?= Yii::t('app','Hello, {username}', ['username' => $user->username]) ?></p>

    <p><?= Yii::t('app', 'Follow the link below to confirm your email address and activate account:') ?></p>

    <p><?= Html::a(Html::encode($confirmLink), $confirmLink) ?></p>

    <p><?= Yii::t('app', 'You will be redirected to user page.') ?></p>
</div>
