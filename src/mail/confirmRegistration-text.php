<?php

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/confirm-email', 'token' => $user->confirm_token]);
?>
Hello, <?= $user->username ?>,

Follow the link below to confirm your email address and activate account:

<?= $confirmLink ?>

You will be redirected to user page.
