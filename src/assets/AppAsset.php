<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.css',
        'css/main.css',
        'css/site.css',
        'http://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin,cyrillic-ext',
    ];
    public $js = [
        'js/vendor/d3.v3.min.js',
        'js/vendor/modernizr-2.8.3.min.js',
        'js/main.js',
        'js/tools.js',
        'js/scripts.js',
        'js/alert.js',
    ];
    public $depends = [
        '\rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
    ];
}
