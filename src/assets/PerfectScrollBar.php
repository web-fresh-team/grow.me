<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Andrew Prohorovych
 */
class PerfectScrollBar extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/perfect-scrollbar/css/perfect-scrollbar.css',
    ];
    public $js = [
        'plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
        'plugins/perfect-scrollbar/js/perfect-scrollbar.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}