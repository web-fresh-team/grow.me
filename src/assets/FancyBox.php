<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Andrew Prohorovych
 */
class FancyBox extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/fancybox/source/jquery.fancybox.css',
    ];
    public $js = [
        'plugins/fancybox/source/jquery.fancybox.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}