<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 8/5/15
 * Time: 3:54 PM
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Andrew Prohorovych
 */
class LightSlider extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/lightslider-master/src/css/lightslider.css',
    ];
    public $js = [
        'plugins/lightslider-master/src/js/lightslider.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}