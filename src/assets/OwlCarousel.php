<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Andrew Prohorovych
 */
class OwlCarousel extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/owl.carousel/assets/owl.carousel.css',
        'plugins/owl.carousel/assets/owl.theme.default.min.css',
    ];
    public $js = [
        'plugins/owl.carousel/owl.carousel.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}