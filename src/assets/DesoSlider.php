<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Andrew Prohorovych
 */
class DesoSlider extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/deso/css/jquery.desoslide.min.css',
        'plugins/deso/css2/css/vendor/animate/animate.min.css',
        'plugins/deso/css2/css/vendor/magic/magic.min.css',
    ];
    public $js = [
        'plugins/deso/js/jquery.desoslide.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}