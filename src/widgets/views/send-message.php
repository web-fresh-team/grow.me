<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'add-comment',
    'class' => 'yii\widgets\ActiveForm',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'class' => 'yii\widgets\ActiveField',
        'template' => "{input}\n{hint}\n{error}",
        'inputOptions' => [
            'class' => 'registration-input',
        ],
        'errorOptions' => [
            'class' => 'for-error',
        ],
        'options' => [
            'class' => 'input-wrapper'

        ]
    ],
    'errorCssClass' => 'error',

]);

?>

    <div class="comment-form">

        <?= $form->field($model, 'text', [
            'inputOptions' => [
                'placeholder' => Yii::t('app', 'Input your text')
            ]
        ])->textarea() ?>

        <div class="send-button button">
            <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'button send-ajax']) ?>
        </div>

        <?php if ($type == "comment") : ?>
            <i class="fa fa-arrow-up"></i>
        <?php elseif ($type == "message"): ?>
            <div class="clearfix"></div>
        <?php endif ?>

    </div>


<?php ActiveForm::end() ?>