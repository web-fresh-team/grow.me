<?php

namespace app\widgets;


use app\models\Message;
use app\modules\comment\models\Comment;
use yii\base\Widget;


class SendMessage extends Widget
{

    public $modelName = '';

    public function run()
    {
        $model = '';

        if ($this->modelName == 'comment') {
            $model = new Comment();
        } elseif ($this->modelName == 'message') {
            $model = new Message();
        }

        return $this->render('send-message', [
            'model' => $model,
            'type' =>  $this->modelName,
        ]);


    }

}