<?php
use yii\helpers\Url;
use app\assets\DesoSlider;
use app\assets\FancyBox;
use app\assets\PerfectScrollBar;
use app\widgets\SendMessage;

/* @var $this yii\web\View */
/* @var $friendId yii\web\View */

/* @var $project \app\controllers\SiteController */

/* @var $comment \app\modules\comment\models\Comment; */

$this->title = Yii::$app->name;


FancyBox::register($this);
DesoSlider::register($this);
PerfectScrollBar::register($this);

$this->registerJs(<<<JS

    jQuery(document).ready(function () {

        var msgBlock = jQuery('.hidden').html();

        //Scroll bar for list friends
        var friends = jQuery('.friend-list .bottom-part ul');
        friends.perfectScrollbar();

        //Scroll bar for list friends
        var messagesBlock = jQuery('.message-block .chat .messages-place .messages .friend-message');
        messagesBlock.perfectScrollbar();

        //Load messages with specific friend
        var LoadMessages = function(friendId){
            jQuery.ajax({
                url: '/message/load-messages',
                data: { 'friendId' : friendId }
            }).done (function(data){
                var toClean = jQuery('.friend-message');
                var messagePlace = toClean.find('.clearfix');
                toClean.find('.message-from, .message-to').remove();
                var messages =  data.messages;
                jQuery.each(messages,function(key,val){
                    var newBlock = msgBlock;
                    if (val.isMy) {
                        newBlock = newBlock.replace('super-message','message-from');
                    } else {
                        newBlock = newBlock.replace('super-message','message-to');
                    }
                    newBlock = newBlock.replace('#text', val.text).replace('#time', val.sent);
                    messagePlace.before(newBlock);
                });
                messagesBlock.scrollTop(messagesBlock.prop( "scrollHeight" ));
                messagesBlock.perfectScrollbar('update');
            });
        };

        //Active list item
        var friendLink = jQuery('.friend-list .bottom-part ul li a');
        friendLink.click(function(e){
            event.preventDefault(e);
            var active = 'active-item';
            jQuery.each(friendLink, function(key,val) {
               var current =  jQuery(val).parent();
               if (current.hasClass(active)) { current.toggleClass(active) }
            });
            var link = jQuery(this);
            link.parent().toggleClass(active);
            var friendId = link.attr('data-friend');
            jQuery('#add-comment').attr('data-friend',friendId);
            jQuery('.message-block .title p span').html(link.html());
            LoadMessages(friendId);
        });

        // Send message   send-ajax
         var SendMessages = function(friendId, text){
            jQuery.ajax({
                url: '/message/send-message',
                data: { 'friendId' : friendId, 'text' : text }
            }).done (function(data){
            //var text = data.text.trim();
            var message = data.text.trim();
            if (message) {
                var text = jQuery('.registration-input').val('');
                var newBlock = msgBlock;
                newBlock = newBlock.replace('super-message','message-from')
                .replace('#text', data.text).replace('#time', data.sent);
                var messagePlace = jQuery('.friend-message .clearfix');
                messagePlace.before(newBlock);
                messagesBlock.scrollTop(messagesBlock.prop( "scrollHeight" ));
                messagesBlock.perfectScrollbar('update');
            }

            });
        };

        jQuery('#add-comment').submit(function(e){
                event.preventDefault(e);
                var friendId = jQuery(this).attr('data-friend');
                if (friendId != undefined) {
                    var text = jQuery('.registration-input').val();
                    SendMessages(friendId,text);
                } else {
                    alert('choose friend');
                }
                return false;
        });
        var element = $('*[data-friend="'+$friendId+'"]');
        element.trigger('click');
    });

JS
);


?>
<div class="hidden">
    <div class="super-message">
        #text<!--<div class="time">#time</div>-->
    </div>
</div>
<div class="wrapper-footer">
    <div class="content">
        <div class="container">
            <div class="friend-list">
                <div class="title">
                    <p><?=Yii::t('app','list of Friends')?></p>
                </div>
                <div class="bottom-part">
                    <ul>
                        <?php foreach ($user->activeFriends as $friend): ?>
                            <li>
                                <a href="" data-friend="<?= $friend->id ?>"><?= $friend->username ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="message-block">
                <div class="chat">
                    <div class="title">
                        <p><?= Yii::t('app','Dialog with ') ?><span>...</span></p>
                    </div>
                    <div class="messages-place">
                        <div class="messages">
                            <div class="friend-message">
                                <div class="clearfix"></div>
                            </div>
                            <div class="write-message">
                                <?= SendMessage::widget(['modelName' => 'message']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
