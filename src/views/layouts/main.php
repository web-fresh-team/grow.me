<?php
use app\assets\AppAsset;
use yii\helpers\Html;
use app\modules\user\models\User;

/* @var $this \yii\web\View */
/* @var $content string */

rmrevin\yii\fontawesome\AssetBundle::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1648394312044399',
            xfbml      : true,
            version    : 'v2.5'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<?php if (Yii::$app->controller->action->uniqueId === 'site/index'): ?>
    <header class="main-header">
        <div class="container">
            <div class="logo">
                <a href="/"><img src="/img/logo-white1.png" alt="logo"/></a>
            </div>
            <div class="menu-toggle" onclick="app.mainMenu.toggle()">
                <i class="fa fa-bars"></i>
            </div>
            <nav class="main-menu" id="main-menu">
                <ul>
                    <?php if (Yii::$app->user->isGuest) : ?>
                        <li><a href="/"><?= Yii::t('app','Home')?></a></li>
                    <?php else : ?>
                        <li><a href="/personal-area/<?= Yii::$app->user->identity->username ?>"><?= Yii::t('app','Personal area')?></a></li>
                    <?php endif ?>


                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <?php if (Yii::$app->user->identity->role == User::ROLE_OWNER): ?>
                            <li>
                                <?= Html::a(Yii::t('app', 'Create Project'), ['/idea-create']) ?>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>

                    <li>
                        <?= Html::a(Yii::t('app', 'Contact'), ['/contact-us']) ?>
                    </li>

                    <li>
                        <?= Html::a(Yii::t('app', 'Startup catalog'), ['/search']) ?>
                    </li>

                    <li>
                        <?= Html::a(Yii::t('app', 'Blog'), ['/blog']) ?>
                    </li>

                    <?php if (Yii::$app->user->isGuest): ?>
                        <li>
                            <a href='/login'>
                                <?= Yii::t('app', 'Login') ?>
                            </a>
                        </li>
                    <?php else : ?>
                        <li>
                            <a href="/logout" data-method="post">
                                <?= Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')' ?>
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
                <div class="clearfix"></div>
            </nav>
            <div class="clearfix"></div>
        </div>
    </header>
<?php else : ?>
    <header class="category header-norm">
        <div class="bg2">
            <div class="container">
                <div class="top">
                    <div class="logo">
                        <a href="/"><img src="/img/logo-bulb-darkgray.png" alt="logo"/></a>
                    </div>
                    <div class="menu-toggle" onclick="app.mainMenu.toggle()">
                        <i class="fa fa-bars"></i>
                    </div>
                    <nav class="category-menu" id="main-menu">
                        <ul>
                            <?php if (Yii::$app->user->isGuest) : ?>
                                <li><a href="/"><?= Yii::t('app','Home')?></a></li>
                            <?php else : ?>
                                <li><a href="/personal-area/<?= Yii::$app->user->identity->username ?>"><?= Yii::t('app','Personal area')?></a></li>
                            <?php endif ?>

                            <?php if ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->role == User::ROLE_OWNER)) : ?>
                                <li>
                                    <?= Html::a(Yii::t('app', 'Create Project'), ['/idea-create']) ?>
                                </li>
                            <?php endif; ?>

                            <li>
                                <?= Html::a(Yii::t('app', 'Contact'), ['/contact-us']) ?>
                            </li>

                            <li>
                                <?= Html::a(Yii::t('app', 'Startup catalog'), ['/search']) ?>
                            </li>

                            <li>
                                <?= Html::a(Yii::t('app', 'Blog'), ['/blog']) ?>
                            </li>

                            <?php if (Yii::$app->user->isGuest): ?>
                                <li>
                                    <a href='/login'>
                                        <?= Yii::t('app', 'Login') ?>
                                    </a>
                                </li>
                            <?php else : ?>
                                <li>
                                    <a href="/logout" data-method="post">
                                        <?= Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')' ?>
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                        <div class="clearfix"></div>
                    </nav>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="category-title">
                <div class="line"></div>
                <span><?= $this->title ?></span>
            </div>
            <div class="clearfix"></div>
        </div>

    </header>
<?php endif ?>
<?php $this->beginBody() ?>
<?= $content ?>

<footer>
    <div class="container">
        <div class="logo-wrap">
            <div class="logo">
                <span>
                «<span class="name">Grow.Me</span>»
            </span>
            </div>
        </div>
        <div class="columns">
            <div class="col">
                <span class="title"><?= Yii::t('app', 'About us')?></span>
                <ul class="links-list">
                    <li><a href="#"><?= Yii::t('app', 'Our Profile')?></a></li>
                    <li><?= Html::a(Yii::t('app', 'Advertisement'), ['advertisement'])?></li>
                    <li><?= Html::a(Yii::t('app', 'Blog'), ['/blog']) ?></li>

                </ul>
            </div>
            <div class="col">
                <span class="title"> &nbsp; </span>
                <ul class="links-list">
                    <li><a href="#"><?= Yii::t('app', 'Guide')?></a></li>
                    <li><a href="#"><?= Yii::t('app', 'Why Grow.me?')?></a></li>
                    <li><a href="#"><?= Yii::t('app', 'FAQ')?></a></li>
                </ul>
            </div>
            <div class="col">
                <span class="title"><?= Yii::t('app', 'Investing')?></span>
                <ul class="links-list">
                    <li><a href="#"><?= Yii::t('app', 'Glossary')?></a></li>
                    <li><?= Html::a(Yii::t('app', 'Terms'), ['/terms'])?></li>
                    <li><?= Html::a(Yii::t('app', 'Risks'), ['/risks']) ?></li>
                </ul>
            </div>
            <div class="col">
                <span class="title"><?= Yii::t('app', 'Contact us')?></span>
                <ul class="links-list">
                    <li><i class="telephone"></i><span>(380) 96-09-66-488</span></li>
                    <li><a href="mailto:hello@grow.me.com"><i class="mail"></i>admin@grow-me.co</a></li>
                    <li><a href="skype:growme"><i class="skype"></i>diadems1</a></li>
                    <li><i class="marker"></i><span>Львов, Украина</span></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="basement">
        <div class="container">
            <div class="copyright">
                <?= Yii::t('app','© 2010-{year} All Rights Reserved',['year'=>date('Y')])?>
            </div>
            <div class="socials">
                <a href="https://twitter.com/growmeco" class="twitter"></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
