<?php
use yii\helpers\Html;
use app\assets\LightSlider;
use app\assets\PerfectScrollBar;
use yii\widgets\ActiveForm;
use dosamigos\editable\Editable;
use app\modules\user\models\User;
use yii\helpers\BaseStringHelper;
use app\components\Helper;
use app\modules\project\models\Project;
use alexBond\thumbler\Thumbler;

/* @var $projects \app\modules\project\models\Project [] */
/* @var $investments \app\modules\user\controllers\AdminController */
/* @var $countProjects int */
/* @var $amount int */
/* @var $this yii\web\View */
/* @var $model User */

$this->title = Yii::t('app', 'Personal area');
LightSlider::register($this);
PerfectScrollBar::register($this);

$this->registerJs(<<<JS
window['PopUpShow'] = function () {
    $("#popup1").show();
};

window['PopUp2Show'] = function() {
    $("#popup2").show();
};

window['PopUpHide'] = function () {
    $("#popup1").hide();
    $("#popup2").hide();
};

window['WriteFriend'] = function() {
   location.replace('/message/chat?id='+jQuery('#write-friend').attr('data-companion'));
};

function awesomeGallery(slider) {
    slider.lightSlider({
        gallery: true,
        item: 1,
        vertical: true,
        verticalHeight: 520,
        autoWidth: false,
        vThumbWidth: 50,
        thumbMargin: 10,
        slideMargin: 0,
        controls: false,
        enableDrag: false,
        thumbItem: 5,
        onSliderLoad: function (el) {
            var myFunc = function(element) {
                var arr = element.parents('.lSPager').find('li');
                var toFind = element.parent();
                var num = arr.index(toFind);
                var dataId = jQuery(datas[num]);
                var dataFriend = dataId.data('friend');
                btn.attr('data-friend', dataFriend);
                writeBtn.attr('data-companion', dataFriend);
            };
            var datas = el.parent().parent().find('#vertical li');
            var link = el.parent().parent().find('.lSPager li a');
            var btn = jQuery('.second-friend-btn');
            var writeBtn = jQuery('#write-friend');
            var element = jQuery(link[0]);
            if(jQuery(element).length) {
                myFunc(element);
            } else {
                link = jQuery('.lslide.active');
                var dataFriend = link.data('friend');
                btn.attr('data-friend', dataFriend);
                writeBtn.attr('data-companion', dataFriend);
            }
            link.click(function () {
                var element = $(this);
                myFunc(element);
            });
        },
        onBeforeSlide: function(el) {

        }
    });
}

jQuery(document).ready(function () {
    var slider = jQuery('#vertical');
    awesomeGallery(slider);
    var editable = jQuery('.editable-click');

    editable.click(function () {
        jQuery(this).next().find('.editable-submit i')
            .removeClass('glyphicon').removeClass('glyphicon-ok')
            .addClass('fa').addClass('fa-check-square');
        jQuery(this).next().find('.editable-cancel i')
            .removeClass('glyphicon').removeClass('glyphicon-ok')
            .addClass('fa').addClass('fa-minus-square');
    });
    var personalProjectListScrollBar = function () {
        var inf2 = jQuery('.bottom-part .order ul');
        if (!inf2.length) return;
        inf2.perfectScrollbar({});
    };
    personalProjectListScrollBar();


    var acceptFriend = function (fromId) {
        jQuery.ajax({
            url: '/friend/accept-friend',
            data: {'fromId': fromId}
        }).done(function () {
            location.reload();
        });
    };
    jQuery('.accept-friend').click(function () {
        acceptFriend(jQuery(this).attr('data-friend'));
    });

    var deniedFriend = function (fromId) {
        jQuery.ajax({
            url: '/friend/denied-friend',
            data: {'fromId': fromId}
        }).done(function () {
            location.reload();
        });
    };
    jQuery('.denied-friend').click(function () {
        deniedFriend(jQuery(this).attr('data-friend'));
    });

    var deleteFriend = function (toId) {
        jQuery.ajax({
            url: '/friend/delete-friend',
            data: {'toId': toId}
        }).done(function () {
            location.reload();
        });
    };
    var deleteProject = function (project_id) {
        jQuery.ajax({
            url: '/site/delete-project',
            data: {'project_id': project_id}
        }).done(function () {
            location.reload();
        });
    };
    jQuery('.delete-friend').click(function () {
        var btn = jQuery('.second-friend-btn');
        deleteFriend(btn.attr('data-friend'));
    });
    jQuery('.delete-project').click(function () {
        deleteProject($(this).attr('data-project-id'));
    });
    jQuery('.order-delete a').click(function(){
        var btn = jQuery('.delete-project');
        btn.attr('data-project-id', $(this).attr('data-project-id'));
    });




    jQuery('.username-error').delay(1500).fadeOut(800);

    //Write to friend



    //Change photo

     var changePhoto = function (path) {
         jQuery.ajax({
             url: '/site/change-photo',
             data: {'path' : path}
         }).done (function(data) {

         });
     };

    var image = jQuery('.change-photo');
    var button = image.next().find('#user-photo_button');
    var input = image.next().find('#user-photo');

    image.click(function(){
        button.click();
    });

    input.on('change', function(){
        changePhoto(input.val());
    });

    var arr = jQuery('#vertical li');
    var friends = new Array();
    jQuery.each(arr, function(key, val){
        var cur = jQuery(val);
        var friend = {
                id : cur.data('id'),
                photo : cur.data('thumb'),
                friend : cur.data('friend')
            };
        friends.push(cur.data('name'));
        friends[cur.data('name')] = friend;
    });

    var filtrationFriends = function(friends) {
        var filter = jQuery('.friends-filter input').val();
        var friends = $.grep(friends, function(n){
            if(n.indexOf(filter)>=0)
                return true;
            else
                return false;
        });

        return friends;
    };
    $('.friends-filter').on('change', function(){
        var array = filtrationFriends(friends);
        //jQuery('#vertical li').hide();
        jQuery('.lSPager li').hide();
        var bigLi = jQuery('#vertical li');
        var littleLi = jQuery('.lSPager li');

        jQuery.each(bigLi, function(key,val){
            jQuery.each(array, (function(key, name){
                if(name == jQuery(val).data('name')) {
                    var cur = jQuery(val);
                    cur.addClass('helper');
                    var index = bigLi.filter('.helper').index();
                    var littleCur = jQuery(littleLi[index]);
                    littleCur.show();
                    littleCur.addClass('helper');
                    cur.removeClass('helper');
                }
            }))
        });
        var clickClass = function(){
            jQuery(littleLi).filter('.helper')[0].click();
            jQuery(littleLi).filter('.helper').removeClass('helper');
        };
        setTimeout(clickClass, 200);
    });
});

JS
);


$form = ActiveForm::begin();
?>
<div class="wrapper-footer">
    <div class="delete-friend-popup" id="popup1">
        <div class="popup-content">
            <p><?= Yii::t('app', 'Do you really want to delete friend?') ?></p>
            <a href="javascript:PopUpHide()"><?= Yii::t('app', 'Cancel') ?></a>
            <a href="#" class="delete-friend"><?= Yii::t('app', 'Delete friend') ?></a>
        </div>
    </div>
    <div class="delete-project-popup" id="popup2">
        <div class="popup-content">
            <p><?= Yii::t('app', 'Do you really want to delete this project?') ?></p>
            <a href="javascript:PopUpHide()"><?= Yii::t('app', 'Cancel') ?></a>
            <a href="#" class="delete-project"><?= Yii::t('app', 'Delete project') ?></a>
        </div>
    </div>
    <div class="content" style="padding-top: 10px">
        <div class="container">

            <?php if (Yii::$app->getSession()->hasFlash('user-successful-registration')): ?>
                <div class="alert alert-success">
                    <?= Yii::t('app', 'You have successfully registered.') ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->getSession()->hasFlash('user-successful-password-reset')): ?>
                <div class="alert alert-success">
                    <?= Yii::t('app', 'You have successfully reset your password.') ?>
                </div>
            <?php endif; ?>

            <div class="personal-information">
                <div class="left-block">
                    <div class="editable-block">
                        <h1>
                            <?= $form->field($model, 'username')->widget(Editable::className(), [
                                'url' => 'personal-area/' . $model->username,
                                'clientOptions' => [
                                    'error' => new \yii\web\JsExpression("function(xhr, newValue) {
                                        if (xhr.status === 200){
                                            return '';
                                        }
                                        return (typeof xhr === 'string' ? xhr : xhr.responseText || xhr.statusText || 'Unknown error!');
                                    }"),
                                ]
                            ]); ?>
                        </h1>
                    </div>
                    <div class="tabs">
                        <ul>
                            <li>
                                <input type="radio" name="tabs-0" checked="checked" id="tabs-0-0"/>
                                <label for="tabs-0-0"><?= Yii::t('app', 'About Me') ?></label>

                                <div class="personal-information-text">
                                    <div class="editable-block with-attribute">
                                        <img class="change-photo"
                                             src="<?= Helper::getThumb($model->photo, 200, 150, Thumbler::METHOD_BOXED, 'FFFFFF', false, ['default' => 'avatar']); ?>"
                                             alt="">

                                        <?= $form->field($model, 'photo')->widget(\mihaildev\elfinder\InputFile::className(), [
                                            'language' => 'ru',
                                            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                                            'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                                            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                                            'options' => ['class' => 'form-control'],
                                            'buttonOptions' => ['class' => 'btn btn-default'],
                                            'multiple' => false       // возможность выбора нескольких файлов
                                        ]); ?>

                                        <?= $form->field($model, 'first_name')->widget(Editable::className(), [
                                            'url' => 'personal-area/' . $model->username,
                                            'mode' => 'inline',
                                        ]); ?>

                                        <?= $form->field($model, 'last_name')->widget(Editable::className(), [
                                            'url' => 'personal-area/' . $model->username,
                                            'mode' => 'inline',
                                        ]); ?>

                                    </div>
                                </div>
                            </li>
                            <li>
                                <input type="radio" name="tabs-0" id="tabs-0-1"/>
                                <label for="tabs-0-1"><?= Yii::t('app', 'Contacts') ?></label>

                                <div class="personal-information-text">
                                    <div class="editable-block with-attribute">

                                        <?= $form->field($model, 'email')->widget(Editable::className(), [
                                            'url' => 'personal-area/' . $model->username,
                                            'mode' => 'inline',
                                        ]); ?>

                                        <?= $form->field($model, 'phone')->widget(Editable::className(), [
                                            'url' => 'personal-area/' . $model->username,
                                            'mode' => 'inline',
                                        ]); ?>

                                        <?= $form->field($model, 'address')->widget(Editable::className(), [
                                            'url' => 'personal-area/' . $model->username,
                                            'mode' => 'inline',
                                        ]); ?>

                                    </div>
                                </div>
                            </li>
                            <?php if (!empty($model->futureFriends)): ?>
                                <li>
                                    <input type="radio" name="tabs-0" id="tabs-0-2"/>
                                    <label for="tabs-0-2"><?= Yii::t('app', 'New Friends') ?></label>

                                    <div class="personal-information-text">
                                        <?php foreach ($model->futureFriends as $friend): ?>
                                            <div class="new-friend">
                                                <img src="<?= $friend->photo ?>">

                                                <div class="text"><?= $friend->username ?></div>
                                                <i class="fa fa-check-square accept-friend"
                                                   data-friend="<?= $friend->id ?>"></i>
                                                <i class="glyphicon-remove fa fa-minus-square denied-friend"
                                                   data-friend="<?= $friend->id ?>"></i>
                                            </div>
                                        <?php endforeach ?>

                                    </div>
                                </li>

                            <?php endif ?>
                        </ul>
                    </div>
                    <div class="personal-edit">
                        <table>
                            <tr>
                                <th>
                                    <?= Yii::t('app', 'My projects count') ?>
                                </th>
                                <td>
                                    <i class="fa"><?= $countProjects ?></i>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= Yii::t('app', 'The collected amount') ?>
                                </th>
                                <td>
                                    <i class="fa fa-usd"><?= $amount ?></i>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="right-block">

                    <?php if (!$model->activeFriends): ?>
                        <div class="no-friend">
                            <p><?= Yii::t('app', 'You don`t have friend yet') ?></p>
                        </div>
                    <?php else : ?>
                        <div class="friends-filter-wrapper">
                            <div class="friends-filter">
                                <input type="text">
                            </div>
                        </div>
                        <div class="friends-navi-wrapper">
                            <div class="first-friend-btn">
                                <a href="javascript:WriteFriend()"
                                   id="write-friend"><?= Yii::t('app', 'Write to friend') ?></a>
                            </div>
                            <div class="second-friend-btn">
                                <a href="javascript:PopUpShow()"><?= Yii::t('app', 'Delete friend') ?></a>
                            </div>
                        </div>
                        <ul id="vertical">
                            <?php $i = 0;
                            foreach ($model->activeFriends as $friend): ?>
                                <li data-thumb="<?= Helper::getThumb($friend->photo, 200, 150, 'friend-avatar'); ?>"
                                    data-friend="<?= $friend->id ?>"
                                    data-id="<?= $i++ ?>"
                                    data-name="<?= $friend->username ?>">
                                    <a href="/personal-area/<?= $friend->username ?>"
                                       style="background: url(<?= Helper::getThumb($friend->photo, 340, 500, 'friend-avatar', Thumbler::METHOD_CROP_CENTER); ?>)">
                                    </a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    <?php endif ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="personal-list">
                <div class="list-orders">
                    <div class="title">
                        <p>
                            <?php if (Yii::$app->user->identity->getRole() == User::ROLE_OWNER) : ?>
                                <?= Yii::t('app', 'list of ideas') ?>
                            <?php elseif (Yii::$app->user->identity->getRole() == User::ROLE_INVESTOR): ?>
                                <?= Yii::t('app', 'list of invest') ?>
                            <?php endif ?>
                        </p>
                    </div>
                    <div class="bottom-part">
                        <?php
                        if ($countProjects == 0): ?>
                            <div class="no-friend no-list">
                                <?php
                                if (Yii::$app->user->identity->getRole() == User::ROLE_OWNER) : ?>
                                    <p><?= Yii::t('app', 'You don`t have projects yet') ?></p>
                                <?php else: ?>
                                    <p><?= Yii::t('app', 'You don`t have investments yet') ?></p>
                                <?php endif ?>
                            </div>
                        <?php else : ?>
                            <div class="order">
                                <ul>
                                    <?php foreach ($projects as $project): ?>
                                        <?php if ($project->status != Project::STATUS_FOR_DELETING): ?>
                                            <li>
                                                <a href="#<?= $project->slug ?>">
                                                    <div><?= $project->title ?></div>
                                                </a>
                                            </li>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="list-information">
                    <?php if ($countProjects == 0): ?>
                        <div class="no-friend">
                            <?php
                            if (Yii::$app->user->identity->getRole() == User::ROLE_OWNER) : ?>
                                <p><?= Yii::t('app', 'You don`t have projects yet') ?></p>
                            <?php else: ?>
                                <p><?= Yii::t('app', 'You don`t have investments yet') ?></p>
                            <?php endif ?>
                        </div>
                    <?php else : ?>
                        <?php foreach ($projects as $project): ?>
                            <?php if ($project->status != Project::STATUS_FOR_DELETING): ?>
                                <div id="<?= $project->slug ?>">
                                    <div class="order-description">
                                        <div class="order-desc-image"
                                             style='background: url("<?= $project->photos[0]['url'] ?>")'></div>
                                        <div class="order-desc-text">
                                            <h1><?= $project->title ?></h1>

                                            <p>
                                                <?= BaseStringHelper::truncateWords($project->description, 45); ?>
                                            </p>
                                        </div>
                                        <div class="order-bottom-info">
                                            <p><?= Yii::t('time', '{d, date, medium}', ['d' => $project->created_at]) ?></p>

                                            <div class="buttons">
                                                <?= Html::a(Yii::t('app', 'Edit'), ["/idea-edit/" . $project->slug], ['class' => 'btn']) ?>
                                                <?= Html::a(Yii::t('app', 'Read more'), ["/idea/" . $project->slug], ['class' => 'btn']) ?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="order-navi-panel">
                                        <div class="order-like">
                                            <p><?= $project->countLikes() ?></p>
                                            <a href="javascript:void(0);"></a>
                                        </div>
                                        <div class="order-comment">
                                            <p><?= $project->CommentsAmount ?></p>
                                            <?= Html::a(null, ["idea/" . $project->slug, '#' => 'comments']) ?>
                                        </div>
                                        <div class="order-delete">
                                            <a href="javascript:PopUp2Show()" data-project-id="<?= $project->id ?>"></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
