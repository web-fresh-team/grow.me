<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\OwlCarousel;
use alexBond\thumbler\Thumbler;
use yii\helpers\BaseStringHelper;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\category\models\Category;
use app\components\Helper;

/* @var $this yii\web\View */

/* @var $projects app\modules\project\models\Project [] */

/* @var $categories app\modules\category\models\Category [] */

/* @var $model app\models\SortProject */

$this->title = Yii::$app->name;

?>
<div class="wrapper-footer">
    <div class="search">
        <div class="container">
            <div class="bg2">
                <?php $form = ActiveForm::begin([
                    'method' => 'get',
                    'class' => 'yii\widgets\ActiveForm',
                    'options' => [
                        'class' => 'sort-form',
                    ],
                    'action' => '/search',
                    'fieldConfig' => [
                        'class' => 'yii\widgets\ActiveField',
                        'template' => "{input}\n{hint}\n{error}",
                        'inputOptions' => [
                            'class' => 'registration-input',
                        ],
                        'errorOptions' => [
                            'class' => 'for-error',
                        ],
                        'options' => [
                            'class' => 'input-wrapper'
                        ]
                    ],
                    'errorCssClass' => 'error',

                ]); ?>

                <div class="my-nav-bar">
                    <div class="styled-select">
                        <?= Html::activeDropDownList($model, 'category',
                            ArrayHelper::merge([0 => Yii::t('app', 'All categories')], ArrayHelper::map(Category::find()->all(), 'id', 'title'))) ?>
                    </div>

                    <div class="styled-select">
                        <?= $form->field($model, 'bankSort')->
                        dropDownList($model->getBankMap()); ?>
                    </div>

                    <div class="styled-select">
                        <?= $form->field($model, 'likeSort')->
                        dropDownList($model->getLikeMap()); ?>
                    </div>

                    <div class="search-button">
                        <?= Html::submitButton('', ['class' => 'search']) ?>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

            <div class="popular">
                <div class="projects">
                    <?php foreach ($projects as $project) : ?>
                        <div class="col">
                            <a href="/idea/<?= $project->slug ?>" class="card">
                                <div class="thumb"><img src="
                                        <?= $path = Helper::getThumb($project->photos['0']['url'], 272, 180, Thumbler::METHOD_BOXED) ?>
                                        "/></div>
                                <div class="cost">$<?= $project->bank . ' ' . Yii::t('app', 'Bank') ?> </div>
                                <div class="likes"><i class="fa fa-heart"></i><?= $project->likes_amount ?></div>
                                <div class="desc">
                                    <?php
                                        $description = $project->short_description;
                                    ?>
                                    <div class="name"><span><?= $title = $project->title; ?></span></div>
                                    <div class="list">
                                        <?= $description ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach ?>
                    <div class="clearfix"></div>
                </div>
            </div>


        </div>
    </div>
</div>
</div>

