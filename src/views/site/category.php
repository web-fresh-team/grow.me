<?php
use yii\helpers\Url;
use app\assets\OwlCarousel;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

OwlCarousel::register($this);
?>
<div class="wrapper-footer">
    <div class="category-content">
    <div class="container">
        <div class="slider-big-item">
            <div class="left-img">
                <img src="/img/slider/Base.png" class="big-image">
                <img src="/img/slider/qwe.png" class="blue-line"> <div class="blue-line-text">$65,120</div>
            </div>
            <div class="right-block">
                <div class="avatar">
                    <label class="avatar-label">
                        <img src="/img/slider/images.jpg" id="avatar" />
                        Yaroslav Zhyvylo
                    </label>
                </div>
                <div class="slider-content">
                    <p class="slider-title ">Fast comprehensive balanced nutrition business</p>

                    <p>Look, everyone wants to be like Germany, but do we really have the pure strength to "will"? The
                        alien mothership is in orbit here.Look, everyone wants to be like Germany, but do we really have
                        the pure strength to "will"? The alien mothership is in orbit here.Look, everyone wants to be
                        like Germany, but do we really have the pure strength to "will"? The alien mothership is in
                        orbit here.Look, everyone wants to be like Germany, but do we really have the pure strength to
                        "will"? The alien mothership is in orbit here.</p>
                    <a href="#">
                        <button class="btn">
                            Read more
                        </button>
                    </a>
                </div>
                <div class="slider-bottom">
                    <div class="img-icon">
                        <label>
                            <a href="#"><img src="/img/slider/HeartIcon.png"></a>
                            46
                        </label>
                    </div>
                    <div class="img-icon">
                        <label>
                            <a href="#"><img src="/img/slider/HeartIcon.png"></a>
                            46
                        </label>
                    </div>
                    <p>12 hours ago</p>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>
    <div class="container">
        <div class="slider-frame">

        </div>
        <div class="own-slider">
            <input type="image" src="/img/slider/Arrow-left.png" id="prev-button" class="prev-arrow">
            <div id="owl-slider" class="slider">
                <div>
                    <div class="item-img">
                        <img src="/img/slider/hamburger.png">

                        <div class="item-img-content">
                            <p>Humburger of nanomaterials flavored banana</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <p class="bottom-text">265 likes 84 comments</p>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <div class="item-img">
                        <img src="/img/slider/lake.png">

                        <div class="item-img-content">
                            <p>Simulator rest for the poor or autism</p>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <p class="bottom-text">265 likes 84 comments</p>
                    <div class="clearfix"></div>

                </div>

                <div>
                    <div class="item-img">
                        <img src="/img/slider/man.png">
                        <div class="item-img-content">
                            <p>Polymer island of recycled plastic</p>
                        </div>
                    </div>
                    <p class="bottom-text">265 likes 84 comments</p>
                </div>

                <div>
                    <div class="item-img">
                        <img src="/img/slider/lake.png">

                        <div class="item-img-content">
                            <p>Simulator rest for the poor or autism</p>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <p class="bottom-text">265 likes 84 comments</p>
                    <div class="clearfix"></div>

                </div>


                <div class="item-img">
                    <img src="/img/slider/man.png">
                    <div class="item-img-content">
                        <p>Polymer island of recycled plastic</p>
                    </div>
                    <p class="bottom-text">265 likes 84 comments</p>
                </div>


            </div>

            <input type="image" src="/img/slider/Arrow-right.png" id="next-button" class="next-arrow">
        </div>
        <div class="block">
            <div class="text-1">
                <p class="cust-title">Technology</p>

                <p>Facts are meaningless. You could use facts to prove anything that's even remotely true! Jesus must be
                    spinning in his grave!Facts are meaningless. You could use facts to prove anything that's even remotely
                    true! Jesus must be spinning in his grave!Facts are meaningless. You could use facts to prove anything
                    that's even remotely true! Jesus must be spinning in his grave!</p>
            </div>

            <div class="img-block">
                <img src="/img/category/technology.png">
            </div>
        </div>

        <div class="block">
            <div class="text-2">
                <p class="cust-title">Internet</p>

                <p>Old people don't need companionship. They need to be isolated and studied so it can be determined what
                    nutrients they have that might be extracted.Old people don't need companionship. They need to be
                    isolated and studied so it can be determined what nutrients they have that might be extracted.Old people
                    don't need companionship. They need to be isolated and studied so it can be determined what nutrients
                    they have that might be extracted.</p>
            </div>

            <div class="img-block">
                <!--фото-->
                <img src="/img/category/internet.png">
            </div>
        </div>
        <div class="block">
            <div class="text-1">
                <p class="cust-title">Medicine</p>

                <p>Facts are meaningless. You could use facts to prove anything that's even remotely true! Jesus must be
                    spinning in his grave!Facts are meaningless. You could use facts to prove anything that's even remotely
                    true! Jesus must be spinning in his grave!Facts are meaningless. You could use facts to prove anything
                    that's even remotely true! Jesus must be spinning in his grave!</p>
            </div>

            <div class="img-block">
                <!--фото-->
                <img src="/img/category/technology.png">
            </div>
        </div>
        <div class="block">
            <div class="text-2">
                <p class="cust-title">Food</p>

                <p>Facts are meaningless. You could use facts to prove anything that's even remotely true! Jesus must be
                    spinning in his grave!Facts are meaningless. You could use facts to prove anything that's even remotely
                    true! Jesus must be spinning in his grave!Facts are meaningless. You could use facts to prove anything
                    that's even remotely true! Jesus must be spinning in his grave!</p>
            </div>

            <div class="img-block">
                <!--фото-->
                <img src="/img/category/internet.png">
            </div>
        </div>
        <div class="clearfix"></div>
    </div>


    <div class="get-started">

    </div>

</div>
</div>