<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<div class="wrapper-footer">
    <div class="container">
    <div class="registration-form">
        <div class="input-wrapper error">
            <div class="for-error">
                    Fill in bla bla bla bla
            </div>
            <input type="text" class="registration-input" placeholder="First Name">
        </div>
        <div class="input-wrapper error">
            <div  class="for-error">Fill in bla bla bla bla</div>
            <input type="text" class="registration-input" placeholder="Last Name">
        </div>
        <div class="input-wrapper">
            <!--<div class="for-error"></div>-->
            <input type="text" class="registration-input" placeholder="Email">
        </div>
        <div class="input-wrapper">
            <!--<div class="for-error error"></div>-->
            <input type="text" class="registration-input" placeholder="Phone">
        </div>
        <div class="registration-select">
            <select>
                <option>Technology</option>
                <option>Technology2</option>
                <option>Technology</option>
                <option>Technology2</option>
            </select>
        </div>
        <div class="registration-select" style="margin-right: 0">
            <select>
                <option>Internet</option>
                <option>Technology2</option>
            </select>
        </div>
        <div class="input-wrapper error">
            <div>Fill in bla bla bla bla</div>
            <input type="text" class="registration-input" placeholder="State / Region">
        </div>
        <div class="input-wrapper">
            <input type="text" class="registration-input" placeholder="ZIP / Postal Code">
        </div>
        <div class="input-wrapper">
            <input type="text" class="registration-input" placeholder="Address 1">
        </div>
        <div class="input-wrapper">
            <input type="text" class="registration-input" placeholder="Address 2">
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="button">
        <a href="#">sign up</a>
    </div>
</div>
</div>