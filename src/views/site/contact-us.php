<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = Yii::t('app', 'Contact us');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrapper-footer">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                <?= Yii::t('app', 'Thank you for contacting us. We will respond to you as soon as possible.'); ?>
            </div>

        <?php else: ?>

            <p>
                <?= Yii::t('app', 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.'); ?>
            </p>
            <?php $form = ActiveForm::begin([
                'id' => 'contact-form',
                'class' => 'yii\widgets\ActiveForm',
                'fieldConfig' => [
                    'class' => 'yii\widgets\ActiveField',
                    'template' => "{input}\n{hint}\n{error}",
                    'inputOptions' => [
                        'class' => 'registration-input',
                    ],
                    'errorOptions' => [
                        'class' => 'for-error',
                    ],
                    'options' => [
                        'class' => 'input-wrapper'
                    ]
                ],
                'errorCssClass' => 'error',
            ]); ?>
            <div class="registration-form">
                <div class="col-lg-5">

                    <?= $form->field($model, 'name', [
                        'inputOptions' => [
                            'placeholder' => $model->getAttributeLabel('name')
                        ]
                    ]) ?>

                    <?= $form->field($model, 'email', [
                        'inputOptions' => [
                            'placeholder' => $model->getAttributeLabel('email')
                        ]
                    ]) ?>

                    <?= $form->field($model, 'subject', [
                        'inputOptions' => [
                            'placeholder' => $model->getAttributeLabel('subject')
                        ]
                    ]) ?>

                    <?= $form->field($model, 'body', [
                        'inputOptions' => [
                            'placeholder' => $model->getAttributeLabel('body')
                        ]
                    ])->textArea(['rows' => 6]) ?>

                    <div class="button">
                        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>