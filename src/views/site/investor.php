<?php
use yii\helpers\Url;
use app\assets\OwlCarousel;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

OwlCarousel::register($this);
?>
<div class="wrapper-footer">
    <div class="content" >
    <div class="invest-strategy">
        <div class="container">
            <div class="big-board-list">
                <div class="big-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4>
                    <p>Description startup Description startup Description startup</p>
                    <p>$215</p>
                </div>
                <div class="big-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4>
                    <p>Description startup Description startup Description startup</p>
                    <p>$215</p>
                </div>
                <div class="big-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4>
                    <p>Description startup Description startup Description startup</p>
                    <p>$215</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="container">
            <div class="small-board-list">
                <div class="small-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4><p class="price">$215</p>
                    <div class="line"></div>
                    <p>Description startup Description startup Description startup</p>
                </div>
                <div class="small-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4><p class="price">$215</p>
                    <div class="line"></div>
                    <p>Description startup Description startup Description startup</p>
                </div>
                <div class="small-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4><p class="price">$215</p>
                    <div class="line"></div>
                    <p>Description startup Description startup Description startup</p>
                </div>
                <div class="small-board">
                    <div class="wrapper">
                        <div class="navi-board">
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/plus.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/search.png">
                                </div>
                            </a>
                            <a href="#">
                                <div class="icon-wrapper">
                                    <img src="/img/page-of-investors/heart.png">
                                </div>
                            </a>
                        </div>
                        <img src="/img/page-of-investors/page-investors.png">
                    </div>
                    <h4>The name of the startup</h4><p class="price">$215</p>
                    <div class="line"></div>
                    <p>Description startup Description startup Description startup</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="container">
            <div class="board-list">
                <div>
                    <div class="title">
                        <p>popular</p>
                        <div class="navi-arrow">
                            <input type="image" src="/img/page-of-investors/Arrow_Icon2.png" id="prev-btn">
                            <input type="image" src="/img/page-of-investors/Arrow_Icon.png" id="next-btn">
                        </div>
                    </div>
                    <div id="owl-slider-board" class="boards-slider">
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                        <div>
                            <img src="/img/page-of-investors/startup-image.png" alt="image">
                            <p>Brita Water Filter</p>
                            <p class="price">$185</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</div>