<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContactForm */

/* @var $posts app\modules\blog\models\BlogPost [] */

$this->title = Yii::t('app', 'Advertisement');
?>

<div class="container">
    <div class="info">
        <div class="link">
            <h1>
                <?= $this->title ?>
            </h1>
            <p>
                <?= Yii::t('app', 'All issues related to the deployment of advertising on the site and community meetings to direct {email}',
                    ['email' => Html::a(Yii::$app->params['adminEmail'], 'mailto:' . Yii::$app->params['adminEmail'])]) ?>
            </p>
        </div>
    </div>
</div>
