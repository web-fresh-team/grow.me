<?php
use yii\helpers\Url;
use app\assets\DesoSlider;
use app\assets\FancyBox;
use app\modules\project\models\Project;
use app\widgets\SendMessage;
use app\modules\user\models\User;
use mihaildev\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\components\Helper;

/* @var $this yii\web\View */

/* @var $model app\modules\project\models\Project */

$this->title = empty($model->title) ? Yii::t('app', "Create Project") : $model->title;

FancyBox::register($this);
DesoSlider::register($this);

$this->registerJs(<<<JS
jQuery(".accordion").accordion({
    heightStyle: "content"
});

jQuery('.fancy a').fancybox();

jQuery('.images').on('click', '.fa', function () {
    var image = jQuery(this).parents('.fancy').addClass('to-delete');
    var images = jQuery('.images .fancy');
    var index = images.filter('.to-delete').index();
    var browses = jQuery('.multiple-input-list tr');
    var current = jQuery(browses[index]);
    var toDelete = true;
    if (index == 0) {
        toDelete = false;
    }
    image.fadeOut(500, function () {
        image.remove();
        if (toDelete) {
            current.remove();
        } else {
            current.find('.form-group input').val('');
        }
    });
});

var afterAdd = function () {
    var browses = jQuery('.multiple-input-list tr');
    browses.find('input').each(function (index) {
        var cur = jQuery(this);
        if (cur.val() == '' && index != 0) {
            cur.parents('tr').remove();
        }
    });
};

jQuery('.button .btn-primary, .button .btn-success').mouseover(function () {
    afterAdd();
});

var index = jQuery('.images .fancy').size();


var addPhoto = jQuery('.add-button .button a');
addPhoto.click(function (e) {
    event.preventDefault(e);

    var browse = jQuery('.add-button .hidden button');
    browse.click();

    var input = jQuery('.add-button .hidden input');
    input.off().on('change', function () {
        var value = input.val();

        jQuery.ajax({
            url: '/site/thumb',
            data: {
                url: value,
                width: 250,
                height: 200
            },
            success: function (value) {
                var photoPath = jQuery('.add-button .hidden input').val();
                jQuery('.hidden.photo input').attr('value',photoPath);

                var photo = jQuery('.hidden.photo').html();

                var toAppend = photo.replace('#href', photoPath)
                .replace('#src', value)
                .replace('#index', index++);
                var images = jQuery('.images');

                images.find('.clearfix').before(toAppend);
            }
        });
    });
    return false;
});

JS
);


?>
<div class="hidden photo">
    <li class="fancy">
        <div class="delete">
            <i class="fa fa-times-circle-o"></i>
        </div>
        <a href="#href" class="image">
            <img src="#src" width="200">
        </a>
        <input name='Project[photos][#index][url]' type="text" class="hidden">
    </li>
</div>
<div class="wrapper-footer">
    <div class="content">
        <div class="container">
            <?php $form = ActiveForm::begin([
                'class' => 'yii\widgets\ActiveForm',
                'fieldConfig' => [
                    'class' => 'yii\widgets\ActiveField',
                    'template' => "{input}\n{hint}\n{error}",
                    'inputOptions' => [
                        'class' => 'registration-input',
                    ],
                    'errorOptions' => [
                        'class' => 'for-error',
                    ],
                    'options' => [
                        'class' => 'input-wrapper'
                    ]
                ],
                'errorCssClass' => 'error',

            ]); ?>

            <div class="edit-page">

                    <div class="hat">

                        <?= $form->field($model, 'title', [
                            'inputOptions' => [
                                'placeholder' => $model->getAttributeLabel('title'),
                                'class' => 'registration-input',
                            ],
                            'template' => "{label} {input}\n{hint}\n{error}",
                        ])->textInput() ?>

                        <?= $form->field($model, 'bank', [
                            'inputOptions' => [
                                'placeholder' => $model->getAttributeLabel('bank')
                            ],
                            'template' => "{label} {input}\n{hint}\n{error}"
                        ])->textInput() ?>
                        <div class="clearfix"></div>
                        <?= $form->errorSummary($model) ?>
                    </div>
                    <div class="accordion">
                        <h3><?= Yii::t('app', 'Short description') ?></h3>
                        <div class="elfinder">
                            <?= $form->field($model, 'short_description')->textarea(['maxlength' => true]) ?>
                        </div>

                        <h3><?= Yii::t('app', 'Description') ?></h3>
                        <div class="elfinder">
                            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                                'editorOptions' => [
                                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                    'height' => 200
                                ],
                            ]) ?>
                        </div>

                        <h3><?= Yii::t('app', 'Images') ?></h3>
                        <div class="images">
                            <?php if(!$model->isNewRecord) : ?>
                                <ul>
                                    <?php $i=0; foreach ($model->photos as $photo) :?>
                                        <li class="fancy">
                                            <div class="delete">
                                                <i class="fa fa-times-circle-o"></i>
                                            </div>
                                            <a href="<?= str_replace(DIRECTORY_SEPARATOR, '/', $photo['url']) ?>" class="image">
                                                <img src="<?= Helper::getThumb($photo['url'], 250, 200, \alexBond\thumbler\Thumbler::METHOD_BOXED); ?>"
                                                     width="200">
                                            </a>
                                            <input name='Project[photos][<?= $i++; ?>][url]' value="<?= $photo['url'] ?>" type="text" class="hidden">
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            <?php endif ?>
                            <div class="clearfix"></div>
                            <div class="add-button">
                                <div class="button">
                                    <a href=""><?= Yii::t('app', 'Add photo') ?></a>
                                </div>
                                <div class="hidden">
                                    <?= \mihaildev\elfinder\InputFile::widget([
                                        'language'   => 'ru',
                                        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                                        'filter'     => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                                        'name'       => 'myinput',
                                        'value'      => '',
                                    ]); ?>

                                </div>
                            </div>
                        </div>

                        <h3><?= Yii::t('app', 'Categories') ?></h3>
                        <div class="checklist">
                            <?= $form->field($model, 'categories_list')->checkboxList(\app\modules\category\models\Category::getDropDownArray('id', 'title')) ?>
                        </div>
                    </div>

                </div>
            <div class="button">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="post-footer"></div>
    </div>
</div>
