<?php
use yii\helpers\Url;
use alexBond\thumbler\Thumbler;
use app\components\Helper;
use app\assets\DesoSlider;
use app\assets\FancyBox;

use app\widgets\SendMessage;
use app\modules\user\models\User;

/* @var $this yii\web\View */

/* @var $project \app\modules\project\models\Project */

/* @var $comment \app\modules\comment\models\Comment; */

$this->title = $project->title;

$projectId = $project->id;

FancyBox::register($this);
DesoSlider::register($this);


$this->registerJs(<<<JS
jQuery('#approve-message-btn').click(function() {
  window.location.reload();
});
   window['PopUpShow'] = function (text) {
    $("p").remove();
    var cur = jQuery('.popup-content');
    $('<p>'+text+'</p>').appendTo(cur);
    $("#popup").show();
    };

    window['PopUpHide'] = function () {
        $("#popup").hide();
    };


    jQuery('.tabs').tabs();

    //LoadMoreComments
    var loadMoreComments = function (counter) {
        var projectName = "$project->slug";
        jQuery.ajax({
            url: '/idea/'+projectName,
            data: {'counter': counter}
        }).done (function(data) {
            var button = jQuery('.btn-div');
            var counter = button.find('.btn-more');
            //counter.hide();
            counter.attr('data-counter',data.counter);
            var comment = jQuery('.hidden').html();
            jQuery.each(data.comments, function( key, val ){
                var toAppend = comment.replace('#title',val.username).replace('#text', val.text).replace('#added',val.added);
                button.before(toAppend);
            });
            if (data.disable == true) {
                button.remove();
            }
        });
    };

    jQuery('.btn-div a').click(
        function() {
            var counter = jQuery('.btn-more').attr('data-counter');
            loadMoreComments(counter);
        });


    //CommentSend
    var sendComment = function(projectId, text) {
        jQuery.ajax({
            url: '/site/add-comment',
            data: { 'projectId' : projectId, 'text' : text}
        }).done (function(data){
            var form = jQuery('#add-comment');
            var arrowIcon = jQuery('.fa-arrow-up');
            form.css({'height':'0px'});
            arrowIcon.fadeOut();
            var messagePlace = jQuery('.success-message');
            var message = messagePlace.find('.message');
            message.html(data.message);
            messagePlace.fadeIn();
        });
    };

    jQuery('.send-ajax').click(
    function(e) {
        event.preventDefault(e);
        var projectId = $projectId;
        var text = jQuery('.add-comment-popup .registration-input').val();
        sendComment(projectId, text);
        return false;
    });

    //SendInvest
    var createInvest = function(projectId, amount) {
        jQuery.ajax({
            url: '/investment/create-invest',
            data: { 'projectId' : projectId, 'amount' : amount}
        }).done (function(data){
            if (data.done == true){
                PopUpShow(data.message);
            } else {
                PopUpShow(data.message);
            }
        });
    };


    jQuery('.send-invest').click(
        function(e) {
            event.preventDefault(e);
            var projectId = $projectId;
            var amount= jQuery('.invest-form .registration-input').val();
            createInvest(projectId, amount);
            return false;
        });


    jQuery('.open-form').click(
        function(e) {
            event.preventDefault(e);
            var opener = jQuery(this);
            opener.fadeOut('fast');
            jQuery('.invest-form').fadeIn('slow');
            return false;
        });


   //AcceptInvest
    var acceptInvest = function(investId, invest) {
        jQuery.ajax({
            url: '/investment/accept-invest',
            data: { 'investId' : investId}
        }).done (function(data){
            if (data.done == true){
                var investJ = jQuery(invest);
                investJ.fadeOut(500, function() {
                    investJ.remove()});
            } else {
                PopUpShow(data.message);
            }
        });
    };


   jQuery('.accept-link').click(
        function(e) {
            event.preventDefault(e);
            var investId = jQuery(this).attr('data-invest');
            var invest = jQuery(this).parents('.post-menu');
            acceptInvest(investId,invest);
            return false;
        });


        var plusIcon = jQuery('.fa-plus');
        var form = jQuery('#add-comment');
        var arrowIcon = jQuery('.fa-arrow-up');
        var approveMessage = jQuery('.approve-message');

        //OpenCommentBox
        plusIcon.click(function(){
            form.css({'height':'auto'});
            plusIcon.fadeOut();
            arrowIcon.fadeIn();
            jQuery('#comment-text').val("");
        });

        //CloseCommentBox
        arrowIcon.click(function(){
            form.css({'height':'0px'});
            arrowIcon.fadeOut();
            plusIcon.fadeIn();
        });

        //CloseMessage
        approveMessage.click(function(e){
            event.preventDefault(e);
            var messagePlace = jQuery('.success-message');
            messagePlace.fadeOut();
            plusIcon.fadeIn();
        }).before(function(){
            arrowIcon.fadeOut();
        });

        var MyInput = jQuery('#invest-summ');

        MyInput.on('keypress', function(e){
                var unicode = e.keyCode;
                if (unicode == 45){

                return false;
                }
            });
        $(document).ready(function() {


        });
JS
);

?>
<div class="hidden">
    <div class="post-menu">
        <div class="post-content">
            <p class="title">#title</p>

            <p>#text</p>

            <div class="slider-bottom">
                <p>#added</p>
            </div>
        </div>
    </div>
</div>

<div class="wrapper-footer">
    <div class="create-invest-popup" id="popup">
        <div class="popup-content">
            <p></p>
            <a href="javascript:PopUpHide()">
                <?= Yii::t('app', 'Ok') ?>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div id="slideshow_1_thumbs_1" class="left-thumb">
                <ul class="slideshow1_thumbs desoslide-thumbs-vertical list-inline text-center">
                    <?php foreach ($project->photos as $photo) : ?>
                        <li>
                            <a href="<?= Helper::getThumb($photo['url'], 700, 500, null, Thumbler::METHOD_BOXED, 'fafafa') ?>">
                                <img
                                    src="<?= Helper::getThumb($photo['url'], 1280, 720, null, Thumbler::METHOD_BOXED, 'fafafa') ?>"
                                    alt="<?= Yii::t('app', 'I have an idea for a company') ?>"
                                    data-desoslide-caption-title="<?= $project->short_description ?>">
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div id="slideshow1" class="right-image"></div>
            <div class="clearfix"></div>
        </div>

        <div class="container">
            <div class="description">
                <p class="idea-title"><?= Yii::t('app', 'Description and characteristics') ?></p>
                <div class="price">
                    <ul>
                        <li>$<?= $project->amount ?></li>
                        <li>$<?= $project->bank ?></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="bottom-text">
                    <p>
                        <?= $project->description ?>
                    </p>
                </div>

                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getRole() == User::ROLE_INVESTOR) : ?>
                    <div class="invest-place">
                        <div class="invest-form">
                            <div class="input-wrapper">
                                <input id="invest-summ" type="number" min="0" class="registration-input"
                                       placeholder="<?= Yii::t('app', 'Your sum') ?>">
                            </div>
                            <div class="button">
                                <a href="#" class="send-invest">
                                    <?= Yii::t('app', 'Send invest') ?>
                                </a>
                            </div>
                        </div>
                        <div class="button">
                            <a href="javascript:void(0);" class="open-form">
                                <?= Yii::t('app', 'Invest') ?>
                            </a>
                        </div>
                    </div>
                <?php endif ?>

            </div>
            <div class="tabs">
                <ul>
                    <li><a href="#comments"><?= Yii::t('app', 'Comments') ?></a></li>
                    <li><a href="#accepted-invest"><?= Yii::t('app', 'Investments') ?></a></li>
                    <?php if (!Yii::$app->user->isGuest && $project->user_id == Yii::$app->user->identity->getId()) : ?>
                        <li><a href="#sent-invest"><?= Yii::t('app', 'Invest requests') ?></a></li>
                    <?php endif ?>
                </ul>
                <div id="comments" class="tab post-list">
                    <div class="post-head">
                        <p><?= Yii::t('app', 'Comments list') ?></p> <i class="fa fa-plus"></i>
                        <div class="clearfix"></div>
                    </div>
                    <div class="add-comment-popup">
                        <div class="success-message">
                            <div class="message">#message</div>
                            <div class="button">
                                <a href="javascript:void(0)" class="approve-message" id="approve-message-btn"><?= Yii::t('app', 'OK') ?></a>
                            </div>
                        </div>
                        <?= SendMessage::widget(['modelName' => 'comment']); ?>
                    </div>

                    <?php foreach ($project->lastComments as $comment): ?>
                        <div class="post-menu">
                            <div class="post-content">
                                <div class="post-image">
                                    <a href="<?= Url::to(['/personal-area/'. $comment->user->username]) ?>">
                                        <img
                                            src="<?= Helper::getThumb($comment->user->photo, 75, 100, 'avatar'); ?>"
                                            alt="">
                                    </a>
                                </div>
                                <p class="title"><?= $comment->user->username ?></p>

                                <p><?= $comment->text ?></p>

                                <div class="clearfix"></div>
                                <div class="slider-bottom">
                                    <p><?= $comment->timeCreated ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>

                    <?php if ($project->lastComments != null): ?>
                        <div class="btn-div">
                            <a href="javascript:void(0);">
                                <button class="btn-more" data-counter="0">
                                    <img src="/img/invest/circle-arrow.png">
                                    <span><?= Yii::t('app', 'Need more comments') ?></span>
                                </button>
                            </a>
                        </div>
                    <?php endif ?>

                    <?php if ($project->lastComments == null): ?>
                        <div class="instead-empty-message"><?= Yii::t('app', 'No comments yet') ?></div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                </div>
                <div id="accepted-invest" class="tab accepted-invest-list">
                    <?php foreach ($project->acceptedInvestments as $invest) : ?>
                        <div class="post-menu">
                            <div class="post-content">
                                <div class="post-image">
                                    <img src="<?= Helper::getThumb($invest->user->photo, 75, 100); ?>" alt="">
                                </div>
                                <div class="info">
                                    <p class="title"><?= Yii::t('app', 'User: ') . $invest->user->username ?></p>
                                    <p><?= Yii::t('app', 'Amount: ') . $invest->amount ?> $</p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="slider-bottom">
                                    <p><?= $invest->timeCreated ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    <?php if ($project->acceptedInvestments == null): ?>
                        <div class="instead-empty-message"><?= Yii::t('app', 'No accepted invest') ?></div>
                    <?php endif ?>
                </div>
                <?php if (!Yii::$app->user->isGuest && $project->user_id == Yii::$app->user->identity->getId()) : ?>
                    <div id="sent-invest" class="tab sent-invest-list">
                        <?php foreach ($project->sentInvestments as $invest) : ?>
                            <div class="post-menu">
                                <div class="post-content">
                                    <div class="post-image">
                                        <img src="<?= Helper::getThumb($invest->user->photo, 75, 100) ?>" alt="">
                                    </div>
                                    <div class="info">
                                        <p class="title"><?= Yii::t('app', 'User: ') . $invest->user->username ?></p>
                                        <p><?= Yii::t('app', 'Amount: ') . $invest->amount ?> $</p>
                                    </div>
                                    <div class="accept-button">
                                        <a href="#" class="accept-link"
                                           data-invest="<?= $invest->id ?>"><?= Yii::t('app', 'Accept') ?></a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="slider-bottom">
                                        <p><?= $invest->timeCreated ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <?php if ($project->sentInvestments == null): ?>
                            <div class="instead-empty-message"><?= Yii::t('app', 'Nobody sent invest') ?></div>
                        <?php endif ?>
                    </div>
                <?php endif ?>
            </div>
        </div>

        <div class="post-footer"></div>
        <div class="clearfix"></div>
    </div>
</div>
