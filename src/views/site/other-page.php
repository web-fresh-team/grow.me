<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

?>
<div class="wrapper-footer">
    <div class="content" >
    <div class="other-page-content">
        <div class="container">
            <h1>The text of the fish on site for investors and startups</h1>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur assumenda blanditiis culpa cupiditate eligendi error explicabo illo incidunt mollitia, odio perferendis quae qui quidem reiciendis sequi sint ullam vel voluptates!
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias, aliquam aperiam debitis ducimus earum error fugit nemo nesciunt officiis pariatur perspiciatis quam repellat sunt totam ullam velit veritatis, voluptate.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda at culpa cupiditate dolorem eaque explicabo fuga fugiat, fugit ipsa labore maxime neque omnis perferendis possimus quos rerum temporibus tenetur!

        </div>
        <div class="clearfix"></div>
        <div class="container">
            <div class="pages-pagination">
                <ul>
                    <li><a href="#">&#8592;</a></li>
                    <li><a href="#">1</a></li>
                    <li><span>...</span></li>
                    <li><a href="#">7</a></li>
                    <li class="current"><a href="#"><div>8</div></a></li>
                    <li><a href="#">9</a></li>
                    <li><span>...</span></li>
                    <li><a href="#">85</a></li>
                    <li><a href="#">&#8594;</a></li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>