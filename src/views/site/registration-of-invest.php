<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<div class="wrapper-footer">
    <div class="content" style="padding-top: 30px;">
    <div class="popular">
        <div class="container">
            <div class="projects">
                <div class="col">
                    <div class="card">
                        <div class="thumb"><img src="/img/idea-tumb-1.jpg"/></div>
                        <div class="cost">$785,700 invested</div>
                        <div class="desc">
                            <div class="name"><span>Fast comprehensive balanced nutrition business</span></div>
                            <div class="list">
                                <ul>
                                    <li>$546,000 sales so far in 2015</li>
                                    <li>78% mon / mon revenue growth Jan - Apr before selling out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="thumb"><img src="/img/idea-tumb-1.jpg"/></div>
                        <div class="cost">$785,700 invested</div>
                        <div class="desc">
                            <div class="name"><span>Fast comprehensive balanced nutrition business</span></div>
                            <div class="list">
                                <ul>
                                    <li>$546,000 sales so far in 2015</li>
                                    <li>78% mon / mon revenue growth Jan - Apr before selling out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="thumb"><img src="/img/idea-tumb-1.jpg"/></div>
                        <div class="cost">$785,700 invested</div>
                        <div class="desc">
                            <div class="name"><span>Fast comprehensive balanced nutrition business</span></div>
                            <div class="list">
                                <ul>
                                    <li>$546,000 sales so far in 2015</li>
                                    <li>78% mon / mon revenue growth Jan - Apr before selling out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="form-fill">
            <h1>form to fill</h1>
            <p>
                You`ll have the Slurm you can drink when you`re partying.
            </p>
            <div class="middle">
                <div class="left-side">
                    <form action="">
                        <div class="input-text">
                            <input type="text" placeholder="Email">
                        </div>
                        <div class="input-pass">
                            <input type="password" placeholder="Password">
                        </div>
                        <input type="submit" value="Sign Up" class="button">
                    </form>
                </div>
                <div class="or">
                    <p>
                        or
                    </p>
                </div>
                <div class="right-side">
                    <button class="facebook"><i><img src="/img/regi-invest/facebook-icon.png"></i> Sign Up with Facebook </button>
                    <button class="twitter"><i><img src="/img/regi-invest/tw-icon.png"></i> Sign Up with Twitter </button>
                    <button class="google"><i><img src="/img/regi-invest/google_icon.png"></i> Sign Up with Google+ </button>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="bottom-text">
                <p>
                    Bender, hurry! This fuel`s  expensive! Also, we`re dying! Bender, you risked your life to save me!
                </p>
                <a href="">Already Registered?</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>