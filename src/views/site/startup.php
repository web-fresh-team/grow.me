<?php
use yii\helpers\Url;
use app\assets\PerfectScrollBar;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
PerfectScrollBar::register($this);
?>
<div class="wrapper-footer">
    <div class="content" >
    <div class="startup-content">
        <div class="container" id="categoryBoard">
            <div class="category-board" >
                <div class="title">
                    <p>
                        category 1
                    </p>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="category-board" >
                <div class="title">
                    <p>
                        category 1
                    </p>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="category-board" >
                <div class="title">
                    <p>
                        category 1
                    </p>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="category-board" >
                <div class="title">
                    <p>
                        category 1
                    </p>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="category-board" >
                <div class="title">
                    <p>
                        category 1
                    </p>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="category-board" >
                <div class="title">
                    <p>
                        category 1
                    </p>
                </div>
                <div class="list">
                    <ul>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                        <li>
                            <a href="#">
                                <div>
                                    <img src="/img/startup/startup-image.png">
                                    <div class="text">
                                        <p>Description startup</p>
                                        <p>$215</p>
                                    </div>
                                </div>

                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</div>