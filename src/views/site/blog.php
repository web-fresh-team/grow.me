<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContactForm */

/* @var $posts app\modules\blog\models\BlogPost [] */

$this->title = Yii::t('app', 'Blog');
?>

<div class="wrapper-footer">
    <div class="container">

        <?php if (empty($posts)): ?>
            <div class="no-friend">
                <p><?= Yii::t('app', 'No posts here yet') ?></p>
            </div>
        <?php endif; ?>
        <?php $i = 0; foreach ($posts as $post): ?>
            <div class="block" id="post<?= $post->id ?>">
                <div class="text-<?= ($i++) % 2 + 1 ?>">
                    <p class="cust-title"><?= $post->title ?></p>

                    <p><?= $post->text ?></p>
                </div>
                <?php if (!empty($post->photo)): ?>
                    <div class="img-block">
                        <a href="<?= $post->photo ?>" target="_blank">
                            <img src="<?= $post->photo ?>">
                        </a>
                    </div>
                <?php endif ?>
            </div>

        <?php endforeach; ?>

        <div class="clearfix"></div>
    </div>
</div>
