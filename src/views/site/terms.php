<?php
/** @var $this \yii\web\View */
$this->title = Yii::t('app', 'Terms')
?>
<div class="wrapper-footer">
    <div class="container">
        <div>
            <h1>
                <?= $this->title ?>
            </h1>

            <p>Welcome to GrowMe. Our goal is to introduce entrepreneurs and sophisticated investors and simplify
                the
                whole
                capital raising process for early stage businesses.</p>

            <p>This website is not directed to any person in any jurisdiction where (by reason of that person's
                nationality,
                residence or otherwise) the publication or availability of the website is prohibited. Persons in
                respect
                of whom
                such prohibitions apply must not access this website. With respect to persons accessing this website
                from the United
                Kingdom and the European Union, references herein to "GrowMe, LLC" or "Company" shall mean its
                affiliate,
                "GrowMe Limited," a company registered in England and Wales. With respect to persons accessing this
                website from
                outside of the United States, references to "Accredited Investor" status shall include all relevant
                investor
                sophistication standard(s) applicable to persons in each such jurisdiction seeking to make private
                venture
                investments of such nature as enabled by this site. Specifically, references to "Accredited
                Investors"
                accessing
                this site from the United Kingdom are those persons who have been certified as a High Net Worth
                Individual or Self
                Certified Sophisticated Investor in accordance with the UK Financial Services and Markets Act 2000
                (Financial
                Promotion) Order 2005.</p>

            <p>Users of the website may open an account as an "Entrepreneur" (who are looking for advice and an
                introduction to
                potential investors), as an "Investor" (who must complete our <a href="/terms#accredited-investor"
                                                                                 class="def-anchor">Accredited
                    Investor</a>/sophistication
                questionnaire), or they may view content made available to the public (which generally will not
                include
                certain
                information about business opportunities or Investors that may only be viewed by Investors and
                Entrepreneurs).
                Investors may be direct investors in companies, or may be participants in one or more "syndicates"
                in
                which an
                opportunity to invest is shared among several Investors.</p>

            <p>The only people who are authorized to create accounts on this site are professionals who understand
                risk
                and are
                willing to bear the consequences. If you aren't an Entrepreneur looking for advice and an
                introduction
                to potential
                investors or you don't think that you are both an <a href="/terms#accredited-investor"
                                                                     class="def-anchor">Accredited
                    Investor</a> and sophisticated enough to protect your own interests, then you should not try to
                create an
                account on this site.</p>

            <p>By using GrowMe (including by simply viewing content on angel.co (the "Site"), you are agreeing that
                you, and each
                person you allow to access GrowMe through your account, will abide by the terms of the following
                agreement, which
                is summarized here, and set forth in its entirety below. This agreement is between you and GrowMe,
                LLC ("We,"
                "GrowMe" or the "Company"), and it governs your access to and use of the services, websites, and
                applications
                offered by GrowMe (the "Service"). Your access to and use of the Service is conditioned on your
                acceptance of and
                compliance with this agreement.</p>

            <h2>Summary</h2>

            <h3>General Principles</h3>

            <p>The security laws applicable to private company finance are complicated and occasionally ambiguous.
                In
                general, they
                are designed to protect unsophisticated people from making ill-advised investments. The following
                terms
                are designed
                to minimize the risk that any unsophisticated/unaccredited individuals try to become investors
                through
                the Service,
                and to protect the Company in the event that anyone claims he, she or it was damaged by using the
                Services.</p>

            <p>ENTREPRENEURS: If you are an Entrepreneur, you are welcome to upload information about your business
                plan, including
                information you consider confidential (see the definition of Locked Information, below), and to
                designate which
                Investors may see that information (either by listing specific investors, or by indicating
                categories of
                Investors
                that may see your "Locked" information). We will do our best to display information you have
                designated
                as "Locked"
                only to Investors who have told us that they fit within the categories you have identified. We
                cannot,
                however,
                require Investors who see your Locked information to agree not to distribute that information . We
                also
                cannot
                guarantee that there will never be a software bug or a hacker attack that will allow unauthorized
                viewing of
                material, or that Investors actually fit within any category.</p>

            <p>INVESTORS: If you are an <a href="/terms#accredited-investor" class="def-anchor">Accredited
                    Investor</a>
                you must
                recognize that it is important to use discretion in how you handle that Locked Information. You will
                also want to
                select what kinds of Entrepreneurs you would like an introduction to and what kinds of business
                plans
                you would like
                to see. We do not promise to show you all qualifying business plans, nor can we promise that all
                business plans that
                we show you will actually satisfy your criteria, but we will try.</p>

            <p>GENERAL USERS: Certain portions of the GrowMe site will be visible to users who have not signed up as
                either
                Entrepreneurs or as Investors. Those users, nevertheless, are bound by this agreement. Entrepreneurs
                should be aware
                that information they provide that is not designated as "Locked" will be visible to everybody.</p>

            <h3>What We are Promising</h3>

            <p>Our obligations under this agreement are limited to two elements (A) we will require all Investors on
                the
                site to
                complete an "Accredited Investor Questionnaire"; and (B) we will do our best to disclose <a
                    href="/terms#content"
                    class="def-anchor">Content</a>
                submitted by Entrepreneurs only in accordance with the instructions of those Entrepreneurs and only
                to
                Investors who
                have indicated an interest in such materials. Other than that, you are on your own.</p>

            <p>We do not, and cannot, guarantee that any <a href="/terms#content" class="def-anchor">Content</a> is
                true, correct,
                complete or viable.</p>

            <h3>What You are Promising</h3>

            <p>You are promising to use GrowMe responsibly and in a professional manner, and to hold us harmless
                against any
                damage that may happen to you or us as a result of your use of GrowMe.</p>

            <h3>Arbitration</h3>

            <p>Users have waived the right to bring suit in a court and/or to participate in "class action" suits.
                Instead, we will
                use binding arbitration with each party in the event of a dispute.</p>

            <h3>Other Rights We Have</h3>

            <p>Users of GrowMe must understand that we have virtually unlimited rights to reject any Investor or
                Entrepreneur, to
                delete any Content, or to publish any Content provided to us by an Entrepreneur or an Investor on
                GrowMe or in
                any other medium (except Content marked as "Locked"), including for the purposes of marketing our
                Service.</p>

            <h3>There are Things We are Absolutely Not Obligated to Do:</h3>

            <p>We are not obligated to display your <a href="/terms#content" class="def-anchor">Content</a> to any
                other
                user, nor
                are we obligated to introduce you to any Entrepreneur or Investor. We are not responsible for doing
                diligence on the
                investors you meet through GrowMe or on business plans available through GrowMe - You are. We are
                not
                responsible for verifying that that any potential investor is accredited (except in relation to
                Investors from the
                United Kingdom) or otherwise authorized or appropriate to invest in you - You are. Similarly, we do
                not
                recommend
                any startups for investment or endorse their fitness for investment, verify the information on the
                site
                or in our
                emails and we don't claim any of that information is accurate - again, that's your job. You need to
                make
                up your own
                mind and decide what risks you want to take. In particular, no part of this website is intended to
                constitute
                investment advice. You should seek your own independent advice.</p>

            <h3>Privacy and Protection of User Information</h3>

            <p>The Company does not promise to maintain the confidentiality of information you disclose to users on
                our
                site, and in
                fact intends to publish most Entrepreneurs' <a href="/terms#content" class="def-anchor">Content</a>
                to
                our members.
                You may ask us to show your information only to a select set of Investors, and we will do our best
                to
                limit
                disclosure to those Investors, but we cannot promise that nobody else will see your information on
                or
                site, or what
                the people who are allowed to see your information do with it. If you want your information to
                remain
                private, don't
                make it available on our side to other users.</p>

            <p>Personal information we collect from users will be maintained consistent with our Privacy Policy,
                such as
                information
                substantiating your status as an <a href="/terms#accredited-investor" class="def-anchor">Accredited
                    Investor</a> or
                a <a href="/terms#qualified-purchaser" class="def-anchor">Qualified Purchaser</a> (or, for non-U.S.
                persons, a
                sophisticated investor as defined under local regulations). However, other users may still be able
                to
                infer certain
                facts about your income and net worth from your qualification as an investor on GrowMe).</p>

            <h3>Disputes with Others</h3>

            <p>If disputes arise between you and anyone other than the Company, we have no obligation to participate
                or
                assist
                either party.</p>

            <h3>Disclaimers; Limitations; Waivers Of Liability</h3>

            <p>These are exceptions our attorneys and investors make us include to ensure that we are not at risk
                for
                significant
                liabilities. Since we are offering the Service free of charge, we hope you'll understand.</p>

            <h3>Term And Termination</h3>

            <p>This agreement will remain in effect between you and the Company unless either the Company terminates
                it,
                or you
                terminate it by deleting all your Content from the Service, closing your account or you cease to
                view
                Content
                accessible through the Site. After termination, certain relevant provisions (such as the indemnity
                section) will
                remain in force.</p>

            <h3>Copyright Policy</h3>

            <p>GrowMe respects the intellectual property rights of others and expects users of the Service to do the
                same. We
                will respond to notices of alleged copyright infringement that comply with applicable law and are
                properly provided
                to us in accordance with the Copyright Policy set forth herein.</p>

            <h3>Miscellaneous</h3>

            <p>These are all the things that make a contract enforceable and unambiguous, address the rules
                applicable
                to any
                disputes between you and the Company, and cover certain issues related to international users.</p>

            <h3>Definitions</h3>

            <p>Most terms used in this agreement are defined in this section. For your convenience, when those terms
                are
                used the
                first time, they are hyperlinked to their definition in this section.</p>

            <h2>Agreement</h2>
            <ol class="root contents">
                <li><a href="/terms#general-principles" class="def-anchor agreement-section-link">General
                        Principles</a>
                </li>
                <li><a href="/terms#what-we-are-promising" class="def-anchor agreement-section-link">What we are
                        promising</a></li>
                <li><a href="/terms#what-you-are-promising" class="def-anchor agreement-section-link">What you are
                        promising</a>
                </li>
                <li><a href="/terms#arbitration" class="def-anchor agreement-section-link">Arbitration</a></li>
                <li><a href="/terms#other-rights-we-have" class="def-anchor agreement-section-link">Other rights we
                        have</a></li>
                <li><a href="/terms#there-are-things-we-are-absolutely-not-obligated-to-do"
                       class="def-anchor agreement-section-link">There are things we are absolutely not obligated to
                        do</a></li>
                <li><a href="/terms#privacy-and-protection-of-personal-information"
                       class="def-anchor agreement-section-link">Privacy
                        and protection of personal information</a></li>
                <li><a href="/terms#disputes-with-others" class="def-anchor agreement-section-link">Disputes with
                        others</a></li>
                <li><a href="/terms#disclaimers-limitations-waivers-of-liability"
                       class="def-anchor agreement-section-link">disclaimers;
                        limitations; waivers of liability</a></li>
                <li><a href="/terms#term-and-termination" class="def-anchor agreement-section-link">term and
                        termination</a></li>
                <li><a href="/terms#copyright-policy" class="def-anchor agreement-section-link">copyright policy</a>
                </li>
                <li><a href="/terms#miscellaneous" class="def-anchor agreement-section-link">miscellaneous</a></li>
                <li><a href="/terms#definitions" class="def-anchor agreement-section-link">definitions</a></li>
            </ol>
            <ol class="root">
                <li>
                    <p>
                        <a class="definition agreement-section-title" name="general-principles">General
                            Principles</a>
                    </p>

                    <p>Federal securities law requires securities sold in the United States to be registered with
                        the
                        Securities and
                        Exchange Commission, unless the sale qualifies for an exemption. Generally, start-up
                        ventures
                        use one or
                        more of the "private placement" exemptions because they allow them to raise capital without
                        complying with
                        the costly and time-consuming registration process. One of the requirements of the "private
                        placement"
                        exemption is that neither the company which is offering its securities nor any person acting
                        on
                        the Issuer's
                        behalf may offer or sell the securities by any form of "general solicitation". Furthermore,
                        many
                        states and
                        foreign countries all have jurisdiction-specific regulations governing securities
                        transactions
                        that must be
                        observed. You must make your own assessment regarding regulatory requirements as may be
                        applied
                        to your
                        activities on our site.</p>
                </li>
                <li>
                    <a class="definition agreement-section-title" name="what-we-are-promising">What we are
                        promising</a>
                    <ol class="alpha">
                        <li>
                            <a class="definition strong" name="ii-a">Grant You the Right to use the Service.</a> All
                            right, title,
                            and interest in and to the Service (excluding Content provided by users) is and will
                            remain
                            the
                            exclusive property of GrowMe and its licensors. The Service is protected by copyright,
                            trademark, and
                            other laws of both the United States and foreign countries. Except as expressly provided
                            herein, nothing
                            in this agreement gives you a right to use the GrowMe name or any of the GrowMe
                            trademarks, logos,
                            domain names, or other distinctive brand features. Subject to your acceptance of this
                            agreement,
                            GrowMe grants to you a worldwide, non-assignable, non-exclusive, non-transferable,
                            revocable limited
                            license to use the Service and related software and to display the results of such
                            Service
                            anywhere on
                            the rest of the web, other than on websites one of the principal purposes of which is to
                            compete with
                            GrowMe provided that you,
                            <ol class="alpha">
                                <li>do not modify the Content;</li>
                                <li>attribute GrowMe with a human and machine-followable link (an A tag) linking
                                    back
                                    to the page
                                    displaying the original source of the content on GrowMe;
                                </li>
                                <li>upon request, either by GrowMe or by a user who provided the Content, make a
                                    reasonable
                                    effort to update a particular piece of Content to the latest version on GrowMe;
                                    and
                                </li>
                                <li>upon request, either by GrowMe or by a user who contributed to the Content, make
                                    a reasonable
                                    attempt to delete Content that has been deleted on GrowMe.
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong>Not To Claim Any Rights in Your Information.</strong>
                            We are promising not to claim any ownership interest in the Content provided by you to
                            us
                            solely because
                            you have provided it, although we will claim a license sufficient to display such items
                            through
                            GrowMe.
                        </li>
                        <li>
                            <strong>Require all New Members to Execute an "Accredited Investor
                                Questionnaire".</strong>
                            We will require all new users of GrowMe who identify themselves as Investors to execute
                            an Accredited
                            Investor Questionnaire. We have created a questionnaire that is designed to make members
                            think about
                            whether they really are accredited and sophisticated, but we do not verify the
                            representations they
                            make.
                        </li>
                        <li>
                            <strong>Display Entrepreneurs' Content and Allow Communications with Investors Only as
                                Instructed by the
                                Entrepreneur/Investor.</strong>
                            When you create an account with GrowMe, and at any time thereafter, you are able to set
                            a
                            variety of
                            filters that will control who is supposed to be able to see your information and what
                            information you
                            will be shown. We promise not to intentionally violate those filters, although you must
                            recognize that
                            we cannot guarantee that there will never be a software bug or a hacker attack that will
                            allow
                            unauthorized viewing of material or unsolicited contacts to occur.
                        </li>
                    </ol>
                </li>
                <li>
                    <a class="definition agreement-section-title" name="what-you-are-promising">What you are
                        promising</a>
                    <ol class="alpha">
                        <li>
                            <strong>To Act Responsibly.</strong>
                            You are promising to act responsibly - which means:
                            <ol class="lower-roman">
                                <li>
                                    You are making the following Promises:
                                    <ol class="decimal">
                                        <li>
                                            <em>Requirements of Membership.</em>
                                            <ol class="alpha">
                                                <li>
                                                    That you have the right, authority, and capacity to enter into
                                                    this
                                                    agreement on
                                                    your own behalf and on behalf on any entity for whom you are
                                                    acting
                                                    and to abide
                                                    by all of the terms and conditions contained herein, and that if
                                                    any
                                                    aspect of
                                                    your participation in GrowMe violates provisions of local law to
                                                    which you
                                                    are subject, you will cease using the Service and close your
                                                    account,
                                                </li>
                                                <li>
                                                    That you are at least 13 years old, and that if you are less
                                                    than 18
                                                    years old,
                                                    your parent or legal guardian has agreed to stand behind any
                                                    agreement you enter
                                                    into as a participant on GrowMe;
                                                </li>
                                                <li>
                                                    That you will maintain an <a
                                                        href="/terms#allowable-user-identity"
                                                        class="def-anchor">Allowable User
                                                        Identity</a>;
                                                </li>
                                                <li>
                                                    That you will conduct yourself in a professional manner in all
                                                    your
                                                    interactions
                                                    with GrowMe and with any other user of GrowMe;
                                                </li>
                                            </ol>
                                        </li>
                                        <li>
                                            <em>Requirements related to Content on GrowMe.</em>
                                            <ol class="alpha">
                                                <li>
                                                    That you will only provide GrowMe with Content that you have a
                                                    right to
                                                    provide to GrowMe and to allow GrowMe to display through the
                                                    Service --
                                                    which means that you have adequate rights to all copyrights,
                                                    trademarks, trade
                                                    secrets, intellectual property or other material provided by you
                                                    for
                                                    display by
                                                    GrowMe, and that you understand that any other Content you find
                                                    on or through
                                                    GrowMe is the sole responsibility of the person who originated
                                                    such Content.
                                                </li>
                                                <li>
                                                    That you understand that your Content may be republished and if
                                                    you
                                                    do not have
                                                    the right to submit Content for such use, it may subject you to
                                                    liability.
                                                    GrowMe will not be responsible or liable for any use of your
                                                    Content by
                                                    GrowMe in accordance with this agreement.
                                                </li>
                                                <li>
                                                    That you are not relying on GrowMe and that you understand that
                                                    we do not
                                                    endorse, support, represent or guarantee the completeness,
                                                    truthfulness,
                                                    accuracy, or reliability of any Content or communications posted
                                                    via
                                                    the Service
                                                    or endorse any opinions expressed via the Service.
                                                </li>
                                                <li>
                                                    That you understand that by using the Service, you may be
                                                    exposed to
                                                    Content
                                                    that might be offensive, harmful, inaccurate or otherwise
                                                    inappropriate, and
                                                    that you have no claim against GrowMe for any such material.
                                                </li>
                                                <li>
                                                    That you understand that the Service may include advertisements
                                                    or
                                                    other
                                                    content, which may be targeted to the Content or information on
                                                    the
                                                    Service,
                                                    queries made through the Service, or other information, and you
                                                    have
                                                    no claim
                                                    against GrowMe for the placement of advertising or similar
                                                    content on the
                                                    Service or in connection with the display of Content or
                                                    information
                                                    from the
                                                    Service whether submitted by you or others.
                                                </li>
                                            </ol>
                                        </li>
                                        <li>
                                            <em>Requirements related to Investments.</em>
                                            <ol class="alpha">
                                                <li>
                                                    That you will use your own judgment before making any decision
                                                    to
                                                    invest or to
                                                    accept an investment involving what is to you a material amount
                                                    of
                                                    money.
                                                </li>
                                                <li>
                                                    That you will be solely responsible for complying with
                                                    applicable
                                                    law regarding
                                                    any transaction, including without limitation the determination
                                                    of
                                                    whether any
                                                    investor is an Accredited Investor and whether any investment
                                                    complies with the
                                                    terms of local law (whether the law of a US state, or the law of
                                                    any
                                                    foreign
                                                    government with jurisdiction over you or any investor).
                                                </li>
                                                <li>
                                                    That you will obtain such professional advice as is appropriate
                                                    to
                                                    protect your
                                                    interests, including legal, accounting and other advice (i.e.,
                                                    get a
                                                    good
                                                    start-up attorney).
                                                </li>
                                                <li>
                                                    That you have reviewed and understand the discussion of risks
                                                    found
                                                    <a
                                                        href="/risks">here</a>, and that you are otherwise aware of
                                                    the
                                                    risks of any
                                                    investment of the nature of angel investments.
                                                </li>
                                                <li>
                                                    Syndicates:
                                                    <ol class="decimal">
                                                        <li>
                                                            That you understand that GrowMe is not acting as an
                                                            investment
                                                            advisor or in any other capacity in relation to
                                                            "Syndicates"
                                                            (described
                                                            <a href="/syndicates">here</a>), and that the lead
                                                            investor
                                                            in a
                                                            Syndicate is also not providing any such advice or in
                                                            any
                                                            way
                                                            responsible for the success or failure of that
                                                            investment
                                                            and you are
                                                            not relying on his or her advice.
                                                        </li>
                                                        <li>
                                                            You specifically agree and acknowledge that you are not
                                                            relying upon any
                                                            Person, other than the underlying issuer and its
                                                            officers
                                                            and directors,
                                                            in making its investment or decision to invest in any
                                                            Syndicate. You
                                                            further agree that no sponsor or other participant in
                                                            any
                                                            Syndicate nor
                                                            the respective controlling Persons, officers, directors,
                                                            partners,
                                                            agents, or employees of any such person shall be liable
                                                            to
                                                            any other
                                                            Investor for any action heretofore taken or omitted to
                                                            be
                                                            taken by any
                                                            of them in connection with an investment through a
                                                            Syndicate.
                                                        </li>
                                                    </ol>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    You are promising not to:
                                    <ol class="decimal">
                                        <li>
                                            Violate the <a href="/terms#community-rules" class="def-anchor">Community
                                                Rules</a> of
                                            GrowMe.
                                        </li>
                                        <li>
                                            Make any <a href="/terms#inappropriate-uses" class="def-anchor">Inappropriate
                                                Uses of
                                                the System</a>.
                                        </li>
                                        <li>
                                            Expect GrowMe to evaluate, confirm, endorse, or otherwise stand behind
                                            any person's
                                            statements or recommend any investment, or treat any email or other
                                            information you
                                            receive as a result of your access to the Service as a representation of
                                            any
                                            kind by
                                            GrowMe or any Company Person on which you should rely.
                                        </li>
                                        <li>
                                            Claim any ownership right in any material, software or other
                                            intellectual
                                            property
                                            displayed on, published by or otherwise available through GrowMe, other
                                            than Content,
                                            software or intellectual property that you own or otherwise have rights
                                            to
                                            without
                                            regard for its appearance on GrowMe.
                                        </li>
                                        <li>
                                            Copy or distribute the content of the Service except as specifically
                                            allowed
                                            in this
                                            agreement.
                                        </li>
                                        <li>
                                            Claim any right to access, view or alter any source code or object code
                                            of
                                            GrowMe.
                                        </li>
                                        <li>
                                            Use GrowMe to market services, particularly investment advisory
                                            services,
                                            that might
                                            cause GrowMe to have to register as a broker dealer with the SEC, or to
                                            be treated as
                                            an underwriter.
                                        </li>
                                        <li>
                                            Market competing services to people you've identified through GrowMe.
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong>Indemnify GrowMe and Related Parties.</strong>
                            You are promising to hold us harmless against any damage that may happen to us as a
                            result
                            of your use
                            of GrowMe.
                            <ol class="lower-roman">
                                <li>
                                    <em>Indemnity.</em>
                                    In particular, you agree to indemnify and hold the Company (and any employee,
                                    officer, director
                                    or affiliate of the Company, each a "Company Person") harmless (including costs
                                    and
                                    attorneys'
                                    fees) from any claim or demand made by any third party due to or arising out of
                                    your
                                    access to
                                    or use of the Service, the violation of this agreement by you, the infringement
                                    by
                                    you, or any
                                    third party using your account, of any intellectual property or other right of
                                    any
                                    person or
                                    entity, or for any content posted through the Service by you (including claims
                                    related to
                                    defamation, invasion of privacy, or other violation of a person's rights). Your
                                    obligations
                                    under the foregoing indemnity may not be offset against any other claim you may
                                    have
                                    against the
                                    Company or any Company Person. You remain solely responsible for all content
                                    that
                                    you upload,
                                    post, email, transmit, or otherwise disseminate using, or in connection with,
                                    the
                                    Service. You
                                    agree that the provisions in this paragraph will survive any termination of your
                                    account(s) or
                                    the Service.
                                </li>
                                <li>
                                    <em>Release.</em>
                                    In addition, you hereby release any claims you may have against GrowMe and any
                                    Company Person
                                    that are in anyway related to the Service or your use of Content offered through
                                    the
                                    Service,
                                    including any recommendations or referrals you may receive as a result of your
                                    registration with
                                    GrowMe. You are solely responsible for your use of the Service, for any Content
                                    you provide,
                                    and for any consequences thereof, including the use of your Content by other
                                    users
                                    and third
                                    parties partners.
                                </li>
                                <li>
                                    <em>GrowMe Employees and Affiliates.</em>
                                    You understand that GrowMe employees and affiliates may participate in the
                                    Service as
                                    Entrepreneurs or Investors, and that GrowMe is not responsible for any of their
                                    activities,
                                    including statements or other information in any emails or other communications
                                    such
                                    individuals
                                    make in that capacity.
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong>Confidentiality.</strong>
                            By becoming member of GrowMe, or otherwise using the Services or viewing content made
                            available
                            through GrowMe in any way, you may have an opportunity to see <a
                                href="/terms#locked-information"
                                class="def-anchor">Locked
                                Information</a>. It is expected that you will use discretion in determining what you
                            do
                            with that
                            information. You agree, however, that you will not republish any information you acquire
                            through the
                            Service via an internet website, one of the principal purposes of which is to compete
                            with
                            GrowMe.
                        </li>
                        <li>
                            <strong>Other Promises Necessary to Allow Us to Provide the Service.</strong>
                            <ol class="lower-roman">
                                <li>
                                    You promise to comply with the terms of the license set forth in <a
                                        href="/terms#ii-a"
                                        class="def-anchor">Section
                                        II.a</a>.
                                </li>
                                <li>
                                    You are licensing to us the right to publish all the Content you upload to
                                    GrowMe, including
                                    any comments or other forum posts you may offer on the site in order to provide
                                    the
                                    Service.
                                    Your Content will be viewable by other users of the Service and through third
                                    party
                                    services and
                                    websites. You should only provide Content that you are comfortable sharing with
                                    others under
                                    this agreement.
                                </li>
                                <li>
                                    To the extent that GrowMe is determined, for any reason not to be the licensee
                                    of
                                    any
                                    material you have provided to us (including all rights of paternity, integrity,
                                    disclosure and
                                    withdrawal and any other rights that may be known as or referred to as "moral
                                    rights," "artist's
                                    rights," "droit moral," or the like (collectively "Moral Rights")), you hereby
                                    ratify and
                                    consent to any action that may be taken with respect to such Moral Rights by or
                                    authorized by
                                    GrowMe and agree not to assert any Moral Rights with respect thereto. You
                                    further
                                    agree that
                                    you will confirm any such ratifications, consents and agreements from time to
                                    time
                                    as requested
                                    by the Company.
                                </li>
                                <li>
                                    You acknowledge that GrowMe is not obligated to pay you or to cause any other
                                    party to pay
                                    you anything with respect to your activities on GrowMe, or to feature or
                                    otherwise display
                                    your Content on any web page.
                                </li>
                                <li>
                                    You acknowledge that you do not rely on the Company to monitor or edit the
                                    Service
                                    (including
                                    emails initiated by individuals, regardless of whether those individuals are
                                    otherwise
                                    associated with the Company) and that the Service may contain content which you
                                    find
                                    offensive
                                    or which is untrue or misleading and you hereby waive any objections you might
                                    have
                                    with respect
                                    to viewing such content.
                                </li>
                                <li>
                                    You agree that this agreement does not entitle you to any support, upgrades,
                                    updates, add-ons
                                    patches, enhancements, or fixes for the Services (collectively, "Updates"). The
                                    Company,
                                    however, may occasionally provide automatic Updates to the Services at its sole
                                    discretion (and
                                    without any advanced notification to you). Any such Updates for the Services
                                    shall
                                    become part
                                    of the Services and subject to this agreement.
                                </li>
                                <li>
                                    If you operate, manage or otherwise control a search engine or robot, or you
                                    republish a
                                    significant fraction of GrowMe Content (as we may determine in our reasonable
                                    discretion),
                                    you must additionally follow these rules:
                                    <ol class="decimal">
                                        <li>
                                            You must use a descriptive user agent header.
                                        </li>
                                        <li>
                                            You must follow robots.txt at all times.
                                        </li>
                                        <li>
                                            You must make it clear how to contact you, either in your user agent
                                            string,
                                            or on your
                                            website if you have one.
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong>APIs available through the Service.</strong>
                            We may make available one or more APIs for interacting with the Service. Your use of any
                            GrowMe API
                            is subject to this agreement and the GrowMe API Rules, which will be posted before we
                            make these APIs
                            available (and will become part of this agreement).
                        </li>
                    </ol>
                </li>
                <li>
                    <p>
                        <a class="definition agreement-section-title" name="arbitration">Arbitration</a>
                    </p>

                    <p>
                        Any and all controversies, claims or disputes arising out of or relating to this Agreement
                        (including,
                        without limitation, its formation, performance or breach) or arising out of or relating to
                        the
                        relationship
                        between the parties
                        (including disputes related to any investment, syndicate or other transaction you undertook
                        through or in
                        connection with your use of the Service) shall be solely and exclusively resolved by
                        confidential binding
                        arbitration as provided
                        in Section XII (b)(iv) "<a href="/terms#binding-arbitration" class="def-anchor">Binding
                            Arbitration</a>".
                        The claimant shall have the right to participate by phone or similar means and need not
                        attend
                        the
                        arbitration live in order to participate, except that the parties will confer on an
                        appropriate
                        method for
                        obtaining the claimant's testimony,
                        including agreeing on a means of obtaining live testimony from the claimant if appropriate,
                        in a
                        manner that
                        minimizes travel and expense burdens on the claimant.
                    </p>
                </li>
                <li>
                    <a class="definition agreement-section-title" name="other-rights-we-have">Other rights we
                        have</a>
                    <ol class="alpha">
                        <li>
                            GrowMe may, but is not required to, monitor or control the Content posted via the
                            Service. Our
                            failure to exercise that right, however, does not give you any right to make a claim
                            against
                            GrowMe.
                        </li>
                        <li>
                            GrowMe reserves the right to discontinue the Service or to change the content of the
                            Service in any
                            way and at any time, with or without notice to you, without liability.
                        </li>
                        <li>
                            GrowMe reserves the right to terminate your access to the Service without notice and, if
                            you violate
                            this agreement to pursue other remedies at law or in equity.
                        </li>
                        <li>
                            We may delete your account for any reason or for no reason at all, and if we delete your
                            account you
                            will lose all access to any information, connections or other features that may have
                            value
                            to you that
                            may have been associated with your account.
                        </li>
                        <li>
                            The Company has the right to refuse registration of, or cancel your user account and/or
                            User
                            ID in its
                            discretion for any reason or for no reason. In addition, the Company reserves the right
                            at
                            all times
                            (but will not have an obligation) to remove or refuse to distribute any Content on the
                            Service and to
                            terminate users or reclaim usernames. We also reserve the right to access, read,
                            preserve,
                            and disclose
                            any information as we reasonably believe is necessary to (i) satisfy any applicable law,
                            regulation,
                            legal process or governmental request, (ii) enforce this agreement (including
                            investigation
                            of potential
                            violations hereof), (iii) detect, prevent, or otherwise address fraud, security or
                            technical
                            issues,
                            (iv) respond to user support requests, or (v) protect the rights, property or safety of
                            GrowMe, its
                            users and the public.
                        </li>
                        <li>
                            Without limiting the generality of the foregoing, you specifically acknowledge that the
                            Company has the
                            right to terminate or limit your account for any reason or no reason at all. Any Content
                            that has been
                            uploaded through the Service may be deleted at any time without notice to you. The
                            Company
                            is exempt
                            from liability to any person for any claim based upon its termination of an account or
                            disabling of
                            access to or removal of any Content, including material it believes, in its sole
                            discretion
                            to violate
                            this agreement, regardless of whether the material ultimately is determined to be
                            infringing
                            or
                            otherwise prohibited, and regardless of whether such termination or disabling has the
                            effect
                            of reducing
                            the value of any Content or opportunities that might otherwise have been available to
                            you.
                            By using the
                            Service, you agree that notice to you through an email to the email address you provided
                            in
                            your profile
                            constitutes reasonable efforts to notify you of any removal or disabling if such notice
                            is
                            required.
                        </li>
                        <li>
                            Comments, Feedback, Suggestions, Ideas, And Other Submissions. The Service may invite
                            you to
                            chat or
                            participate in blogs, message boards, online forums and other functionality and may
                            provide
                            you with the
                            opportunity to create, submit, post, display, transmit, perform, publish, distribute or
                            broadcast
                            content to the Company and/or to or via the Service, including, without limitation,
                            text,
                            writings,
                            photographs, graphics, comments, suggestions, intellectual property or personally
                            identifiable
                            information or other material (collectively "User Content"). Any such material you
                            transmit
                            to the
                            Company or otherwise through the Service will be treated as non-confidential and
                            non-proprietary. All
                            comments, feedback, suggestions, ideas, forum posts and other submissions ("Ideas")
                            disclosed,
                            submitted, or offered to the Company in connection with the use of the Service or
                            otherwise
                            and any
                            chat, blog, message board, online forum, text, email or other communication with the
                            Company
                            ("User
                            Emails") shall deemed to have been licensed to the Company on a nonexclusive, worldwide,
                            royalty-free,
                            perpetual basis.
                        </li>
                    </ol>
                </li>
                <li>
                    <a class="definition agreement-section-title"
                       name="there-are-things-we-are-absolutely-not-obligated-to-do">There
                        are things we are absolutely not obligated to do</a>
                    <ol class="alpha">
                        <li>
                            We are not obligated to display your Content to any other user, nor are we obligated to
                            introduce you to
                            any Entrepreneur or Investor.
                        </li>
                        <li>
                            We are not responsible for doing diligence on the users you meet through GrowMe.
                        </li>
                        <li>
                            We are not responsible for verifying that that any Investor is accredited (except in
                            relation to
                            Investors from the United Kingdom) or otherwise authorized or appropriate to invest in
                            you,
                            or for
                            determining whether any use of GrowMe constitutes a general solicitation of securities
                            under US law
                            or the laws of any state or other jurisdiction, including foreign jurisdictions.
                        </li>
                        <li>
                            We do not recommend any startups for investment or endorse their fitness for investment,
                            verify the
                            information on the site or in our emails and we don't claim any of that information is
                            accurate. In
                            particular, we do not act as investment adviser to any Investor(s) and no part of this
                            website is
                            intended to constitute investment advice.
                        </li>
                        <li>
                            We are not obligated to maintain the confidentiality of any Content you give us, other
                            than
                            Locked
                            Information, and with respect to Locked Information, we are not obligated to protect it
                            other than by
                            designating it as such.
                        </li>
                        <li>
                            The Company has no obligation to monitor or enforce any intellectual property rights
                            that
                            may be
                            associated with Content you provide to us, but the Company does have the right to
                            enforce
                            such rights
                            through any means it sees fit, including bringing and controlling actions on your
                            behalf.
                        </li>
                        <li>
                            In the event that the Company invests in any business, we are not obligated to make that
                            investment
                            opportunity available to anyone else.
                        </li>
                        <li>
                            The Company does not control or endorse the content, messages or information found in
                            the
                            Service or
                            external sites that may be linked to or from GrowMe and, therefore, the Company
                            specifically
                            disclaims any responsibility with regard thereto.
                        </li>
                        <li>
                            The Company has no obligation to accept, display, review, monitor, or maintain any
                            Content
                            submitted by
                            users, user forum posts, commentary, ratings or compliments ("Comments"). We have the
                            right
                            to delete
                            Content or Comments from the Service without notice for any reason at any time. The
                            Company
                            may move,
                            re-format, edit, alter, distort, remove or refuse to exploit Content or Comments without
                            notice to you
                            and without liability. Notwithstanding the forgoing rights, the Company reserves the
                            right
                            to treat
                            Content provided by users and Comments as content stored at the direction of users for
                            which
                            the Company
                            will not exercise editorial control except as required to enforce the rights of third
                            parties and
                            applicable content restrictions below when violations are brought to the Company's
                            attention.
                        </li>
                        <li>
                            The Service may contain or deliver advertising and sponsorships. Advertisers and
                            sponsors
                            are
                            responsible for ensuring that material submitted for inclusion is accurate and complies
                            with
                            applicable
                            laws. We are not responsible for the illegality or any error, inaccuracy or problem in
                            the
                            advertiser's
                            or sponsor's Content.
                        </li>
                    </ol>
                </li>
                <li>
                    <a class="definition agreement-section-title"
                       name="privacy-and-protection-of-personal-information">Privacy
                        and
                        protection of personal information</a>
                    <ol class="alpha">
                        <li>
                            GrowMe values your privacy. Please review our
                            <a href="/privacy">Privacy Policy</a>
                            to learn more about how we collect and use information about you via the Service.
                        </li>
                        <li>
                            The Company may collect and collate a variety of information regarding the use of the
                            Service. The
                            Company is the sole owner of all such information it collects. By using the Service you
                            consent to the
                            transfer of your information to the United States and/or other countries for storage,
                            processing and use
                            by GrowMe.
                        </li>
                        <li>
                            The Company uses industry-standard security measures to protect the loss, misuse and
                            alteration of the
                            information under our control. Although we make good faith efforts to store any
                            non-public
                            information
                            uploaded to the Service or collected by the Company in a secure operating environment
                            that
                            is not
                            available to the public, we cannot guarantee complete security. We cannot and do not
                            guarantee that our
                            security measures will prevent third party "hackers" from illegally accessing our site
                            and
                            obtaining
                            access to content or information thereon.
                        </li>
                        <li>
                            The Service may contain links to other websites. We are not responsible for the content,
                            accuracy or
                            opinions expressed in such websites, and such websites are not investigated, monitored
                            or
                            checked for
                            accuracy or completeness by us.
                        </li>
                        <li>
                            The Company reserves the right to reveal your identity (or whatever information we know
                            about you) in
                            the event of a complaint, subpoena, warrant, court order or legal action. The Company
                            may
                            log all
                            internet protocol addresses accessing the Services and other information about users'
                            access, and
                            maintain backup copies of content indefinitely and disclose that information to law
                            enforcement
                            authorities as required.
                            See our <a href="/privacy">Privacy Policy</a>.
                        </li>
                    </ol>
                </li>
                <li>
                    <p>
                        <a class="definition agreement-section-title" name="disputes-with-others">Disputes with
                            others</a>
                    </p>

                    <p>We reserve the right, but have no obligation, to monitor and/or manage disputes between you
                        and
                        other users
                        of the Service. If you have a dispute with other users, you release the Company and hereby
                        agree
                        to
                        indemnify the Company from claims, demands, and damages (actual and consequential) of every
                        kind
                        and nature,
                        known and unknown, arising out of or in any way connected with such dispute.</p>
                </li>
                <li>
                    <a class="definition agreement-section-title"
                       name="disclaimers-limitations-waivers-of-liability">disclaimers;
                        limitations; waivers of liability</a>
                    <ol class="alpha">
                        <li>
                            YOU EXPRESSLY AGREE THAT ACCESS TO AND USE OF THE SERVICE IS AT YOUR SOLE RISK AND IS
                            PROVIDED ON AN "AS
                            IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT
                            LIMITED TO,
                            WARRANTIES OF TITLE OR IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY OR
                            FITNESS
                            FOR A
                            PARTICULAR PURPOSE (EXCEPT ONLY TO THE EXTENT PROHIBITED UNDER THE LAWS APPLICABLE TO
                            TERMS
                            OF USE WITH
                            ANY LEGALLY REQUIRED WARRANTY PERIOD TO THE SHORTER OF THIRTY DAYS FROM FIRST USE OR THE
                            MINIMUM PERIOD
                            REQUIRED). WITHOUT LIMITING THE FOREGOING, NEITHER THE COMPANY NOR ITS AFFILIATES OR
                            SUBSIDIARIES, OR
                            ANY OF THEIR DIRECTORS, EMPLOYEES, AGENTS, ATTORNEYS, THIRD-PARTY CONTENT PROVIDERS,
                            DISTRIBUTORS,
                            LICENSEES OR LICENSORS (COLLECTIVELY, "COMPANY PARTIES") WARRANT THAT THE SERVICE WILL
                            BE
                            UNINTERRUPTED
                            OR ERROR-FREE.
                        </li>
                        <li>
                            TO THE FULLEST EXTENT PERMITTED BY LAW, THE DISCLAIMERS OF LIABILITY CONTAINED HEREIN
                            APPLY
                            TO ANY AND
                            ALL DAMAGES OR INJURY WHATSOEVER CAUSED BY OR RELATED TO USE OF, OR INABILITY TO USE,
                            THE
                            SERVICE UNDER
                            ANY CAUSE OR ACTION WHATSOEVER OF ANY JURISDICTION, INCLUDING, WITHOUT LIMITATION,
                            ACTIONS
                            FOR BREACH OF
                            WARRANTY, BREACH OF CONTRACT OR TORT AND THAT THE COMPANY PARTIES SHALL NOT BE LIABLE
                            FOR
                            ANY DIRECT,
                            INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES IN ANY WAY WHATSOEVER
                            ARISING OUT OF
                            THE USE OF, OR INABILITY TO USE, THE SERVICE. YOU FURTHER SPECIFICALLY ACKNOWLEDGE THAT
                            THE
                            COMPANY
                            PARTIES ARE NOT LIABLE, AND YOU AGREE NOT TO SEEK TO HOLD THE COMPANY PARTIES LIABLE,
                            FOR
                            THE CONDUCT OF
                            THIRD PARTIES, INCLUDING OTHER USERS OF THE SERVICE AND OPERATORS OF EXTERNAL SITES, AND
                            THAT THE RISK
                            OF THE SERVICE AND EXTERNAL SITES AND OF INJURY FROM THE FOREGOING RESTS ENTIRELY WITH
                            YOU.
                            The Company
                            is not responsible for any technical malfunction or other problems of any telephone
                            network
                            or service,
                            computer systems, servers or providers, computer or mobile phone equipment, software,
                            failure of email
                            or players on account of technical problems or traffic congestion on the internet or at
                            any
                            site or
                            combination thereof, including injury or damage to your or to any other person's
                            computer,
                            mobile phone,
                            or other hardware or software, related to or resulting from using or downloading
                            materials
                            in connection
                            with the web and/or in connection with the Services, including any mobile software.
                            Under no
                            circumstances will the Company be responsible for any loss or damage, including any loss
                            or
                            damage to
                            any content or personal injury or death, resulting from anyone's use of the Service, any
                            content or
                            third party applications, software or content posted on or through the Services or
                            transmitted to users,
                            or any interactions between users of the Service, whether online or offline.
                        </li>
                        <li>
                            We make no warranty and disclaim all responsibility and liability for the completeness,
                            accuracy,
                            availability, timeliness, security or reliability of the Service or any content thereon
                            or
                            any content
                            you receive as a result of your relationship with GrowMe. GrowMe will not be
                            responsible or liable
                            for any harm to your computer system, loss of data, or other harm that results from your
                            access to or
                            use of the Service, or any Content. You also agree that GrowMe has no responsibility or
                            liability for
                            the deletion of, or the failure to store or to transmit, any Content and other
                            communications maintained
                            by the Service. We make no warranty that the Service will meet your requirements or be
                            available on an
                            uninterrupted, secure, or error-free basis. No advice or information, whether oral or
                            written, obtained
                            from GrowMe or through the Service, will create any warranty not expressly made herein
                        </li>
                        <li>
                            UNDER NO CIRCUMSTANCES WILL ANY COMPANY PERSON BE LIABLE TO YOU FOR MORE THAN THE AMOUNT
                            YOU
                            HAVE PAID
                            THE COMPANY IN THE NINETY (90) DAYS IMMEDIATELY PRECEDING THE DATE ON WHICH YOU FIRST
                            ASSERT
                            ANY SUCH
                            CLAIM.
                        </li>
                        <li>
                            To the extent any provision(s) relating to arbitration, disclaimer, waiver of liability,
                            or
                            any other
                            rights and obligations set forth herein, is not permissible or enforceable under foreign
                            laws as applied
                            to users from such foreign jurisdictions, each such provision shall be deemed removed
                            and
                            invalid, but
                            all remaining provisions shall be in full force and effect.
                        </li>
                    </ol>
                </li>
                <li>
                    <p>
                        <a class="definition agreement-section-title" name="term-and-termination">term and
                            termination</a>
                    </p>

                    <p>Unless terminated by the Company, this agreement will remain in full force and effect while
                        you
                        use any of
                        the Services. Subject to the last sentence of this Section X, you may terminate this
                        agreement
                        at any time
                        by deleting all Content you have provided to GrowMe and ceasing to use the Service. The
                        Company may
                        terminate this agreement at any time, particularly if you violate any provision of this
                        agreement. Upon
                        termination of this agreement for any reason, you shall destroy and remove from all
                        computers,
                        and other
                        storage media all copies of the any intellectual property owned by the Company or any other
                        user
                        of the
                        Services that you acquired by use of the Service. Your representations in this agreement and
                        the
                        provisions
                        of Sections III and any other provision which by their nature are designed to survive
                        termination shall
                        survive any termination or expiration of this agreement.</p>
                </li>
                <li>
                    <p>
                        <a class="definition agreement-section-title" name="copyright-policy">copyright policy</a>
                    </p>

                    <p>
                        GrowMe respects the intellectual property rights of others and expects users of the Service
                        to do the
                        same. We will respond to notices of alleged copyright infringement that comply with
                        applicable
                        law and are
                        properly provided to us. The Digital Millennium Copyright Act (DMCA) provides recourse to
                        copyright owners
                        who believe that their rights under the United States Copyright Act have been infringed by
                        acts
                        of third
                        parties over the Internet. If you believe that your copyrighted work has been copied without
                        your
                        authorization and is available on or in the Service in a way that may constitute copyright
                        infringement, you
                        may provide notice of your claim to the Company as outlined in the Company's copyright
                        policy
                        below:
                    </p>

                    <p>
                        Copyright Policy. If You believe that any material on the Service violates this agreement or
                        your
                        intellectual property rights, please notify the Company as soon as possible by sending an
                        email
                        to <a
                            href="mailto:abuse@angel.co">abuse@angel.co</a>, or by mailing a letter to the <a
                            href="/terms#copyright-agent" class="def-anchor">GrowMe Copyright Agent</a> (listed
                        below) containing
                        the following information in accordance with the Digital Millennium Copyright Act: (i) a
                        physical or
                        electronic signature of the copyright owner or a person authorized to act on their behalf;
                        (ii)
                        identification of the copyrighted work claimed to have been infringed; (iii) identification
                        of
                        the material
                        that is claimed to be infringing or to be the subject of infringing activity and that is to
                        be
                        removed or
                        access to which is to be disabled, and information reasonably sufficient to permit us to
                        locate
                        the
                        material; (iv) your contact information, including your address, telephone number, and an
                        email
                        address; (v)
                        a statement by you that you have a good faith belief that use of the material in the manner
                        complained of is
                        not authorized by the copyright owner, its agent, or the law; and (vi) a statement that the
                        information in
                        the notification is accurate, and, under penalty of perjury, that you are authorized to act
                        on
                        behalf of the
                        copyright owner.
                    </p>

                    <p>
                        Our designated copyright agent for notice of alleged copyright infringement or other legal
                        notices regarding
                        Content appearing on the Service is:
                    </p>

                    <p>
                    </p>
                    <address>
                        <a class="definition" name="copyright-agent">GrowMe LLC.</a> ?
                        <br>
                        Attn: Copyright ?
                        <br>
                        16 Maiden Lane?
                        <br>
                        Suite 600
                        <br>
                        ?San Francisco, CA 94108 ?
                        <br>
                        Email: <a href="mailto:copyright@angel.co?subject=GrowMe%20Copyright%20Policy">copyright@angel.co</a>
                    </address>
                    <p></p>

                    <p>In Europe:</p>

                    <p>
                    </p>
                    <address>
                        GrowMe Limited
                        <br>
                        White Bear Yard,
                        <br>
                        144a Clerkenwell Road
                        <br>
                        London EC1R 5DF
                        <br>
                        United Kingdom ?
                    </address>
                    <p></p>

                    <p>
                        Please note that in addition to being forwarded to the person who provided the allegedly
                        illegal
                        content, we
                        may send a copy of your notice (with your personal information removed) to <a
                            href="http://www.chillingeffects.org" rel="nofollow">Chilling Effects</a> for
                        publication
                        and/or
                        annotation. You can see an <a
                            href="http://www.chillingeffects.org/fairuse/notice.cgi?NoticeID=16887"
                            rel="nofollow">example of such a publication</a>. A link to your published
                        notice will be displayed on GrowMe in place of the removed content.
                    </p>

                    <p>
                        We reserve the right to remove Content alleged to be infringing or otherwise illegal without
                        prior notice
                        and at our sole discretion. In appropriate circumstances, GrowMe will also terminate a
                        user's
                        account if
                        the user is determined to be a repeat infringer.
                    </p>
                </li>
                <li>
                    <a class="definition agreement-section-title" name="miscellaneous">miscellaneous</a>
                    <ol class="alpha">
                        <li>
                            <strong>Amendments to this Agreement.</strong>
                            We may amend this agreement at any time in our sole discretion, effective upon posting
                            the
                            amended Terms
                            at the domain of www.angel.co where the prior version of this agreement was posted, or
                            by
                            communicating
                            these changes through any written contact method we have established with you. Your use
                            of
                            the Services
                            following the date on which such amended Terms are published will constitute consent to
                            such
                            amendments.
                        </li>
                        <li>
                            <strong>Governing Law/Resolution Of Disputes/Waiver Of Injunctive Relief</strong>
                            <ol class="lower-roman">
                                <li>
                                    <em>Governing Law/Venue.</em>
                                    This agreement and all aspects of the Service shall be governed by and construed
                                    in
                                    accordance
                                    with the internal laws of the United States and the State of Delaware governing
                                    contracts
                                    entered into and to be fully performed in Delaware (i.e., without regard to
                                    conflict
                                    of law's
                                    provisions) regardless of your location except that the arbitration provision
                                    shall
                                    be governed
                                    by the Federal Arbitration Act. For the purpose of any judicial proceeding to
                                    enforce such award
                                    or incidental to such arbitration or to compel arbitration, or if for any reason
                                    a
                                    claim
                                    proceeds in court rather than in arbitration, the parties hereby submit to the
                                    non-exclusive
                                    jurisdiction of the state and Federal courts sitting in San Francisco, CA, and
                                    agree
                                    that
                                    service of process in such arbitration or court proceedings shall be
                                    satisfactorily
                                    made upon it
                                    if sent by certified, express or registered mail addressed to it at the address
                                    set
                                    forth in the
                                    books and records of the Company, or if no such address has been provided, by
                                    email
                                    to the email
                                    address provided by the relevant parties to the Company in connection with their
                                    use
                                    of the
                                    Service. With respect to any disputes or claims not subject to informal dispute
                                    resolution or
                                    arbitration (as set forth below), you agree not to commence or prosecute any
                                    action
                                    in
                                    connection therewith other than in the state and federal courts located in San
                                    Francisco County,
                                    California, and you hereby consent to, and waive all defenses of lack of
                                    personal
                                    jurisdiction
                                    and forum non conveniens with respect to, venue and jurisdiction in the state
                                    and
                                    federal courts
                                    located in San Francisco County, California.
                                </li>
                                <li>
                                    <em>Injunctive Relief.</em>
                                    You acknowledge that the rights granted and obligations made hereunder to the
                                    Company are of a
                                    unique and irreplaceable nature, the loss of which shall irreparably harm the
                                    Company and which
                                    cannot be replaced by monetary damages alone so that the Company shall be
                                    entitled
                                    to injunctive
                                    or other equitable relief (without the obligations of posting any bond or
                                    surety) in
                                    the event
                                    of any breach or anticipatory breach by you. You irrevocably waive all rights to
                                    seek injunctive
                                    or other equitable relief and agree to limit your claims to claims for monetary
                                    damages (if
                                    any).
                                </li>
                                <li>
                                    <em>Informal Negotiations.</em>
                                    To expedite resolution and control the cost of any dispute, controversy or claim
                                    related to this
                                    agreement ("Dispute"), you and the Company agree to first attempt to negotiate
                                    any
                                    Dispute
                                    (except those Disputes expressly provided below) informally for at least thirty
                                    (30)
                                    days before
                                    initiating any arbitration or court proceeding. Such informal negotiations
                                    commence
                                    upon written
                                    notice from one person to the other. You will send your notice to GrowMe LLC,
                                    153
                                    Townsend
                                    Street, Suite 9059, San Francisco, CA, ATTENTION: LEGAL DEPARTMENT.
                                </li>
                                <li>
                                    <a class="definition em" name="binding-arbitration">Binding Arbitration</a>.
                                    If you and the Company are unable to resolve a Dispute through informal
                                    negotiations, either you
                                    or the Company may elect to have the Dispute (except those Disputes expressly
                                    excluded below)
                                    finally and exclusively resolved by binding arbitration. Any election to
                                    arbitrate
                                    by one party
                                    shall be final and binding on the other. YOU UNDERSTAND THAT ABSENT THIS
                                    PROVISION,
                                    YOU WOULD
                                    HAVE THE RIGHT TO SUE IN COURT AND HAVE A JURY TRIAL. The arbitration shall be
                                    commenced and
                                    conducted under the Commercial Arbitration Rules of the American Arbitration
                                    Association ("AAA")
                                    and, where appropriate, the AAA's Supplementary Procedures for Consumer Related
                                    Disputes ("AAA
                                    Consumer Rules"), both of which are available at the <a
                                        href="http://www.adr.org"
                                        rel="nofollow">AAA
                                        website</a>. The
                                    determination of whether a Dispute is subject to arbitration shall be governed
                                    by
                                    the Federal
                                    Arbitration Act and determined by a court rather than an arbitrator. Your
                                    arbitration fees and
                                    your share of arbitrator compensation shall be governed by the AAA Rules and,
                                    where
                                    appropriate,
                                    limited by the AAA Consumer Rules. If such costs are determined by the
                                    arbitrator to
                                    be
                                    excessive, the Company will pay all arbitration fees and expenses. The
                                    arbitration
                                    may be
                                    conducted in person, through the submission of documents, by phone or online.
                                    The
                                    arbitrator
                                    will make a decision in writing, but need not provide a statement of reasons
                                    unless
                                    requested by
                                    a party. The arbitrator must follow applicable law, and any award may be
                                    challenged
                                    if the
                                    arbitrator fails to do so. Except as otherwise provided in this agreement, you
                                    and
                                    the Company
                                    may litigate in court to compel arbitration, stay proceedings pending
                                    arbitration,
                                    or to
                                    confirm, modify, vacate or enter judgment on the award entered by the
                                    arbitrator.
                                    Judgment upon any award rendered by the
                                    arbitrator(s) may be entered and enforcement obtained thereon in any court
                                    having jurisdiction. All arbitration proceedings shall be closed to the public
                                    and confidential and all records relating thereto shall be permanently sealed,
                                    except as necessary to obtain court confirmation of the arbitration award.
                                    Subject to Section IX of this Agreement
                                    (<a href="/terms#disclaimers-limitations-waivers-of-liability"
                                        class="def-anchor">Disclaimers;
                                        Limitations; Waivers Of Liability</a>), the arbitrator shall have
                                    authority to grant any form of appropriate relief, whether legal or equitable in
                                    nature, including specific performance. You and we agree to abide
                                    by all decisions and awards rendered in such proceedings. Such decisions and
                                    awards rendered by the arbitrator shall be final and conclusive. All such
                                    controversies, claims or disputes shall be settled in this manner in lieu of any
                                    action at law or equity.
                                </li>
                                <li>
                                    <em>Restrictions/No Class Actions.</em>
                                    You and the Company agree that any arbitration shall be limited to the Dispute
                                    between the
                                    Company and you individually. To the full extent permitted by law, (1) no
                                    arbitration shall be
                                    joined with any other; (2) there is no right or authority for any Dispute to be
                                    arbitrated on a
                                    class-action basis or to utilize class action procedures; and (3) there is no
                                    right
                                    or authority
                                    for any Dispute to be brought in a purported representative capacity on behalf
                                    of
                                    the general
                                    public or any other persons
                                </li>
                                <li>
                                    <em>Exclusive Process.</em>
                                    You acknowledge that the arbitrator(s), and not any
                                    federal, state or local court or agency, shall have exclusive authority to
                                    resolve
                                    any dispute arising under or relating to the interpretation, applicability,
                                    enforceability or formation of this Agreement, including but not limited to any
                                    claim that all or any part of this Agreement is void or voidable. Without
                                    limiting the generality of the foregoing, the arbitrator shall have the
                                    exclusive
                                    authority to interpret the scope of this clause, and the arbitrability of the
                                    controversy, claim or dispute.
                                </li>
                                <li>
                                    <em>Exceptions to Informal Negotiations and Arbitration.</em>
                                    You and the Company agree that the following Disputes are not subject to the
                                    above
                                    provisions
                                    concerning informal negotiations and binding arbitration: (1) any Disputes
                                    seeking
                                    to enforce or
                                    protect, or concerning the validity of, any of your or the Company's
                                    intellectual
                                    property
                                    rights; (2) any Dispute related to, or arising from, allegations of theft,
                                    piracy,
                                    invasion of
                                    privacy or unauthorized use; and (3) any claim for injunctive relief.
                                </li>
                                <li>
                                    To the extent non-U.S. laws mandate a different approach with respect to
                                    governing
                                    law, venue,
                                    statute of limitation, and dispute resolution method with respect to certain
                                    non-U.S. persons,
                                    each such required standard shall be applied, but all other provisions under
                                    this
                                    section shall
                                    remain in full force.
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong>Waiver/Severability</strong>
                            <ol class="lower-roman">
                                <li>
                                    The failure of the Company to require or enforce strict performance by you of
                                    any
                                    provision of
                                    this agreement or to exercise any right under this agreement shall not be
                                    construed
                                    as a waiver
                                    or relinquishment of the Company's right to assert or rely upon any such
                                    provision
                                    or right in
                                    that or any other instance. In fact, the Company may choose to enforce certain
                                    portions of this
                                    agreement more strictly or to interpret certain provisions more strictly against
                                    certain users
                                    than it does against users in general, and such disparate treatment shall not be
                                    grounds for
                                    failing to comply with all this agreement as so interpreted.
                                </li>
                                <li>
                                    You and the Company agree that if any portion of this agreement, except any
                                    portion
                                    of Section
                                    XII(b)(vi), is found illegal or unenforceable, in whole or in part by any court
                                    of
                                    competent
                                    jurisdiction, such provision shall, as to such jurisdiction, be ineffective to
                                    the
                                    extent of
                                    such determination of invalidity or unenforceability without affecting the
                                    validity
                                    or
                                    enforceability thereof in any other manner or jurisdiction and without affecting
                                    the
                                    remaining
                                    provisions of the Terms, which shall continue to be in full force and effect. If
                                    Section
                                    XII(b)(vi) is found to be illegal or unenforceable then neither you nor the
                                    Company
                                    will elect
                                    to arbitrate any Dispute falling within that portion of XI(b)(vi) found to be
                                    illegal or
                                    unenforceable and such Dispute shall be decided by a court of competent
                                    jurisdiction
                                    within the
                                    County of San Francisco, State of California, United States of America, and you
                                    and
                                    the Company
                                    agree to submit to the personal jurisdiction of that court.
                                </li>
                                <li>
                                    The Company operates and controls the Service from its offices in the United
                                    States.
                                    The Company
                                    makes no representation that the Service is appropriate or available in other
                                    locations. The
                                    information provided on or through the Service is not intended for distribution
                                    to
                                    or use by any
                                    person or entity in any jurisdiction or country where such distribution or use
                                    would
                                    be contrary
                                    to law or regulation or which would subject the Company to any registration
                                    requirement within
                                    such jurisdiction or country. Accordingly, those persons who choose to access
                                    the
                                    Service from
                                    other locations do so on their own initiative and are solely responsible for
                                    compliance with
                                    local laws, if and to the extent local laws are applicable.
                                </li>
                                <li>
                                    Certain information provided by Entrepreneurs or Investors may be subject to
                                    United
                                    States
                                    export controls. Thus, no such materials may be downloaded, exported or
                                    re-exported
                                    (i) into (or
                                    to a national or resident of) Cuba, Iraq, North Korea, Iran, Syria, or any other
                                    country to
                                    which the United States has embargoed goods; or (ii) to anyone on the U.S.
                                    Treasury
                                    Department's
                                    list of Specially Designated Nationals or the U.S. Commerce Department's Table
                                    of
                                    Deny Orders.
                                    By downloading any material available through the Service, you represent and
                                    warrant
                                    that you
                                    are not located in, under the control of, or a national or resident of, any such
                                    country or on
                                    any such list. The parties specifically disclaim application of the Convention
                                    on
                                    Contracts for
                                    the International Sale of Goods.
                                </li>
                                <li>
                                    Neither the course of conduct between the parties nor trade practice will act to
                                    modify this
                                    agreement to any party at any time without any notice to you. You may not assign
                                    this agreement
                                    without the Company's prior written consent. This agreement contains the entire
                                    understanding of
                                    you and the Company, and supersedes all prior understandings of the parties
                                    hereto
                                    relating to
                                    the subject matter hereof, and cannot be changed or modified by you except as
                                    posted
                                    on the
                                    Service by the Company. No waiver by either party of any breach or default
                                    hereunder
                                    shall be
                                    deemed to be a waiver of any preceding or subsequent breach or default.
                                </li>
                                <li>
                                    The section headings used herein and the summary of terms at the beginning of
                                    this
                                    agreement are
                                    for convenience only and shall not be given any legal import. Upon the Company's
                                    request, you
                                    will furnish the Company any documentation, substantiation or releases necessary
                                    to
                                    verify your
                                    compliance with this agreement. You agree that this agreement will not be
                                    construed
                                    against the
                                    Company by virtue of having drafted them. You hereby waive any and all defenses
                                    you
                                    may have
                                    based on the electronic form of this agreement and the lack of signing by the
                                    parties hereto to
                                    execute this agreement.
                                </li>
                            </ol>
                        </li>
                        <li>
                            <strong>Statute of Limitations.</strong>
                            You and the Company both agree that regardless of any statute or law to the contrary but
                            only to the
                            extent permissible by law in each relevant jurisdiction, any claim or cause of action
                            arising out of or
                            related to use of the Service, this agreement or our Privacy Policy must be filed within
                            ONE
                            (1) YEAR
                            after such claim or cause of action arose or be forever barred.
                        </li>
                        <li>
                            <strong>No Third Party Beneficiaries.</strong>
                            This agreement is between you and the Company. No user has any rights to force the
                            Company
                            to enforce
                            any rights it may have against any you or any other user, except to the extent that
                            Entrepreneurs may
                            enforce their own intellectual property rights related to Content offered through the
                            Service.
                        </li>
                        <li>
                            <strong>Government Use.</strong>
                            If You are a part of an agency, department, or other entity of the United States
                            Government
                            ("Government"), the use, duplication, reproduction, release, modification, disclosure or
                            transfer of the
                            any of our products or Services is restricted in accordance with the Federal Acquisition
                            Regulations as
                            applied to civilian agencies and the Defense Federal Acquisition Regulation Supplement
                            as
                            applied to
                            military agencies. The Service and any related software is a "commercial item,"
                            "commercial
                            computer
                            software" and "commercial computer software documentation". In accordance with such
                            provisions, any use
                            of the Service by the Government shall be governed solely by this agreement.
                        </li>
                    </ol>
                </li>
                <li>
                    <a class="definition agreement-section-title" name="definitions">definitions</a>
                    <ol class="alpha">
                        <li>
                            <a class="definition strong" name="accredited-investor">Accredited Investor</a> is
                            defined
                            by the SEC in
                            rule 501 as follows:
                            <ol class="lower-roman">
                                <li>
                                    a bank, insurance company, registered investment company, business development
                                    company, or small
                                    business investment company;
                                </li>
                                <li>
                                    an employee benefit plan, within the meaning of the Employee Retirement Income
                                    Security Act, if
                                    a bank, insurance company, or registered investment adviser makes the investment
                                    decisions, or
                                    if the plan has total assets in excess of $5 million;
                                </li>
                                <li>
                                    a charitable organization, corporation, or partnership with assets exceeding $5
                                    million;
                                </li>
                                <li>
                                    a director, executive officer, or general partner of the company selling the
                                    securities;
                                </li>
                                <li>
                                    a business in which all the equity owners are accredited investors;
                                </li>
                                <li>
                                    a natural person who has individual net worth, or joint net worth with the
                                    person's
                                    spouse, that
                                    exceeds $1 million at the time of the purchase;
                                </li>
                                <li>
                                    a natural person with income exceeding $200,000 in each of the two most recent
                                    years
                                    or joint
                                    income with a spouse exceeding $300,000 for those years and a reasonable
                                    expectation
                                    of the same
                                    income level in the current year; or
                                </li>
                                <li>
                                    a trust with assets in excess of $5 million, not formed to acquire the
                                    securities
                                    offered, whose
                                    purchases a sophisticated person makes.
                                </li>
                            </ol>
                            <p>Users accessing the site from outside of the U.S. may also need to meet other
                                sophistication standard
                                so required by laws in that jurisdiction. Specifically, references to "Accredited
                                Investors"
                                accessing this site from the United Kingdom are those persons who have been
                                certified as
                                a High Net
                                Worth Individual or Self Certified Sophisticated Investor in accordance with the UK
                                Financial
                                Services and Markets Act 2000 (Financial Promotion) Order 2005.</p>
                        </li>
                        <li>
                            <a class="definition strong" name="allowable-user-identity">Allowable User Identity</a>:
                            <ol class="lower-roman">
                                <li>
                                    An Allowable User Identity is one consisting of an email address you own and
                                    use,
                                    and a
                                    password. You shall be solely responsible for maintaining the confidentiality of
                                    your password.
                                    You will also update your registration information with the Company from time to
                                    time so that it
                                    remains true, correct and complete. Even if you are accessing the Service
                                    without
                                    creating an
                                    account, you are still agreeing to all of this agreement.
                                </li>
                                <li>
                                    An Allowable User Identity excludes:
                                    <ol class="decimal">
                                        <li>
                                            Any use of a false name or an email address owned or controlled by
                                            another
                                            person with
                                            the intent to impersonate that person or for any other reason;
                                        </li>
                                        <li>
                                            Any use as a User ID a name subject to any rights of a person other than
                                            yourself
                                            without appropriate authorization; or
                                        </li>
                                        <li>
                                            Any other submission of false or misleading information to the Company.
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <a class="definition strong" name="community-rules">Community Rules</a>: Participants in
                            the
                            GrowMe
                            community will not:
                            <ol class="lower-roman">
                                <li>
                                    include any offensive comments that are connected to race, national origin,
                                    gender,
                                    sexual
                                    preference or physical handicap;
                                </li>
                                <li>
                                    include hateful, profane, obscene, indecent, pornographic, sexual or otherwise
                                    objectionable
                                    content or language;
                                </li>
                                <li>
                                    defame, libel, ridicule, mock, disparage, threaten, harass, intimidate or abuse
                                    anyone;
                                </li>
                                <li>
                                    promote violence, illegal drug use, or substance abuse or describe how to
                                    perform a
                                    violent act,
                                    use illegal drugs or abuse other substances;
                                </li>
                                <li>
                                    violate the contractual, personal, intellectual property or other rights of any
                                    person, or
                                    promote or constitute illegal activity;
                                </li>
                                <li>
                                    be in violation of this agreement, any local, state, federal or non-US law, rule
                                    or
                                    regulation
                                    or the rules of conduct posted with respect to any individual feature of the
                                    Services;
                                </li>
                                <li>
                                    reveal any personal information about another individual, including another
                                    person's
                                    address,
                                    phone number, e-mail address, credit card number or any information that may be
                                    used
                                    to track,
                                    contact or impersonate that individual;
                                </li>
                                <li>
                                    attempt to impersonate any other party;
                                </li>
                                <li>
                                    create user accounts by automated means or under false pretenses or mislead
                                    others
                                    as to the
                                    origins of your communications;
                                </li>
                                <li>
                                    trick, defraud, deceive or mislead the Company or other users, especially in any
                                    attempt to
                                    learn sensitive information;
                                </li>
                                <li>
                                    make improper use of the Company's support services or submit false reports of
                                    abuse
                                    or
                                    misconduct;
                                </li>
                                <li>
                                    create or transmit unwanted electronic communications such as "spam," to other
                                    users
                                    or members
                                    of Service or otherwise interfere with other users' or members' enjoyment of the
                                    Service;
                                </li>
                                <li>
                                    disparage, tarnish, or otherwise harm, in the Company's opinion, the Company
                                    and/or
                                    the Service;
                                </li>
                                <li>
                                    disseminate or transmit viruses, worms, Trojan horses, RATs, keyboard loggers,
                                    time
                                    bombs,
                                    spyware, adware, cancelbots, passive collection mechanisms ("PCMs"), or any
                                    other
                                    malicious or
                                    invasive code or program or upload or transmit (or attempt to upload or to
                                    transmit)
                                    any
                                    material that acts as a passive or active information collection or transmission
                                    mechanism,
                                    including, without limitation, clear graphics interchange formats ("gifs"), 1x1
                                    pixels, web
                                    bugs, cookies or other similar devices;
                                </li>
                                <li>
                                    copy or adapt the Service's software including but not limited to Flash, PHP,
                                    HTML,
                                    JavaScript
                                    or other code;
                                </li>
                                <li>
                                    reverse engineer, decompile, reverse assemble, modify or attempt to discover any
                                    software
                                    (source code or object code) that the Service creates to generate web pages or
                                    any
                                    software or
                                    other products or processes accessible through the Service;
                                </li>
                                <li>
                                    except as may be the result of standard search engine or Internet browser usage,
                                    use
                                    or launch,
                                    develop or distribute any automated system, including, without limitation, any
                                    spider, robot (or
                                    "bot"), cheat utility, scraper or offline reader that accesses the Service, or
                                    use
                                    or launch any
                                    unauthorized script or other software;
                                </li>
                                <li>
                                    access or search or attempt to access or search the Service by any means
                                    (automated
                                    or
                                    otherwise) other than through the currently available, published interfaces that
                                    are
                                    provided by
                                    GrowMe (and only pursuant to those terms and conditions), unless you have been
                                    specifically
                                    allowed to do so in a separate agreement with GrowMe (crawling the Service is
                                    permissible in
                                    accordance with this agreement, but scraping the Service without the prior
                                    consent
                                    of GrowMe
                                    except as permitted by this agreement is expressly prohibited);
                                </li>
                                <li>
                                    forge any TCP/IP packet header or any part of the header information in any
                                    email or
                                    posting, or
                                    in any way use the Service to send altered, deceptive or false
                                    source-identifying
                                    information;
                                </li>
                                <li>
                                    cover or obscure any notice, banner, advertisement or other branding on the
                                    Service;
                                </li>
                                <li>
                                    disguise the source of materials or other information you submit to the Service
                                    or
                                    use tools
                                    which anonymize your internet protocol address (e.g., anonymous proxy) to access
                                    the
                                    Service;
                                </li>
                                <li>
                                    interfere with or circumvent any security feature of the Service or any feature
                                    that
                                    restricts
                                    or enforces limitations on use of or access to the Service, Content or the User
                                    Content;
                                </li>
                                <li>
                                    probe, scan, or test the vulnerability of any system or network or breach or
                                    circumvent any
                                    security or authentication measures;
                                </li>
                                <li>
                                    sell access to the Service or any part thereof other than through a mechanism
                                    approved by the
                                    Company; or
                                </li>
                                <li>
                                    interfere with or disrupt (or attempt to do so) the access of any user, host or
                                    network,
                                    including, without limitation, sending a virus, overloading, flooding, spamming,
                                    mail-bombing
                                    the Service, or by scripting the creation of Content in such a manner as to
                                    interfere with or
                                    create an undue burden on the Service.
                                </li>
                            </ol>
                        </li>
                        <li>
                            <a class="definition strong" name="locked-information">Locked Information</a> means, all
                            information
                            acquired by, through or in connection with your use of the Service or the GrowMe website
                            that was
                            provided by another person and which is identified as "Locked" in any manner reasonably
                            designed to
                            identify the character of such information.
                        </li>
                        <li>
                            <a class="definition strong" name="content">Content</a> means any information, text,
                            graphics, or other
                            materials uploaded, downloaded or appearing on the Service. You retain ownership of all
                            Content you
                            submit, post, display, or otherwise make available on the Service.
                        </li>
                        <li>
                            <a class="definition strong" name="inappropriate-uses">Inappropriate Uses of the
                                Systems</a>:
                            GrowMe
                            may at any time designate one or more activities as inappropriate uses of the system in
                            its
                            sole
                            discretion. Without limiting the foregoing, the following uses will be deemed prohibited
                            "Inappropriate
                            Uses of the System":
                            <ol class="lower-roman">
                                <li>
                                    The modification, adaptation, disassembly, decompilation, translation, reverse
                                    engineering or
                                    other attempt to discover the source code or structure, sequence and
                                    organization of
                                    the Service
                                    or any portion of any website on which the Service is offered (except where the
                                    foregoing is
                                    required by applicable local law, and then only to the extent so required under
                                    such
                                    laws);
                                </li>
                                <li>
                                    The use of the Service in any manner that could damage, disable, overburden, or
                                    impair the
                                    Service or another user's use of the Service;
                                </li>
                                <li>
                                    The removal, obscuring or changing of any copyright, trademark, hyperlink or
                                    other
                                    proprietary
                                    rights notices contained in or on the Service or any website on which the
                                    Service is
                                    offered,
                                    Company code embeddable or embedded on a third party web site and/or Company
                                    software;
                                </li>
                                <li>
                                    The submission of any content or material that falsely expresses or implies that
                                    such content or
                                    material is sponsored or endorsed by the Company;
                                </li>
                                <li>
                                    The use of the Services to violate the security of any computer network or
                                    transfer
                                    or store
                                    illegal material; or
                                </li>
                                <li>
                                    The use of the Service or the delivery of any Content in violation of any
                                    applicable
                                    law
                                    (including intellectual property laws, right of privacy or publicity laws and
                                    any
                                    laws of a
                                    non-US jurisdiction applicable to you).
                                </li>
                            </ol>
                        </li>
                        <li>
                            <a class="definition strong" name="qualified-purchaser">Qualified Purchaser</a>: The
                            definition of
                            "qualified purchaser" is found in the Investment Company Act of 1940. The definition
                            includes:
                            <ol class="lower-roman">
                                <li>
                                    any natural person (including any person who holds a joint, community property,
                                    or
                                    other similar
                                    shared ownership interest in an issuer that is excepted under section 3(c)(7)
                                    [of
                                    the Investment
                                    Company Act of 1940] with that person's qualified purchaser spouse) who owns not
                                    less than $
                                    5,000,000 in investments, as defined below;
                                </li>
                                <li>
                                    any company that owns not less than $ 5,000,000 in investments and that is owned
                                    directly or
                                    indirectly by or for 2 or more natural persons who are related as siblings or
                                    spouse
                                    (including
                                    former spouses), or direct lineal descendants by birth or adoption, spouses of
                                    such
                                    persons, the
                                    estates of such persons, or foundations, charitable organizations, or trusts
                                    established by or
                                    for the benefit of such persons;
                                </li>
                                <li>
                                    any trust that is not covered by clause (ii) and that was not formed for the
                                    specific purpose of
                                    acquiring the securities offered, as to which the trustee or other person
                                    authorized
                                    to make
                                    decisions with respect to the trust, and each settlor or other person who has
                                    contributed assets
                                    to the trust, is a person described in clause (i), (ii), or (iv); or
                                </li>
                                <li>
                                    any person, acting for its own account or the accounts of other qualified
                                    purchasers, who in the
                                    aggregate owns and invests on a discretionary basis, not less than $ 25,000,000
                                    in
                                    investments.
                                </li>
                                <li>
                                    any qualified institutional buyer as defined in Rule 144A under the Securities
                                    Act,
                                    acting for
                                    its own account, the account of another qualified institutional buyer, or the
                                    account of a
                                    qualified purchaser, provided that (1) a dealer described in paragraph
                                    (a)(1)(ii) of
                                    Rule 144A
                                    shall own and invest on a discretionary basis at least $25,000,000 in securities
                                    of
                                    issuers that
                                    are not affiliated persons of the dealer; and (2) a plan referred to in
                                    paragraph
                                    (a)(1)(D) or
                                    (a)(1)(E) of Rule 144A, or a trust fund referred to in paragraph (a)(1)(F) of
                                    Rule
                                    144A that
                                    holds the assets of such a plan, will not be deemed to be acting for its own
                                    account
                                    if
                                    investment decisions with respect to the plan are made by the beneficiaries of
                                    the
                                    plan, except
                                    with respect to investment decisions made solely by the fiduciary, trustee or
                                    sponsor of such
                                    plan;
                                </li>
                                <li>
                                    any company that, but for the exceptions provided for in Sections 3(c)(1) or
                                    3(c)(7)
                                    under the
                                    ICA, would be an investment company (hereafter in this paragraph referred to as
                                    an
                                    "excepted
                                    investment company"), provided that all beneficial owners of its outstanding
                                    securities (other
                                    than short-term paper), determined in accordance with Section 3(c)(1)(A)
                                    thereunder,
                                    that
                                    acquired such securities on or before April 30, 1996 (hereafter in this
                                    paragraph
                                    referred to as
                                    "pre-amendment beneficial owners"), and all pre-amendment beneficial owners of
                                    the
                                    outstanding
                                    securities (other than short-term paper) or any excepted investment company
                                    that,
                                    directly or
                                    indirectly, owns any outstanding securities of such excepted investment company,
                                    have consented
                                    to its treatment as a qualified purchaser.
                                </li>
                                <li>
                                    any natural person who is deemed to be a "knowledgeable employee" of the [fund],
                                    as
                                    such term is
                                    defined in Rule 3c-5(4) of the ICA; or
                                </li>
                                <li>
                                    any person ("Transferee") who acquires Interests from a person ("Transferor")
                                    that
                                    is (or was) a
                                    qualified purchaser other than the [fund], provided that the Transferee is: (i)
                                    the
                                    estate of
                                    the Transferor; (ii) a person who acquires the Interests as a gift or bequest
                                    pursuant to an
                                    agreement relating to a legal separation or divorce; or (iii) a company
                                    established
                                    by the
                                    Transferor exclusively for the benefit of (or owned exclusively by) the
                                    Transferor
                                    and the
                                    persons specified in this paragraph.
                                </li>
                                <li>
                                    any company, if each beneficial owner of the company's securities is a qualified
                                    purchaser.
                                </li>
                            </ol>
                            <p>For the purposes of above, the term Investments means:</p>
                            <ol class="lower-roman">
                                <li>
                                    securities (as defined by section 2(a)(1)of the Securities Act of 1933), other
                                    than
                                    securities
                                    of an issuer that controls, is controlled by, or is under common control with,
                                    the
                                    prospective
                                    qualified purchaser that owns such securities, unless the issuer of such
                                    securities
                                    is: (i) an
                                    investment vehicle; (ii) a public company; or (iii) a company with shareholders'
                                    equity of not
                                    less than $50 million (determined in accordance with generally accepted
                                    accounting
                                    principles)
                                    as reflected on the company's most recent financial statements, provided that
                                    such
                                    financial
                                    statements present the information as of a date within 16 months preceding the
                                    date
                                    on which the
                                    prospective qualified purchaser acquires the securities of a Section 3(c)(7)
                                    Company;
                                </li>
                                <li>
                                    real estate held for investment purposes;
                                </li>
                                <li>
                                    commodity interests held for investment purposes;
                                </li>
                                <li>
                                    physical commodities held for investment purposes;
                                </li>
                                <li>
                                    to the extent not securities, financial contracts (as such term is defined in
                                    section
                                    3(c)(2)(B)(ii) of the ICA entered into for investment purposes;
                                </li>
                                <li>
                                    in the case of a prospective qualified purchaser that is a Section 3(c)(7)
                                    Company,
                                    a company
                                    that would be an investment company but for the exclusion provided by section
                                    3(c)(1) of the
                                    ICA, or a commodity pool, any amounts payable to such prospective qualified
                                    purchaser pursuant
                                    to a firm agreement or similar binding commitment pursuant to which a person has
                                    agreed to
                                    acquire an interest in, or make capital contributions to, the prospective
                                    qualified
                                    purchaser
                                    upon the demand of the prospective qualified purchaser; and
                                </li>
                                <li>
                                    cash and cash equivalents (including foreign currencies) held for investment
                                    purposes. For
                                    purposes of this section, cash and cash equivalents include: (i) bank deposits,
                                    certificates of
                                    deposit, bankers' acceptances and similar bank instruments held for investment
                                    purposes; and
                                    (ii) the net cash surrender value of an insurance policy.
                                </li>
                            </ol>
                        </li>
                    </ol>
                </li>
            </ol>
            <hr>
            <div class="tos-footer">
                <p>
                    The Company is an entity offering the transmission, routing, or providing of connections for
                    digital
                    online
                    communications, between or among points specified by a user of material of the user's choosing,
                    without
                    modification of the content of the material sent or received ("transitory digital network
                    communications"), as
                    well as system caching, storage of material residing on a system or network at the direction of
                    a
                    user, and
                    referral or linkage of users to an online location using information location tools, each
                    through
                    the website
                    located at
                    <a href="/">https://grow-me.co</a>
                    and any linked pages or applications owned and operated by the Company.
                </p>

                <p>
                    If you have questions about this agreement, please
                    <a href="mailto:<?= Yii::$app->params['adminEmail'] ?>?subject=GrowMe%20Terms%20of%20Service">contact
                        us</a>.
                    GrowMe Limited may conduct certain regulated activities in the United Kingdom as an appointed
                    representative
                    and/or supervised agent of a licensed third party arranger/manager/adviser.
                </p>

            </div>
        </div>
    </div>
</div>
