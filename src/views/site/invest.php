<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

?>
<div class="wrapper-footer">
    <div class="content invest-footer">
    <div class="container">
        <div class="post-list">
            <div class="post-head">
                <p>post list</p>
            </div>
            <div class="post-menu">
                <div class="post-image">
                    <img src="/img/invest/image-1.png">
                    <div class="bottom-fon">
                        <p>tips & tricks</p>
                    </div>
                    <!--<img src="/img/invest/bottom-img.png">-->
                </div>
                <div class="post-content">
                    <p class="title">
                        kif gets knocked up a notch
                    </p>
                    <p >
                        I quess if you want children beaten, you have to do it yourself. Oh Leela!
                        You`re the only person i could turn to; you`re the only person who ever loved me.
                        I guess if you want children beaten, you have to do in yourself.
                    </p>
                    <div class="slider-bottom">
                        <div class="img-icon">
                            <label>
                                <a href="#"><img src="/img/slider/HeartIcon.png"></a>
                                46
                            </label>
                        </div>
                        <div class="img-icon">
                            <label>
                                <a href="#"><img src="/img/slider/BubbleIcon.png"></a>
                                19
                            </label>
                        </div>
                        <p>12 hours ago</p>
                    </div>
                </div>
            </div>
            <div class="post-menu">
                <div class="post-image">
                    <img src="/img/invest/image-2.png">
                    <div class="bottom-fon">
                        <p>tips & tricks</p>
                    </div>
                </div>
                <div class="post-content">
                    <p class="title">
                        kif gets knocked up a notch
                    </p>
                    <p >
                        I quess if you want children beaten, you have to do it yourself. Oh Leela!
                        You`re the only person i could turn to; you`re the only person who ever loved me.
                        I guess if you want children beaten, you have to do in yourself.
                    </p>
                    <div class="slider-bottom">
                        <div class="img-icon">
                            <label>
                                <a href="#"><img src="/img/slider/HeartIcon.png"></a>
                                46
                            </label>
                        </div>
                        <div class="img-icon">
                            <label>
                                <a href="#"><img src="/img/slider/BubbleIcon.png"></a>
                                19
                            </label>
                        </div>
                        <p>12 hours ago</p>
                    </div>
                </div>
            </div>
            <div class="post-menu">
                <div class="post-image">
                    <img src="/img/invest/image-3.png">
                    <div class="bottom-fon">
                        <p>tips & tricks</p>
                    </div>
                </div>
                <div class="post-content">
                    <p class="title">
                        kif gets knocked up a notch
                    </p>
                    <p >
                        I quess if you want children beaten, you have to do it yourself. Oh Leela!
                        You`re the only person i could turn to; you`re the only person who ever loved me.
                        I guess if you want children beaten, you have to do in yourself.
                    </p>
                    <div class="slider-bottom">
                        <div class="img-icon">
                            <label>
                                <a href="#"><img src="/img/slider/HeartIcon.png"></a>
                                46
                            </label>
                        </div>
                        <div class="img-icon">
                            <label>
                                <a href="#"><img src="/img/slider/BubbleIcon.png"></a>
                                19
                            </label>
                        </div>
                        <p>12 hours ago</p>
                    </div>
                </div>
            </div>
            <div class="btn-div">
                <a href="#">
                    <button class="btn-more">
                        <img src="/img/invest/circle-arrow.png">
                        <span>need more posts</span>
                    </button>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="post-footer">
        <div class="button">
            <a href="#">Invest</a>
        </div>
    </div>
    <div class="clearfix">

    </div>
</div>
</div>