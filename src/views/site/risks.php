<?php
/** @var $this \yii\web\View */
$this->title = Yii::t('app', 'Risks')
?>
<div class="wrapper-footer">
    <div class="container">
        <h1>
            <?= $this->title ?>
        </h1>
        <p><em>Every investor ("<strong>Investor</strong>") should be aware that an investment in a single company or
                multiple companies on the AngelList platform (each, a "Startup") <strong>involves a high degree of
                    risk</strong>, regardless of whether such investment is direct or through a vehicle
                ("<strong>Fund</strong>") formed for purposes of accommodating co-investment arrangement ("<strong>Syndicate</strong>")
                with a lead investor ("<strong>Lead Angel</strong>"). There can be no assurance that (i) a Fund's
                investment objectives will be achieved, (ii) a Startup will achieve its business plan, (iii) a Lead
                Angel has experience in investing, or (iv) an Investor will receive a return of any part of its
                investment. In addition, there may be occasions when AngelList Advisors, LLC ("<strong>Advisor</strong>"),
                a Lead Angel, and/or their respective affiliates may encounter potential conflicts of interest in
                connection with a Fund, such that said party may avoid a loss, or even realize a gain, when other
                Investors in the Fund are suffering losses. The following considerations, among others, should be
                carefully evaluated before making an investment in a Startup or a Fund.</em>

        </p><h6>Risk Inherent in Startup Investments; an Investor May, and Frequently Does, Lose All of Its
            Investment</h6>

        <p>Investments in Startups involve a high degree of risk. Financial and operating risks confronting Startups are
            significant. While targeted returns should reflect the perceived level of risk in any investment situation,
            such returns may never be realized and/or may not be adequate to compensate an Investor or a Fund for risks
            taken. Loss of an Investor's entire investment is possible and can easily occur. Moreover, the timing of any
            return on investment is highly uncertain.
        </p>

        <p>The Startup market is highly competitive and the percentage of companies that survive and prosper is small.
            Startup investments often experience unexpected problems in the areas of product development, manufacturing,
            marketing, financing, and general management, among others, which frequently cannot be solved. In addition,
            Startups may require substantial amounts of financing, which may not be available through institutional
            private placements, the public markets or otherwise.

        </p><h6>Investment in Technologies</h6>

        <p>The value of an Investor's or a Fund's interests in Startups may be susceptible to factors affecting the
            technology industry and/or to greater risk than an investment in a vehicle that invests in a broader range
            of securities. Some of the many specific risks faced by such Startups include:
        </p>
        <ul style="list-style-type:disc;padding-left:2em;margin:1em 0">
            <li> Rapidly changing technologies;
            </li>
            <li> Products or technologies that may quickly become obsolete;
            </li>
            <li> Scarcity of management, technical, scientific, research and marketing personnel with appropriate
                training;
            </li>
            <li> The possibility of lawsuits related to patents and intellectual property;
            </li>
            <li> Rapidly changing investor sentiments and preferences with regard to technology sector investments
                (which are generally perceived as risky); and
            </li>
            <li> Exposure to government regulation, making these companies susceptible to changes in government policy
                and delays or failures in securing regulatory approvals.
            </li>
        </ul>

        <h6>Changing Economic Conditions</h6>

        <p>The success of any investment activity is determined to some degree by general economic conditions. The
            availability, unavailability, or hindered operation of external credit markets, equity markets and other
            economic systems which an individual Startup or a Fund may depend upon to achieve its objectives may have a
            significant negative impact on a Startup's or a Fund's operations and profitability. The stability and
            sustainability of growth in global economies may be impacted by terrorism, acts of war or a variety of other
            unpredictable events. There can be no assurance that such markets and economic systems will be available or
            will be available as anticipated or needed for an investment in a Startup to be successful or for a Fund to
            operate successfully. Changing economic conditions could potentially, and frequently do, adversely impact
            the valuation of portfolio holdings.

        </p><h6>Future and Past Performance</h6>

        <p>The past performance of a Startup or its management, a Lead Angel, or principals of Advisor, is not
            predictive of a Startup's or a Fund's future results. There can be no assurance that targeted results will
            be achieved. Loss of principal is possible, and even likely, on any given investment.

        </p><h6>Difficulty in Valuing Startup Investments</h6>

        <p>It is enormously difficult to determine objective values for any Startup. In addition to the difficulty of
            determining the magnitude of the risks applicable to a given Startup and the likelihood that a given
            Startup's business will be a success, there generally will be no readily available market for a Startup's
            equity securities, and hence, an Investor's investments will be difficult to value.

        </p><h6>Minority Investments</h6>

        <p>A significant portion of an Investor's investments (either directly or through Funds) will represent minority
            stakes in privately held companies. As is the case with minority holdings in general, such minority stakes
            will have neither the control characteristics of majority stakes nor the valuation premiums accorded
            majority or controlling stakes. Investors and Funds will be reliant on the existing management and board of
            directors of such companies, which may include representatives of other financial investors with whom the
            Investor or Fund is not affiliated and whose interests may conflict with the interests of the Investor or
            Fund.

        </p><h6>Lack of Information for Monitoring and Valuing Startups</h6>

        <p>The Investor or the Fund may not be able to obtain all information it would want regarding a particular
            Startup, on a timely basis or at all. It is possible that the Investor or the Fund may not be aware on a
            timely basis of material adverse changes that have occurred with respect to certain of its investments. As a
            result of these difficulties, as well as other uncertainties, an Investor may not have accurate information
            about a Startup's current value or the value of the securities held by a Fund.

        </p><h6>No Assurance of Additional Capital for Startups</h6>

        <p>After an Investor has invested in a Startup, either directly or through a Fund, continued development and
            marketing of the Startup's products or services, or administrative, legal, regulatory or other needs, may
            require that it obtain additional financing. In particular, technology Startups generally have substantial
            capital needs that are typically funded over several stages of investment. Such additional financing may not
            be available on favorable terms, or at all.

        </p><h6>Absence of Liquidity and Public Markets</h6>

        <p>An Investor's investments will generally be private, illiquid holdings. As such, there will be no public
            markets for the securities held by the Investor, directly or through a Fund, and no readily available
            liquidity mechanism at any particular time for any of the investments. In addition, an investment in a Fund
            will be illiquid, not freely transferrable, and involves a high degree of risk. There is no public market
            for membership interests in a Fund (an "Interest"), and it is not expected that a public market will
            develop. Consequently, an Investor will bear the economic risks of its investment for the term of a Fund.

        </p><h6>Legal and Regulatory Risks Associated with Funds</h6>

        <p>No Fund is, nor expects to be, registered as an "investment company" under the United States Investment
            Company Act of 1940, as amended (the "<strong>Investment Company Act</strong>"), pursuant to an exemption
            set forth in Sections 3(c)(1) and/or 3(c)(7) of the Investment Company Act. There is no assurance that such
            exemptions will continue to be available to these entities.
        </p>

        <p>Neither a Fund nor its counsel can assure an Investor that, under certain conditions, changed circumstances,
            or changes in the law, the Fund may not become subject to the Investment Company Act or other burdensome
            regulation. No Fund plans to register the offering of any Interests under the United States Securities Act
            of 1933, as amended (the "<strong>Securities Act</strong>"). As a result, no Investor will be afforded the
            protections of the Securities Act with respect to its investment in the relevant Fund.
        </p><h6>Tax Risks</h6>

        <p>There are many tax risks relating to investments in Startups are difficult to address and complicated. You
            should consult your tax advisor for information about the tax consequences of purchasing equity securities
            of a Startup or an Interest in a Fund.
        </p><h6>Withholding and Other Taxes</h6>

        <p>The structure of any investment in a Startup or in or by a Fund may not be tax efficient for any particular
            Investor, and no Startup or Fund guarantees that any particular tax result will be achieved. In addition,
            tax reporting requirements may be imposed on Investors under the laws of the jurisdictions in which
            Investors are liable for taxation or in which a Fund makes investments. Investors should consult their own
            professional advisors with respect to the tax consequences to them of an investment in a Startup or a Fund
            under the laws of the jurisdictions in which the Investors and/or the Startup or Fund are liable for
            taxation.
        </p><h6>Limited Operating History of Funds</h6>

        <p>Each Fund is or will be a newly formed entity and has no operating history. Each Fund's investment program
            should be evaluated on the basis that the Advisor's, or where appropriate, a Lead Angel's assessment of the
            prospects of investments may not prove accurate and that the Fund will not achieve its investment objective.
            Past performance of a Lead Angel, the Advisor or its principals, or the management of a Startup is not
            predictive of future results.
        </p><h6>Conflicts of Interest; Investment Opportunities</h6>

        <p>Instances may arise in which the interest of a Lead Angel (or its members or affiliates) may potentially or
            actually conflict with the interests of a Fund and/or its Investors. For example, conflicts of interest may
            arise as a result of a Lead Angel having investments in portfolio companies of the relevant Fund as well as
            other investments both public and private.
        </p><h6>Diverse Investors</h6>

        <p>Investors in a Fund may have conflicting investment, tax, and other interests with respect to Startup
            investments, which may arise from the structuring of a Startup investment or the timing of a sale of a
            Startup investment or other factors. As a consequence, decisions made by the manager of a Fund on such
            matters may be more beneficial for some Investors than for others. Investors should be aware that the
            manager of a Fund intends to consider the investment and tax objective of each Fund and Investors as a whole
            when making decisions on investment structure or timing of sale, and not the circumstances of any Investor
            individually.
        </p><h6>Lack of Investor Control</h6>

        <p>Investors in a Fund will not make decisions with respect to the management, disposition or other realization
            of any investment made by the relevant Fund, or other decisions regarding such Fund's business and affairs.
        </p><h6>Confidential Information</h6>

        <p>Certain information regarding the Startups will be highly confidential. Competitors may benefit from such
            information if it is ever made public, and that could result in adverse economic consequences to the
            Investors.
        </p><h6>Forward Looking Statements</h6>

        <p>The information available to Funds and Investors may contain "forward-looking statements" within the meaning
            of the Private Securities Litigation Reform Act of 1995. These statements can be identified by the fact that
            they do not relate strictly to historical or current facts. Forward-looking statements often include words
            such as "anticipates," "estimates," "expects," "projects," "intends," "plans," "believes" and words and
            terms of similar substance in connection with discussions of future operating or financial performance.
            Examples of forward-looking statements include, but are not limited to, statements regarding: (i) the
            adequacy of a Startup's funding to meet its future needs, (ii) the revenue and expenses expected over the
            life of the Startup, (iii) the market for a Startup's goods or services, or (iv) other similar maters.

        </p>

        <p>Each Startup's forward-looking statements are based on management's current expectations and assumptions
            regarding the Startup's business and performance, the economy and other future conditions and forecasts of
            future events, circumstances and results. As with any projection or forecast, forward-looking statements are
            inherently susceptible to uncertainty and changes in circumstances. The Startup's actual results may vary
            materially from those expressed or implied in its forward-looking statements. Important factors that could
            cause the Startup's actual results to differ materially from those in its forward-looking statements include
            government regulation, economic, strategic, political and social conditions and the following factors:


        </p>
        <ul style="list-style-type:disc;padding-left:2em;margin:1em 0">

            <li>recent and future changes in technology, services and standards;</li>
            <li>changes in consumer behavior;</li>
            <li>changes in a Startup's plans, initiatives and strategies, and consumer acceptance thereof;
            </li>
            <li>changes in the plans, initiatives and strategies of the third parties that are necessary or important to
                the Startup's success;
            </li>
            <li>competitive pressures, including as a result of changes in technology;
            </li>
            <li>the Startup's ability to deal effectively with economic slowdowns or other economic or market
                difficulties;
            </li>
            <li>increased volatility or decreased liquidity in the capital markets, including any limitation on the
                Startup's ability to access the capital markets for debt securities, refinance its outstanding
                indebtedness or obtain equity, debt or bank financings on acceptable terms;
            </li>
            <li>the failure to meet earnings expectations;
            </li>
            <li>the adequacy of the Startup's risk management framework;
            </li>
            <li>changes in U.S. GAAP or other applicable accounting policies;
            </li>
            <li>the impact of terrorist acts, hostilities, natural disasters (including extreme weather) and pandemic
                viruses;
            </li>
            <li>a disruption or failure of the Startup's or its vendors' network and information systems or other
                technology on which the Company's businesses rely;
            </li>
            <li>changes in tax, federal communication and other laws and regulations;
            </li>
            <li>changes in foreign exchange rates and in the stability and existence of foreign currencies; and
            </li>
            <li>other risks and uncertainties which may or may not be specifically discussed in materials provided to
                Investors.
            </li>
        </ul>

        <p>Any forward-looking statement made by a Startup speaks only as of the date on which it is made. Startups are
            under no obligation to, and generally expressly disclaim any obligation to, update or alter their
            forward-looking statements, whether as a result of new information, subsequent events or otherwise.

        </p>

        <p><em>The foregoing risks do not purport to be a complete explanation of all the risks involved in acquiring
                equity securities in a Startup or an Interest in a Fund. Each Investor is urged to seek its own
                independent legal and tax advice and read the relevant investment documents before making a
                determination whether to invest in a Startup or a Fund.</em>
        </p>
    </div>
</div>