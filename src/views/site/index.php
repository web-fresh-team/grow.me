<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\OwlCarousel;
use alexBond\thumbler\Thumbler;
use yii\helpers\BaseStringHelper;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $feedBacks app\modules\feedback\models\Feedback [] */
/* @var $projects app\modules\project\models\Project [] */
/* @var $newProjects app\modules\project\models\Project [] */

$this->title = Yii::t('app', 'Grow Me');
$settings = Yii::$app->settings;
$graphRows = $settings->get('index.graph-rows');
OwlCarousel::register($this);
$this->registerJs(<<<JS
    var svg = [];
    var jsonArray = $graphRows;
    var drawDonut = function () {

        if (svg.length) {
            svg.remove();
        }

        var graph = jQuery('#info-graph'),
                plotX = graph.width() / 2,
                plotY = graph.height() / 2;

            if (!graph.length) return;

            svg = d3.select("body").append("svg").attr("width", graph.width()).attr("height", graph.height()).attr("id", "graph-wrap");
            jQuery('#graph-wrap').appendTo(graph);

            svg.append("g").attr("id", "graph");

            var salesData = [];

            $.each(jsonArray,function(key, value){
                    var cur = jQuery(value)[0];
                    salesData.push({label: cur.name, color: cur.color, value: cur.numb});
            });


            Donut3D.draw("graph", salesData, plotX, plotY, 90, 40, 25, 0.65);
        };
        jQuery(document).ready(function () {
            drawDonut();
            window.addEventListener('resize', drawDonut);

        });

JS
);

?>
<div class="wrapper-footer">
    <div class="content">
        <div class="hero">
            <div class="container">
                <div class="sub-slogan">
                    <span><?= $settings->get('index.slogan'); ?></span>
                </div>
                <div class="slogan">
                    <span><?= $settings->get('index.main-slogan'); ?></span>
                </div>
                <div class="buttons">
                    <?php if (Yii::$app->user->isGuest) : ?>
                        <div class="button">
                            <?= Html::a(Yii::t('app', 'I have an idea'), ['user/default/register', 'role' => 1]) ?>
                        </div>
                        <div class="button">
                            <?= Html::a(Yii::t('app', 'I want to invest'), ['user/default/register', 'role' => 2]) ?>
                        </div>
                    <?php else: ?>
                        <div class="button button-else">
                            <a href="/personal-area/<?= Yii::$app->user->identity->username ?>"><?= Yii::t('app', 'Personal area') ?></a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="info">
            <div class="container">
                <div class="top-info-text">
                    <span><?= $settings->get('index.top-info-text'); ?></span>
                </div>
                <div class="info-text">
                    <p>
                        <?= $settings->get('index.info-text'); ?>
                    </p>
                    <p>

                    </p>
                </div>
                <div class="link">
                    <a href="#infograph"><?= $settings->get('index.link-to-graph') ?></a>
                </div>
                <div class="article-text">
                    <p>В головах стартаперов — сборная солянка идей. Они запускают свои идеи, и большинство из них
                        проваливаются в первые пару месяцев. Так случается, потому что стартапер концентрируется не на
                        том: икбоксы в офисе и печеньки не имеют ничего общего с коммерчески успешным проектом. </p>

                    <p>
                        Деньги стартапер берет у инвесторов. Инвесторам нужны твердые бизнес-идеи. Часто у начинающего
                        их нет, потому что надо ждать несколько лет, пока стартапер набьет шишки, потеряет деньги и
                        опять их заработает. Это долго и неэффективно. </p>

                    <p>
                        Инвестору хорошо, когда перспективы идеи видны сразу. Мы представляем инвестиционную площадку Grow-me. Осветите свою гениальную идею, и ее заметят.
                    </p>
                </div>
            </div>
        </div>
        <div class="popular">
            <div class="container">
                <div class="title"><span><?= Yii::t('app', 'Popular on Grow.Me') ?></span></div>
                <div class="projects">
                    <?php $i = 0;
                    foreach ($projects as $project) :?>
                        <div class="col">
                            <a href="/idea/<?= $project->slug ?>" class="card">
                                <div class="thumb"><img src="
                        <?= $path = Helper::getThumb($project->photos['0']['url'], 272, 180, 'project-card', Thumbler::METHOD_BOXED) ?>
                            "/></div>
                                <div
                                    class="cost"><?= Yii::t('app', '${amount} invested', ['amount' => $project->amount]) ?> </div>
                                <div class="likes"><i class="fa fa-heart"></i><?= $project->likes_amount ?></div>
                                <div class="desc">
                                    <?php
                                        $description = $project->short_description;
                                    ?>
                                    <div class="name"><span><?= $title = $project->title; ?></span></div>
                                    <div class="list">
                                        <?= $description ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach ?>
                    <?php $i = 0;
                    foreach ($newProjects as $project) :?>
                        <div class="col">
                            <a href="/idea/<?= $project->slug ?>" class="card">
                                <div class="thumb"><img src="
                        <?= $path = Helper::getThumb($project->photos['0']['url'], 272, 180, 'project-card', Thumbler::METHOD_BOXED) ?>
                            "/></div>
                                <div
                                    class="cost"><?= Yii::t('app', '${amount} invested', ['amount' => $project->amount]) ?></div>
                                <div class="likes"><i class="fa fa-heart"></i><?= $project->likes_amount ?></div>
                                <div class="desc">
                                    <?php
                                        $description = $project->short_description;
                                    ?>
                                    <div class="name"><span><?= $title = $project->title; ?></span></div>
                                    <div class="list">
                                        <?= $description ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="info-graph" id="infograph">
            <div class="container">
                <div class="text">
                    <div class="title"><span><?= $settings->get('index.graph-title'); ?></span></div>
                    <div class="list">
                        <ol>
                            <?php $i = 1;
                            $array = json_decode($graphRows);
                            foreach ($array as $val): ?>
                                <li data-color="<?= $val->color ?>">
                                    <span style="background-color: <?= $val->color ?>">
                                        <?= $i++ ?>
                                    </span>
                                    <?= $val->info ?>
                                </li>
                            <?php endforeach ?>
                        </ol>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="graphic">
                    <div id="info-graph"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="info">
            <div class="container">
                <div class="article-text">
                    <div class="top-info-text">Зернышко
                    </div>
                    <p>
                        Стартап — как зернышко. Под зернышком мы понимаем идею: ее надо посадить в землю, ухаживать и поливать.
                    </p>

                    <p>
                        Grow-me — место для зернышек. Мы не раскручиваем еще больше готовые стартапы, а помогаем рассказать о себе и получить помощь в запуске.
                    </p>

                    <p>
                        Чтобы получить первую инвестицию — не нужно бизнес-планов. Хватит интересного рассказать об идее.
                    </p>
                </div>

                <div class="article-text">
                    <div class="top-info-text">Аналоги
                    </div>
                    <p>Стартаперам сложно полагаться на одну площадку, чтобы найти инвестиции. Есть аналоги Grow-me, но
                        везде берут процент за успешную сделку стартапа и инвестора. На Grow-me вам ничего не нужно платить.
                    </p>

                    <p>Есть похожий на нас американский сервис angel.co. Но там все на английском — для незнающих сложно
                        разобраться.
                    </p>

                    <p>В Рунете есть бизнес-форумы, на которых люди ищут инвесторов и проекты для вложений.
                    </p>

                    <p>Проблема форума — много мошенников. Об этом администрация предупреждает, но гарантий безопасности сделки нет. У нас мошенников нет — об этом ниже.
                    </p>
                </div>
                <div class="article-text">
                    <div class="top-info-text">Как работает Grow-me
                    </div>
                    <ol class="list">
                        <li><b>Регистрация.</b> Вы подаете заявку на сайте и получаете доступ в личный кабинет. В кабинете
                            можно посмотреть статистику по инвестициям: количество вложенных денег, сколько вернуть
                            инвестору и так далее. Инвестор видит то же самое.
                        </li>
                        <li><b>Рассказ о себе.</b> Хороший текст должен быть о читателе. Не о вас, а о том, что волнует
                            читателя. Мы никому не интересны. Люди интересны друг другу настолько, насколько они друг
                            другу полезны. Поэтому в хорошем тексте о себе не будет ничего о вас. Если текст для
                            инвесторов — подумайте, как им будете полезны вы и ваша идея.
                        </li>
                        <li><b>Получение дивидендов.</b> Финансовые пожелания стартап описывает заранее. Условия идеи инвестор
                            может обсудить индивидуально со стартапом. Совет: не тратьте полученные деньги на создание
                            новых сайтов и аренду дорогих офисов. Деньги инвестора — для вложений, которые
                            гарантированно отобьют деньги.
                        </li>
                        <li><b>Отчетность.</b> Grow-me показывает статистику стартапа: как долго существует идея, сколько в нее вложили, какой возврат инвестиций. Прозрачная статистика в свободном доступе.
                        </li>
                    </ol>
                </div>
                <div class="article-text">
                    <div class="top-info-text">Кому запрещен вход
                    </div>
                    <p>Мы не работаем с частными лицами и политическими организациями. Не терпим мошенников. Отсекаем таких на этапе модерации.
                    </p>
                </div>
                <div class="article-text">
                    <div class="top-info-text">Основатель
                    </div>
                    <div class="img-wrap">
                        <img  src="/img/founder.png" alt="img">
                        <div class="sub-img"><b>Олег Вовк</b></div>
                    </div>
                    <p>Создал Grow-me, директор. Ведет бизнес-паблик “Путь миллиардера”. Олег привлекает стартаперов и инвесторов на проект Grow-me. Лично консультируют по любым бизнес-вопросам.
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="reviews">
            <div class="container">
                <div class="title">
                    <span><?= Yii::t('app', 'Feedbacks') ?></span>
                </div>
                <div class="owl-carousel" id="review-list">
                    <?php foreach ($feedBacks as $feedBack): ?>
                        <div class="review">
                            <div class="photo">
                                <img src="<?= $feedBack->photo ?>" alt=""/>
                            </div>
                            <div class="name"><?= $feedBack->title ?></div>
                            <div class="text">
                                <p>
                                    <?= $feedBack->text ?>
                                </p>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <div class="bottom-hero">
            <div class="container">
                <div class="title">
                    <?= $settings->get('index.bottom-title'); ?>
                </div>
                <div class="text">
                    <?= $settings->get('index.bottom-text'); ?>
                </div>
                <div class="button">
                    <?php if (Yii::$app->user->isGuest): ?>
                        <?= Html::a(Yii::t('app', 'Start'), '#') ?>
                    <?php else: ?>
                        <?= Html::a(Yii::t('app', 'Start'), ['/site/start']) ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>