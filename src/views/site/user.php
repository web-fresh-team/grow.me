<?php

use app\assets\LightSlider;
use app\assets\PerfectScrollBar;
use app\modules\user\models\User;
use app\components\Helper;
use yii\bootstrap\Html;
use app\modules\project\models\Project;
use alexBond\thumbler\Thumbler;

/* @var $projects \app\modules\user\controllers\AdminController */
/* @var $model User */
/* @var $this yii\web\View */

$this->title = Yii::t('app', "{username}'s page ", ['username' => $model->username]);
LightSlider::register($this);
PerfectScrollBar::register($this);

$futureFriend =  $model->id;

$this->registerJs(<<<JS

window['PopUpShow'] = function (text) {
    $("p").remove();
    var cur = jQuery('.popup-content');
    $('<p>'+text+'</p>').appendTo(cur);
    $("#popup").show();
};

window['PopUpHide'] = function () {
    $("#popup").hide();
};
function awesomeGallery(slider) {
    slider.lightSlider({
        gallery: true,
        item: 1,
        vertical: true,
        verticalHeight: 520,
        autoWidth: false,
        vThumbWidth: 50,
        thumbMargin: 10,
        slideMargin: 0,
        controls: false,
        enableDrag: false,
        thumbItem: 5,
        onSliderLoad: function (el) {
            var myFunc = function(element) {
                var arr = element.parents('.lSPager').find('li');
                var toFind = element.parent();
                var num = arr.index(toFind);
                var dataId = jQuery(datas[num]);
                var dataFriend = dataId.data('friend');
                btn.attr('data-friend', dataFriend);
                writeBtn.attr('data-companion', dataFriend);
            };
            var datas = el.parent().parent().find('#vertical li');
            var link = el.parent().parent().find('.lSPager li a');
            var btn = jQuery('.second-friend-btn');
            var writeBtn = jQuery('#write-friend');
            var element = jQuery(link[0]);
            if(jQuery(element).html() == jQuery(link[0]).html()) {
                myFunc(element);
            } else {
                link = jQuery('.lslide.active');
                var dataFriend = link.data('friend');
                btn.attr('data-friend', dataFriend);
                writeBtn.attr('data-companion', dataFriend);
            }
            link.click(function () {
                var element = $(this);
                myFunc(element);
            });
        }
    });
}
    jQuery(document).ready(function () {
        var slider = jQuery('#vertical');
        awesomeGallery(slider);

        var personalProjectListScrollBar = function () {
            var inf2 = jQuery('.bottom-part .order ul');
            if (!inf2.length) return;
            inf2.perfectScrollbar({});
        };
        personalProjectListScrollBar();

        //Add to friend
        var AddToFriend = function(futureFriend) {
            jQuery.ajax({
                url: '/friend/add-to-friend',
                data: {'futureFriend' : futureFriend}
            }).done(function(data){
                PopUpShow(data.message);
            });
        };

        jQuery('.add-to-friend').click(function(){
            var futureFriend = "$futureFriend";
            AddToFriend(futureFriend);
        });

        //Like project
         var likeProject = function(project_id) {
            jQuery.ajax({
                url: '/site/like-project',
                data: {'project_id' : project_id}
            }).done(function(data){
                jQuery('.order-like p').text(data.amount);
            });
        };

        jQuery('.order-like a').click(function(){
            var project_id = jQuery(this).attr('data-project');
            likeProject(project_id);
        });

    });

JS
);

?>
<div class="wrapper-footer">
    <div class="user-wrapper">
        <div class="delete-friend-popup" id="popup">
            <div class="popup-content">
                <p></p>
                <a href="javascript:PopUpHide()"><?= Yii::t('app', 'Cancel') ?></a>
            </div>
        </div>
        <div class="content" style="padding-top: 10px">
            <div class="container">
                <div class="personal-information">
                    <div class="left-block">
                        <h1>
                            <?= $model->username ?>
                        </h1>
                        <div class="button">
                            <?php if(!$existFriendship): ?>
                                <a class="add-to-friend" href="javascript:void(0);">
                                    <?=Yii::t('app','Add friend')?>
                                </a>
                            <?php endif ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tabs">
                            <ul>
                                <li>
                                    <input type="radio" name="tabs-0" checked="checked" id="tabs-0-0"/>
                                    <label for="tabs-0-0"><?=Yii::t('app','About Me')?></label>

                                    <div class="personal-information-text">
                                        <img src="<?= Helper::getThumb($model->photo, 200, 150, 'avatar'); ?>">
                                        <table>
                                            <tr>
                                                <th><?=Yii::t('app','First name')?></th>
                                                <th><?=Yii::t('app','Last name')?></th>
                                            </tr>
                                            <tr>
                                                <td><?= $model->first_name ?></td>
                                                <td><?= $model->last_name ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </li>
                                <li>
                                    <input type="radio" name="tabs-0" id="tabs-0-1"/>
                                    <label for="tabs-0-1"><?=Yii::t('app','Contacts')?></label>

                                    <div class="personal-information-text">
                                        <table>
                                            <tr>
                                                <th>
                                                    <?= Yii::t('app', 'Email') ?>
                                                </th>
                                                <td>
                                                    <?= $model->email ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?=Yii::t('app','Phone')?>
                                                </th>
                                                <td>
                                                    <?= $model->phone ?>
                                                </td>

                                            </tr>
                                            <tr>
                                                <th>
                                                    <?=Yii::t('app','Address')?>
                                                </th>
                                                <td>
                                                    <?= $model->address ?>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="right-block">
                        <?php if(!$model->activeFriends):?>
                            <div class="no-friend">
                                <p><?=Yii::t('app','You don`t have friend yet')?></p>
                            </div>
                        <?php else : ?>
                            <ul id="vertical">
                                <?php $i = 0;
                                foreach ($model->activeFriends as $friend): ?>
                                    <li data-thumb="<?= $friend->photo ?>">
                                        <a href="/personal-area/<?= $friend->username ?>"
                                           style="background:
                                               url(<?= Helper::getThumb($friend->photo, 340, 500, 'friend-avatar', Thumbler::METHOD_CROP_CENTER); ?>)">
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="personal-list">
                    <div class="list-orders">
                        <div class="title">
                            <p>
                                <?php if(Yii::$app->user->identity->getRole() == User::ROLE_OWNER) : ?>
                                    <?= Yii::t('app', 'list of ideas') ?>
                                <?php elseif (Yii::$app->user->identity->getRole() == User::ROLE_INVESTOR): ?>
                                    <?= Yii::t('app', 'list of invest') ?>
                                <?php endif ?>
                            </p>
                        </div>
                        <div class="bottom-part">
                            <?php
                            if($countProjects == 0): ?>
                                <div class="no-friend no-list">
                                    <?php
                                    if (Yii::$app->user->identity->getRole() == User::ROLE_OWNER) : ?>
                                        <p><?=Yii::t('app','This user does not have projects yet')?></p>
                                    <?php else:?>
                                        <p><?=Yii::t('app','This user does not have investments yet')?></p>
                                    <?php endif ?>
                                </div>
                            <?php else : ?>
                                <div class="order">
                                    <ul>
                                        <?php foreach ($projects as $project): ?>
                                            <?php if($project->status != Project::STATUS_FOR_DELETING):?>
                                                <li>
                                                    <a href="#<?= $project->slug ?>">
                                                        <div>
                                                            <?= $project->title ?>
                                                        </div>
                                                    </a>
                                                </li>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="list-information">
                        <?php if($countProjects == 0): ?>
                            <div class="no-friend">
                                <?php
                                if (Yii::$app->user->identity->getRole() == User::ROLE_OWNER) : ?>
                                    <p><?=Yii::t('app','This user does not have projects yet')?></p>
                                <?php else:?>
                                    <p><?=Yii::t('app','This user does not have investments yet')?></p>
                                <?php endif ?>
                            </div>
                        <?php else : ?>
                            <?php foreach ($projects as $project): ?>
                                <?php if($project->status != Project::STATUS_FOR_DELETING):?>
                                    <div id="<?= $project->slug ?>">
                                        <div class="order-description">
                                            <div class="order-desc-image"
                                                 style='background: url("<?= Helper::getThumb($project->photos[0]['url'], 230, 400, 'personal-area-project') ?>")'></div>
                                            <div class="order-desc-text">
                                                <h1><?= $project->title ?></h1>

                                                <p>
                                                    <?= $project->description ?>
                                                </p>
                                            </div>
                                            <div class="order-bottom-info">
                                                <p><?= Yii::t('time', '{d, date, medium}', ['d' => $project->created_at]) ?></p>
                                                <?= Html::a(Yii::t('app', 'Read more'), ["/idea/" . $project->slug], ['class' => 'btn']) ?>

                                            </div>
                                        </div>
                                        <div class="order-navi-panel">
                                            <div class="order-like">
                                                <p><?= $project->likesAmount ?></p>
                                                <a href="javascript:void(0);" data-project="<?= $project->id ?>"></a>
                                            </div>
                                            <div class="order-comment">
                                                <p><?= $project->CommentsAmount ?></p>
                                                <a href="/idea/<?= $project->slug?>"></a>
                                            </div>
                                            <div class="order-delete">
                                                <a href="javascript:void(0);"></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>