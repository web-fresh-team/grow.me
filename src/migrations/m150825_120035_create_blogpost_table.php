<?php

use yii\db\Migration;

class m150825_120035_create_blogpost_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog_post}}', [
            'id' => $this->primaryKey(),

            'status' => $this->smallInteger()->notNull(),

            'text' => $this->text()->notNull(),
            'title' => $this->string()->notNull(),
            'photo' => $this->string(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%blog_post}}');

        return true;
    }

}
