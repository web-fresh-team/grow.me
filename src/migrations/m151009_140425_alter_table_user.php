<?php

use yii\db\Migration;

class m151009_140425_alter_table_user extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'address', $this->string());
        $this->alterColumn('{{%user}}', 'phone', $this->string());
        $this->alterColumn('{{%user}}', 'password_hash', $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%user}}', 'address', $this->string()->notNull());
        $this->alterColumn('{{%user}}', 'phone', $this->string()->notNull());
        $this->alterColumn('{{%user}}', 'password_hash', $this->string()->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
