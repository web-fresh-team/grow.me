<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_073153_create_investment_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%investment}}', [
            'id' => Schema::TYPE_PK,

            'amount' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT 0',

            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',

            'user_id' => Schema::TYPE_INTEGER,
            'project_id' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%investment}}');
        return true;
    }
}
