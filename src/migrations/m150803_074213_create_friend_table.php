<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_074213_create_friend_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%friend}}', [
            'from_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'to_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (`from_id`, `to_id`)',

            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%friend}}');
        return true;
    }
}
