<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\User;

class m150804_134828_add_fields_for_registration_confirmation_via_mail extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'confirm_token', Schema::TYPE_STRING);
        $this->addColumn('user', 'status', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT ' . User::STATUS_NEW);
    }

    public function down()
    {
        $this->dropColumn('user', 'confirm_token');
        $this->dropColumn('user', 'status');

        return true;
    }

}
