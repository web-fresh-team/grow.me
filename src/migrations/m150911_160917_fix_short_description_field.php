<?php

use yii\db\Migration;

class m150911_160917_fix_short_description_field extends Migration
{

    public function up()
    {
        $this->alterColumn('project', 'short_description', $this->string()->notNull());
    }

    public function down()
    {
        $this->alterColumn('project', 'short_description', $this->string(50)->notNull());

        return true;
    }

}
