<?php

use yii\db\Schema;
use yii\db\Migration;

class m150804_123547_create_likes_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%likes}}', [
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'project_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (`user_id`, `project_id`)',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%likes}}');
        return true;
    }


}
