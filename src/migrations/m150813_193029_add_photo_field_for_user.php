<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_193029_add_photo_field_for_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'photo', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('user', 'photo');
        return true;
    }

}
