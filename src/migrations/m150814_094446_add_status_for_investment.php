<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_094446_add_status_for_investment extends Migration
{
    public function up()
    {
        $this->addColumn('investment', 'status', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('investment', 'status');
        return true;
    }
}
