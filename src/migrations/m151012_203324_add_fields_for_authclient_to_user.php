<?php

use yii\db\Migration;

class m151012_203324_add_fields_for_authclient_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'facebook_id', $this->string());
        $this->addColumn('{{%user}}', 'vkontakte_id', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'facebook_id');
        $this->dropColumn('{{%user}}', 'vkontakte_id');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
