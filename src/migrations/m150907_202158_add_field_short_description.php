<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_202158_add_field_short_description extends Migration
{
    public function safeUp()
    {
        $this->addColumn('project', 'short_description', Schema::TYPE_STRING . '(50) NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('project', 'short_description');

        return true;
    }

}
