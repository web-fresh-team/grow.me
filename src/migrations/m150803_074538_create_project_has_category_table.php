<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_074538_create_project_has_category_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project_has_category}}', [
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'project_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (`category_id`, `project_id`)',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%project_has_category}}');
        return true;
    }
}
