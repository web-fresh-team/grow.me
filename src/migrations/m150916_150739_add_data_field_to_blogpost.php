<?php

use yii\db\Migration;

class m150916_150739_add_data_field_to_blogpost extends Migration
{
    public function up()
    {
        $this->addColumn('{{%blog_post}}', 'data', $this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%blog_post}}', 'data');
        return true;
    }

}
