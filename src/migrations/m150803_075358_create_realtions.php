<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_075358_create_realtions extends Migration
{
    public function up()
    {
        //Comment
        $this->addForeignKey('user_of_comment', '{{%comment}}', ['user_id'], '{{%user}}', ['id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('project_of_comment', '{{%comment}}', ['project_id'], '{{%project}}', ['id'], 'CASCADE', 'CASCADE');
        //Investment
        $this->addForeignKey('user_of_investment', '{{%investment}}', ['user_id'], '{{%user}}', ['id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('project_of_investment', '{{%investment}}', ['project_id'], '{{%project}}', ['id'], 'CASCADE', 'CASCADE');
        //Message
        $this->addForeignKey('user_from', '{{%message}}', ['from_id'], '{{%user}}', ['id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('user_to', '{{%message}}', ['to_id'], '{{%user}}', ['id'], 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        //Comment
        $this->dropForeignKey('user_of_comment', '{{%comment}}');
        $this->dropForeignKey('project_of_comment', '{{%comment}}');
        //Investment
        $this->dropForeignKey('user_of_investment', '{{%investment}}');
        $this->dropForeignKey('project_of_investment', '{{%investment}}');
        //Message
        $this->dropForeignKey('user_from', '{{%message}}');
        $this->dropForeignKey('user_to', '{{%message}}');

        return true;
    }
}
