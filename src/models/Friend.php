<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "friend".
 *
 * @property integer $from_id
 * @property integer $to_id
 * @property integer $status
 */
class Friend extends \yii\db\ActiveRecord
{

    const STATUS_SENT = 0;
    const STATUS_ACCEPTED = 1;

    static public function getStatusesMap()
    {
        return [
            self::STATUS_SENT => Yii::t('app', 'Sent'),
            self::STATUS_ACCEPTED => Yii::t('app', 'Accepted'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'to_id'], 'required'],
            [['from_id', 'to_id', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from_id' => Yii::t('app', 'From ID'),
            'to_id' => Yii::t('app', 'To ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getUserFrom()
    {
        return $this->hasOne(User::className(),['from_id' => 'id']);
    }

    public function getUserTo()
    {
        return $this->hasOne(User::className(),['from_id' => 'id']);
    }

}
