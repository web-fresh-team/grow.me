<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_has_category".
 *
 * @property integer $category_id
 * @property integer $project_id
 */
class ProjectHasCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_has_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'project_id'], 'required'],
            [['category_id', 'project_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('app', 'Category ID'),
            'project_id' => Yii::t('app', 'Project ID'),
        ];
    }
}
