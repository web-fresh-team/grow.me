<?php

namespace app\models;

use app\modules\category\models\Category;
use app\modules\project\models\Project;
use Yii;
use yii\base\Model;

class SortProject extends  Model
{
    public $category = 'all';
    public $likeSort;
    public $bankSort;

    static public function getBankMap()
    {
        return [
            0 => Yii::t('app', 'Except bank'),
            SORT_ASC => Yii::t('app', 'By bank ascendind'),
            SORT_DESC => Yii::t('app', 'By bank descendind')
        ];
    }

    static public function getLikeMap()
    {
        return [
            0 => Yii::t('app', 'Except likes'),
            SORT_ASC => Yii::t('app', 'By likes ascendind'),
            SORT_DESC => Yii::t('app', 'By likes descendind')
        ];
    }

    public function rules()
    {
        return [
            [['category','bankSort','likeSort'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'byBank' => Yii::t('app', 'By bank'),
            'byLikes' => Yii::t('app', 'Popular'),
            'category' => Yii::t('app', 'Category')
        ];
    }

    public function getProjects()
    {
        if($this->category == 0) {
            if ($this->bankSort == 0) {
                $projects = Project::find()->orderBy(['likes_amount' => intval($this->likeSort)])->all();
            } elseif ($this->likeSort == 0) {
                $projects = Project::find()->orderBy(['bank' => intval($this->bankSort)])->all();
            } else {
                $projects = Project::find()->orderBy(['bank' => intval($this->bankSort), 'likes_amount' => intval($this->likeSort)])->all();
            }
            return $projects;
        } else {
            /** @var Category $categoryM */
            $categoryM = Category::findOne(['id' => $this->category]);
            if ($this->bankSort == 0) {
                $projects = $categoryM->getProjects()->orderBy(['likes_amount' => intval($this->likeSort)])->all();
            } elseif ($this->likeSort == 0) {
                $projects = $categoryM->getProjects()->orderBy(['bank' => intval($this->bankSort)])->all();
            } else {
                $projects = $categoryM->getProjects()->orderBy(['bank' => intval($this->bankSort), 'likes_amount' => intval($this->likeSort)])->all();
            }
            return $projects;
        }
    }
}