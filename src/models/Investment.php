<?php

namespace app\models;

use app\components\TimeCreated;
use app\modules\project\models\Project;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\modules\user\models\User;

/**
 * This is the model class for table "investment".
 *
 * @property integer $id
 * @property double $amount *
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property integer $project_id
 * @property integer $status
 *
 * @property Project $project
 * @property User $user
 */
class Investment extends \yii\db\ActiveRecord
{

    const STATUS_SENT = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_DENIED = 2;

    static public function getStatusesMap()
    {
        return [
            self::STATUS_SENT => Yii::t('app', 'Sent'),
            self::STATUS_ACCEPTED => Yii::t('app', 'Accepted'),
            self::STATUS_DENIED => Yii::t('app', 'Denied'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'number'],
            [['status'], 'required'],
            [['created_at', 'updated_at', 'user_id', 'project_id'], 'integer'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'amount' => Yii::t('app', 'Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'user_id' => Yii::t('app', 'User ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'datetime' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    use TimeCreated;

    public function getTimeCreated() {
        return $this->countTime($this->created_at);
    }

}
