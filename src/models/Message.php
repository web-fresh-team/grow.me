<?php

namespace app\models;

use app\components\TimeCreated;
use app\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $from_id
 * @property integer $to_id
 * @property string $topic
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 *
 * @property User $from
 * @property User $to
 */
class Message extends \yii\db\ActiveRecord
{
    const STATUS_NOTREAD = 0;
    const STATUS_READ = 1;

    static public function getStatusesMap()
    {
        return [
            self::STATUS_NOTREAD => Yii::t('app', 'Not read'),
            self::STATUS_READ => Yii::t('app', 'Read'),

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'to_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['text'], 'required'],
            [['text'], 'string'],
            [['topic'], 'string', 'max' => 255],
            [['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_id' => 'id']],
            [['to_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_id' => 'id']],

        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'datetime' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_id' => Yii::t('app', 'From ID'),
            'to_id' => Yii::t('app', 'To ID'),
            'topic' => Yii::t('app', 'Topic'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::className(), ['id' => 'to_id']);
    }

    use TimeCreated;

    public function getTimeCreated() {
        return $this->countTime($this->created_at);
    }

}
