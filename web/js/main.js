var app = {
    mainMenu: {
        open: false,
        toggle: function () {
            var self = app.mainMenu;
            self.open = !self.open;
            console.log(self.element);
            if (self.open) {
                app.body.addClass('no-scroll');
                self.element.addClass('show');
                self.element.animate({'height': '100%'},300);
            } else {
                self.element.animate({'height': '0%'}, function () {
                    self.element.removeClass('show');
                    app.body.removeClass('no-scroll');
                });
            }
        }
    }
};
!function () {
    var Donut3D = {};

    function pieTop(d, rx, ry, ir) {
        if (d.endAngle - d.startAngle == 0) return "M 0 0";
        var sx = rx * Math.cos(d.startAngle),
            sy = ry * Math.sin(d.startAngle),
            ex = rx * Math.cos(d.endAngle),
            ey = ry * Math.sin(d.endAngle);

        var ret = [];
        ret.push("M", sx, sy, "A", rx, ry, "0", (d.endAngle - d.startAngle > Math.PI ? 1 : 0), "1", ex, ey, "L", ir * ex, ir * ey);
        ret.push("A", ir * rx, ir * ry, "0", (d.endAngle - d.startAngle > Math.PI ? 1 : 0), "0", ir * sx, ir * sy, "z");
        return ret.join(" ");
    }

    function drawArrow(d, rx, ry, ir) {
        var avg = 0.5 * (d.startAngle + d.endAngle);
        var
            ax = rx * Math.cos((avg - 0.1)),
            ay = ry * Math.sin((avg - 0.1)),
            bx = rx * 1.1 * Math.cos((avg - 0.04)),
            by = ry * 1.1 * Math.sin((avg - 0.04)),
            zx = rx * Math.cos((avg + 0.05)),
            zy = ry * Math.sin((avg + 0.05)),
            xx = rx * 1.1 * Math.cos((avg)),
            xy = ry * 1.1 * Math.sin((avg)),
            hx = rx * 1.4 * Math.cos((avg + 0.05)),
            hy = ry * 1.4 * Math.sin((avg + 0.05));
        var ret = [];
        ret.push(ax + ',' + ay);
        ret.push(bx + ',' + by);
        ret.push(hx + ',' + hy);
        ret.push(xx + ',' + xy);
        ret.push(zx + ',' + zy);
        return ret.join(' ');
    }


    function pieOuter(d, rx, ry, h) {
        var startAngle = (d.startAngle > Math.PI ? Math.PI : d.startAngle);
        var endAngle = (d.endAngle > Math.PI ? Math.PI : d.endAngle);

        var sx = rx * Math.cos(startAngle),
            sy = ry * Math.sin(startAngle),
            ex = rx * Math.cos(endAngle),
            ey = ry * Math.sin(endAngle);

        var ret = [];
        ret.push("M", sx, h + sy, "A", rx, ry, "0 0 1", ex, h + ey, "L", ex, ey, "A", rx, ry, "0 0 0", sx, sy, "z");
        return ret.join(" ");
    }

    function pieInner(d, rx, ry, h, ir) {
        var startAngle = (d.startAngle < Math.PI ? Math.PI : d.startAngle);
        var endAngle = (d.endAngle < Math.PI ? Math.PI : d.endAngle);

        var sx = ir * rx * Math.cos(startAngle),
            sy = ir * ry * Math.sin(startAngle),
            ex = ir * rx * Math.cos(endAngle),
            ey = ir * ry * Math.sin(endAngle);

        var ret = [];
        ret.push("M", sx, sy, "A", ir * rx, ir * ry, "0 0 1", ex, ey, "L", ex, h + ey, "A", ir * rx, ir * ry, "0 0 0", sx, h + sy, "z");
        return ret.join(" ");
    }

    function getPercent(d) {
        return (d.endAngle - d.startAngle > 0.2 ?
        Math.round(1000 * (d.endAngle - d.startAngle) / (Math.PI * 2)) / 10 + '%' : '');
    }

    Donut3D.transition = function (id, data, rx, ry, h, ir) {
        function arcTweenInner(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) {
                return pieInner(i(t), rx + 0.5, ry + 0.5, h, ir);
            };
        }

        function arcTweenTop(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) {
                return pieTop(i(t), rx, ry, ir);
            };
        }

        function arcTweenOuter(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) {
                return pieOuter(i(t), rx - .5, ry - .5, h);
            };
        }

        function textTweenX(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) {
                return 0.6 * rx * Math.cos(0.5 * (i(t).startAngle + i(t).endAngle));
            };
        }

        function textTweenY(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) {
                return 0.6 * rx * Math.sin(0.5 * (i(t).startAngle + i(t).endAngle));
            };
        }

        var _data = d3.layout.pie().sort(null).value(function (d) {
            return d.value;
        })(data);

        d3.select("#" + id).selectAll(".innerSlice").data(_data)
            .transition().duration(750).attrTween("d", arcTweenInner);

        d3.select("#" + id).selectAll(".topSlice").data(_data)
            .transition().duration(750).attrTween("d", arcTweenTop);

        d3.select("#" + id).selectAll(".outerSlice").data(_data)
            .transition().duration(750).attrTween("d", arcTweenOuter);

        d3.select("#" + id).selectAll(".percent").data(_data).transition().duration(750)
            .attrTween("x", textTweenX).attrTween("y", textTweenY).text(getPercent);
    };

    Donut3D.draw = function (id, data, x /*center x*/, y/*center y*/,
                             rx/*radius x*/, ry/*radius y*/, h/*height*/, ir/*inner radius*/) {

        var _data = d3.layout.pie().sort(null).value(function (d) {
            return d.value;
        })(data);

        var slices = d3.select("#" + id).append("g").attr("transform", "translate(" + x + "," + y + ")")
            .attr("class", "slices");


        slices.selectAll(".innerSlice").data(_data).enter().append("path").attr("class", "innerSlice")
            .style("fill", function (d) {
                return d3.hsl(d.data.color).darker(0.7);
            })
            .attr("d", function (d) {
                return pieInner(d, rx + 0.5, ry + 0.5, h, ir);
            })
            .each(function (d) {
                this._current = d;
            });


        slices.selectAll(".outerSlice").data(_data).enter().append("path").attr("class", "outerSlice")
            .style("fill", function (d) {
                return d3.hsl(d.data.color).darker(0.7);
            })
            .attr("d", function (d) {
                return pieOuter(d, rx - .5, ry - .5, h);
            })
            .each(function (d) {
                this._current = d;
            });

        slices.selectAll(".arrow").data(_data).enter().append("polygon").attr("class", "arrow")
            .style("fill", function (d) {
                return d.data.color;
            })
            .attr("points", function (d) {
                return drawArrow(d, rx, ry, ir);
            })
            .each(function (d) {
                this._current = d;
            });


        slices.selectAll(".topSlice").data(_data).enter().append("path").attr("class", "topSlice")
            .style("fill", function (d) {
                return d.data.color;
            })
            .style("stroke", function (d) {
                return d.data.color;
            })
            .attr("d", function (d) {
                return pieTop(d, rx, ry, ir);
            })
            .each(function (d) {
                this._current = d;
            });
        slices.selectAll(".percent").data(_data).enter().append("text").attr("class", "percent")
            .attr("x", function (d) {
                return 1.7 * rx * Math.cos(0.5 * (d.startAngle + d.endAngle) - 0.1);
            })
            .attr("y", function (d) {
                return 1.7 * ry * Math.sin(0.5 * (d.startAngle + d.endAngle) - 0.1) + 10;
            })
            .style("fill", function (d) {
                return d3.hsl(d.data.color);
            })
            .text(getPercent).each(function (d) {
                this._current = d;
            });
        slices.selectAll(".circle").data(_data).enter().append("circle").attr("class", "circle").attr("r", 3)
            .attr("cx", function (d) {
                return 1.4 * rx * Math.cos(0.5 * (d.startAngle + d.endAngle) + 0.05);
            })
            .attr("cy", function (d) {
                return 1.4 * ry * Math.sin(0.5 * (d.startAngle + d.endAngle) + 0.05);
            })
            .style("fill", function (d) {
                return d3.hsl(d.data.color);
            })
            .each(function (d) {
                this._current = d;
            });
    };

    this.Donut3D = Donut3D;
}();

jQuery(function () {
    app.body = jQuery('body');
    app.mainMenu.element = app.body.find('#main-menu');
    app.isDesktop = window.screen.width > 1200;

    var owlSlider = function() {

        var owlSliderCategory = jQuery('#owl-slider');

        if(!owlSliderCategory.length) return

        $("#owl-slider").owlCarousel({
            items: 1,
            loop: true,
            responsive: {
                850: {
                    items: 2
                },
                1260: {
                    items: 3
                }
            }
        });

        var owl = $("#owl-slider").data('owlCarousel');

        $("#next-button").click(function () {
            owl.next()
        });

        $("#prev-button").click(function () {
            owl.prev()
        });
    };
    owlSlider();

    var owlSliderBoard = function() {
        var boardSliderId = jQuery('#owl-slider-board');

        if(!boardSliderId.length) return;

        $('#owl-slider-board').owlCarousel({
            items : 4,
            loop: true,
            navigation: false,
            responsive: {
                400: {
                    items: 3
                },
                600: {
                    items: 4
                },
                700: {
                    items: 5
                },
                1000 : {
                    items: 6
                }
            }
        });

        var boardSlider = $("#owl-slider-board").data('owlCarousel');

        $("#prev-btn").click(function(){
            boardSlider.prev();
        });

        $("#next-btn").click(function(){
            boardSlider.next();
        });
    };
    owlSliderBoard();

    var desoSlider = function(){
        var $slideshow1 = $('#slideshow1'), $effect_provider = $('#effect_provider'), $effect_name = $('#effect_name');
        if(!$slideshow1.length) return;
        $slideshow1.desoSlide({
            thumbs: $('ul.slideshow1_thumbs li > a'),
            controls: false
        });

        $(".desoslide-wrapper").append("<a type='button' class='four-arrow' href=''></a>");

        $(".desoslide-wrapper .four-arrow").fancybox();
    };

    desoSlider();



    //ChangeFancy
    function changeF(isFirst) {
        var fancyLink = $('.desoslide-wrapper .four-arrow');
        if (isFirst == 0) {
            fancyLink.attr('href',$('.slideshow1_thumbs li a:first-child img').attr('src'));
        } else {
            fancyLink.attr('href',isFirst.find('img').attr('src'));
        }
    }
    changeF(0);
    $('.slideshow1_thumbs li a').click(function(){
        var curr = $(this);
        changeF(curr);
    });

    $(function() {
        var tab = $( ".personal-list" );
        if(!tab.length) return;
        tab.tabs();
        tab.addClass( " ui-tabs-vertical ui-helper-clearfix" );
        tab.find('li').removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

    });

    var listScrollBar = function () {
        var list = jQuery('#categoryBoard');
        if(!list.length) return;
        $('.list').perfectScrollbar({

        });
    };
    listScrollBar();

    var goSelect = function() {
        var mySelect = jQuery('select');
        if(!mySelect.length) return;
        $('select')
            .selectmenu()
            .addClass("overflow");
    };
    goSelect();

    var personalInformationScrollBar = function() {
        var inf = jQuery('.personal-information-text');
        if(!inf.length) return;
        $('.personal-information-text').perfectScrollbar({

        });
    };
    personalInformationScrollBar();

    var reviewListFn = function() {
        var reviewList = jQuery('#review-list');
        if(!reviewList.length) return;
        $('#review-list').owlCarousel({
            margin: 10,
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
    };
    reviewListFn();
});


// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.