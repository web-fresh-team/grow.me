<?php

namespace tests\unit\fixtures;


use yii\test\ActiveFixture;
use yii\test\Fixture;

class ProjectFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\project\models\Project';
}