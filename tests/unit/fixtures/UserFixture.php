<?php

namespace tests\unit\fixtures;


use yii\test\ActiveFixture;
use yii\test\Fixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\user\models\User';
}