<?php

namespace tests\unit\fixtures;
use yii\test\ActiveFixture;
use yii\test\Fixture;

class CategoryFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\category\models\Category';
}