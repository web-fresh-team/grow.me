<?php

return [
    [
        'username' => 'dDenesik',
        'password_hash' => '$2y$13$Ls9otkcpNifZRmPWBiCS0./bnBa1MQnlrN73jMSf//5BNidCQXVHG',
        'role' => 1,
        'first_name' => 'Velva',
        'last_name' => 'Swift',
        'email' => 'uFritsch@hotmail.com',
        'phone' => '+13102325500',
        'address' => '60511 Cleta Harbors Suite 292
Bernhardshire, WV 49033',
        'status' => 2,
    ],
    [
        'username' => 'Daugherty.Kody',
        'password_hash' => '$2y$13$X6QHxOTVk6bla2zC90HyyuGczl.HWgRRS0iKSBOSUBUvZHtYJTXiS',
        'role' => 1,
        'first_name' => 'Gavin',
        'last_name' => 'Connelly',
        'email' => 'lMaggio@gmail.com',
        'phone' => '1-406-276-6119',
        'address' => '46510 Langworth Gardens Apt. 179
Earleneshire, KS 14940-7491',
        'status' => 1,
    ],
];
