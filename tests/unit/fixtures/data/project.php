<?php

return [
    [
        'title' => 'eErnser',
        'description' => 'Ut dolorum recusandae aliquid consequatur. Placeat qui accusamus beatae necessitatibus.',
        'photos' => '[{"url":"/uploads/Amsterdam.jpg"}]',
        'bank' => 26729,
        'status' => 1,
        'likes_amount' => '0',
        'created_at' => 813677596,
        'updated_at' => 813677596,
        'slug' => 'eErnser',
        'user_id' => '1',
    ],
    [
        'title' => 'wRodriguez',
        'description' => 'Veritatis aspernatur vel dolorem ducimus voluptas omnis dicta debitis. Quam animi ex et et ratione. Alias cupiditate natus labore laudantium.',
        'photos' => '[{"url":"/uploads/Amsterdam.jpg"}]',
        'bank' => 29213,
        'status' => 1,
        'likes_amount' => '0',
        'created_at' => 911637876,
        'updated_at' => 911637876,
        'slug' => 'wRodriguez',
        'user_id' => '1',
    ],
];
