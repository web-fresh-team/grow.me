<?php

namespace tests\unit\fixtures;

use yii\test\ActiveFixture;
use yii\test\Fixture;

class FeedbackFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\feedback\models\Feedback';

}
