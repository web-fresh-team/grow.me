<?php
namespace tests\unit\fixtures;

use yii\test\ActiveFixture;
use yii\test\Fixture;

class SettingsFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\content\models\Settings';
}
