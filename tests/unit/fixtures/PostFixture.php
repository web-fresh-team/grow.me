<?php

namespace tests\unit\fixtures;

use yii\test\ActiveFixture;
use yii\test\Fixture;

class PostFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\blog\models\BlogPost';

}
