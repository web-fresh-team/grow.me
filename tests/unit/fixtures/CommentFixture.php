<?php

namespace tests\unit\fixtures;


use yii\test\ActiveFixture;
use yii\test\Fixture;

class CommentFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\comment\models\Comment';
}