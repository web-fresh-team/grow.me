<?php

/**
 * @var $faker Faker\Generator
 * @var $index integer
 */

$title = $faker->userName;
$time = $faker->unixTime;

return [
    'title' => $title,
    'created_at' => $time,
    'updated_at' => $time,
    'slug' => $title,
    'parent_id' => ''
];