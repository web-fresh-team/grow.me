<?php

use app\modules\user\models\User;
/**
 * @var $faker Faker\Generator
 * @var $index integer
 */

$title = $faker->userName;

return [
    'username' => $faker->userName,
    'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('password'),
    'role' => $faker->randomElement([User::ROLE_OWNER, User::ROLE_INVESTOR]),
    'first_name' => $faker->firstName,
    'last_name' => $faker->lastName,
    'email' => $faker->email,
    'phone' => $faker->phoneNumber,
    'address' => $faker->address,
    'status' => $faker->randomElement([User::STATUS_NEW, User::STATUS_ACTIVE]),
];