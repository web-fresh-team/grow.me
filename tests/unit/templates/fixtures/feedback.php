<?php

/**
 * @var $faker Faker\Generator
 * @var $index integer
 */

use app\modules\feedback\models\Feedback;

$title = $faker->words(3, true);
$time = $faker->unixTime;
$text = $faker->text(200);
$photo = '/uploads/Amsterdam.jpg';
$status = $faker->randomElement([Feedback::STATUS_APPROVED, Feedback::STATUS_CREATED]);

return [
    'title' => $title,
    'text' => $text,
    'photo' => $photo,
    'status' => $status,
    'created_at' => $time,
    'updated_at' => $time,
];
