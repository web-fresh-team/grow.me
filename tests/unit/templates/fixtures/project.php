<?php

use app\modules\project\models\Project;
/**
 * @var $faker Faker\Generator
 * @var $index integer
 */

$title = $faker->userName;
$time = $faker->unixTime;

return [
    'title' => $title,
    'description' => $faker->text,
    'photos' => '[{"url":"/uploads/Amsterdam.jpg"}]',
    'bank' => $faker->numberBetween(1000,100000),
    'status' => Project::STATUS_CREATED,
    'likes_amount' => '0',
    'created_at' => $time,
    'updated_at' => $time,
    'slug' => $title,
    'user_id' => '1'
];