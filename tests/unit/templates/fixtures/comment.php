<?php

use app\modules\comment\models\Comment;

/**
 * @var $faker Faker\Generator
 * @var $index integer
 */

$time = $faker->unixTime;

return [
    'text' => $faker->text,
    'status' => $faker->randomElement([Comment::STATUS_CREATED, Comment::STATUS_APPROVED]),
    'created_at' => $time,
    'updated_at' => $time,
    'parent_id' => '',
    'user_id' => $faker->randomElement([1, 2]),
    'project_id' => $faker->randomElement([1, 2])
];