<?php

/**
 * @var $faker Faker\Generator
 * @var $index integer
 */

use app\modules\blog\models\BlogPost;

$title = $faker->words(3, true);
$time = $faker->unixTime;
$text = $faker->text(($index + 1) * 300 + 300);
$photo = '/uploads/Amsterdam.jpg';
$status = $faker->randomElement([BlogPost::STATUS_APPROVED, BlogPost::STATUS_CREATED]);

return [
    'title' => $title,
    'text' => $text,
    'photo' => $photo,
    'status' => $status,
    'created_at' => $time,
    'updated_at' => $time,
];
